package pl.jacaato.homeautomation;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

import android.support.v7.app.ActionBarActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
	final Handler msgHandler = new Handler();
	TextView textViewDate;
	TextView textViewHum;
	TextView textViewTemp;
	TextView textViewToilet;
	TextView textViewFanStatus;
	JSONArray jsonArray = null;
	
	GraphView graphView;
	GraphViewSeries humiditySeries;
	GraphViewSeries temperatureSeries;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		textViewDate = (TextView)findViewById(R.id.text_view_date);
		textViewHum = (TextView)findViewById(R.id.text_view_humidity);
		textViewTemp = (TextView)findViewById(R.id.text_view_temp);
		textViewToilet = (TextView)findViewById(R.id.text_view_toilet);
		textViewFanStatus = (TextView)findViewById(R.id.text_view_fan_status);
		
		// init example series data
		humiditySeries = new GraphViewSeries(new GraphViewData[]{});
		temperatureSeries = new GraphViewSeries(new GraphViewData[]{});
		temperatureSeries.getStyle().color = Color.RED;
		 
		LineGraphView graphView = new LineGraphView(this, "BathroomData");
		graphView.setScrollable(true);
		graphView.setScalable(true);
		graphView.setManualMaxY(true);
		graphView.setManualMinY(true);
		graphView.setManualYAxisBounds(100, 0);
		graphView.setViewPort(System.currentTimeMillis()/1000, 60);
		graphView.getGraphViewStyle().setNumHorizontalLabels(4);
		graphView.getGraphViewStyle().setNumVerticalLabels(11);
		graphView.setDrawDataPoints(true);

		graphView.addSeries(humiditySeries); // data
		graphView.addSeries(temperatureSeries); // data
		
		 
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.activity_main_relative_layout);
		layout.addView(graphView);
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				refreshData();
			}
		}, 0, 2000);
	}
	
	private void refreshData(){
		try{
			URL url = new URL("http://jacaato-petrykozy.no-ip.info/php/system.php?bathroom_sensors=true");
			HttpURLConnection mUrlConnection = (HttpURLConnection) url.openConnection();
			mUrlConnection.setDoInput(true);
			
			BufferedInputStream is = new BufferedInputStream(mUrlConnection.getInputStream());
			
			String s = "";
			while(is.available() != 0){
				s += (char)is.read();
			}
			jsonArray = new JSONArray(s);
			msgHandler.post(handleTask);
		}
		catch(Exception e){
			Log.e("qwe", e.toString());
		}
	}
	
	final Runnable handleTask = new Runnable() {
		@Override
		public void run() {
			try {
				textViewDate.setText("Date: " + jsonArray.getString(0));
				textViewHum.setText("Hum: " + jsonArray.getString(1));
				textViewTemp.setText("Temp: " + jsonArray.getString(2));
				textViewToilet.setText("Toilet: " + jsonArray.getString(3));
				textViewFanStatus.setText("Fan status: " + jsonArray.getString(4));
				
				String str_date=jsonArray.getString(0);
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = (Date)formatter.parse(str_date); 
				
				humiditySeries.appendData(new GraphView.GraphViewData((double)(date.getTime() / 1000),Double.parseDouble(jsonArray.getString(1))), true, 60);
				temperatureSeries.appendData(new GraphView.GraphViewData((double)(date.getTime() / 1000),Double.parseDouble(jsonArray.getString(2))), true, 60);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
}