#include "spi.h"

//be sure it is sth dummy for communication
#define DUMMY_WRITE 0xFF

void SPI_master_init(){
	DDRB |= _BV(SS);
	DDRB |= _BV(MOSI);
	DDRB |= _BV(SCK);
	DDRB &= ~_BV(MISO);
	
	SPCR &= ~_BV(SPIE);
	SPCR &= ~_BV(DORD);
	SPCR |= _BV(MSTR);
	SPCR &= ~_BV(CPOL);
	SPCR &= ~_BV(CPHA);
	SPCR &= ~_BV(SPR1);
	SPCR &= ~_BV(SPR0);
	SPCR |= _BV(SPE);
	
	SPSR;
	SPDR;
}

inline uint8_t SPI_readByte(){
	//must send something. be sure it is sth dummy
	return SPI_writeByte(DUMMY_WRITE);
}

void SPI_readBytes(uint8_t* data, uint8_t dataLength){
	uint8_t i;
	for(i = 0 ; i < dataLength ; i++){
		data[i] = SPI_readByte();
	}
}

uint8_t SPI_writeByte(uint8_t c){
	SPDR = c;
	while( !(SPSR & _BV(SPIF)) );
	//you can read now too!
	return SPDR;
}
void SPI_writeBytes(uint8_t* data, uint8_t dataLength){
	uint8_t i;
	for(i = 0 ; i < dataLength ; i++){
		SPI_writeByte(data[i]);
	}
}