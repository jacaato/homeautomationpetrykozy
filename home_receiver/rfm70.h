//#define RFM70_DEBUG

#ifndef F_CPU
	#define F_CPU 8000000
#endif

#define CE		1
#define CE_ONE	{DDRB |= _BV(CE); PORTB |= _BV(CE);}
#define CE_ZERO	{DDRB |= _BV(CE); PORTB &= ~_BV(CE);}

//commands. some to use with (comm & x)
#define R_REGISTER			0b00000000
#define W_REGISTER			0b00100000
#define R_RX_PAYLOAD		0b01100001
#define W_TX_PAYLOAD		0b10100000
#define FLUSH_TX			0b11100001
#define FLUSH_RX			0b11100010
#define REUSE_TX_PL			0b11100011
#define ACTIVATE			0b01010000
#define R_RX_PL_WID			0b01100000
#define W_ACK_PAYLOAD		0b10101000
#define W_TX_PAYLOAD_NO_ACK	0b10110000
#define NOP					0b11111111

#include <util/delay.h>
#include <avr/pgmspace.h>
#include "spi.h"

// 0 no, >0 yes
inline uint8_t RFM70_isRXDataReady();

// returns length of read data
uint8_t RFM70_readPayload(uint8_t* pipeNumber, uint8_t* data, uint8_t dataLength);



uint8_t RFM70_readStatus();
inline void RFM70_writeRegister(uint8_t reg, uint8_t* data, uint8_t dataLength);
inline void RFM70_readRegister(uint8_t reg, uint8_t* data, uint8_t dataLength);
void RFM70_readCommand(uint8_t command, uint8_t* data, uint8_t dataLength);
void RFM70_writeCommand(uint8_t command, uint8_t* data, uint8_t dataLength);
uint8_t RFM70_checkBank();
void RFM70_setBank(uint8_t bank);
void RFM70_init();