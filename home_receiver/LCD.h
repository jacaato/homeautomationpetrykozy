#ifndef LCD_H
#define LCD_H

#ifndef F_CPU
	#define F_CPU 8000000
#endif

#define LCD_RS_HIGH	{PORTC |= _BV(5);}
#define LCD_RS_LOW	{PORTC &= ~_BV(5);}

#define LCD_E_HIGH	{PORTC |= _BV(4);}
#define LCD_E_LOW	{PORTC &= ~_BV(4);}

#define LCD_DB7_1	{PORTC |= _BV(3);}
#define LCD_DB7_0	{PORTC &= ~_BV(3);}

#define LCD_DB6_1	{PORTC |= _BV(2);}
#define LCD_DB6_0	{PORTC &= ~_BV(2);}

#define LCD_DB5_1	{PORTC |= _BV(1);}
#define LCD_DB5_0	{PORTC &= ~_BV(1);}

#define LCD_DB4_1	{PORTC |= _BV(0);}
#define LCD_DB4_0	{PORTC &= ~_BV(0);}

#define LCD_DATA	0
#define LCD_COMMAND	1
#define LCD_CLEAR	"               "

#define LCD_BUFFER_SIZE (16)

#define LCD_PORTS_INIT {DDRC = _BV(0) | _BV(1) | _BV(2) \
								|_BV(3) |_BV(4) |_BV(5);}

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>

void lcd_init();
void lcd_write_str(uint8_t row, char* sprintf_str, ...);
void lcd_write_data(uint8_t data, uint8_t isCommand);

#endif