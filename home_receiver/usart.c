#include "usart.h"

void USART_init(){
	//Set baud rate
	UBRRH = (unsigned char)((USART_UBRR >> 8));
	UBRRL = (unsigned char)USART_UBRR;
	//Enable receiver and transmitter
	UCSRB = (1<<RXEN)|(1<<TXEN);
	//Set frame format: 8data, 2stop bit
	UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);
}

uint8_t USART_isReadyToWrite(){
	if( ( UCSRA & (1<<UDRE)) )
		return 1;
	return 0;
}

void USART_writeByte(uint8_t c){
	//wait for empty transmit buffer
	while ( USART_isReadyToWrite() == 0 );
	//send uint8_tacter
	UDR = c;
}

void USART_writeStr(char * str, uint8_t len){
	uint8_t i = 0;
	for(; i < len ; i++){
		USART_writeByte(str[i]);
	}
} 

void USART_writeStdStr(char* str){
	uint8_t i = 0;
	while(str[i] != '\0'){
		USART_writeByte(str[i]);
		i++;
	}
}

uint8_t USART_isReadyToRead(){
	if( (UCSRA & (1<<RXC)) )
		return 1;
	return 0;
}

uint8_t USART_readByte(){
	//wait until a data is available
	while( USART_isReadyToRead() == 0 );
	//data is ready
	return UDR;
}

void USART_readStr(char* str, uint8_t len){
	uint8_t i = 0;
	for(; i < len ; i++){
		str[i] = USART_readByte();
	}
}

void USART_readStdStr(char* str){
	uint8_t i = 0;
	str[i] = USART_readByte();
	while(str[i] != '\0'){
		str[i] = USART_readByte();
		i++;
	}
}