#include "LCD.h"

void lcd_reset_line(){
	LCD_DB4_0;
	LCD_DB5_0;
	LCD_DB6_0;
	LCD_DB7_0;
}//*/

void lcd_write_init_command(uint8_t command){
	lcd_reset_line();
	LCD_RS_LOW;
	LCD_E_LOW;
	_delay_ms(5);
	
	if (command & 16)	{LCD_DB7_1;}
	if (command & 32)	{LCD_DB6_1;}
	if (command & 64)	{LCD_DB5_1;}
	if (command & 128)	{LCD_DB4_1;}
	
	LCD_E_HIGH;
	_delay_us(460);
	LCD_E_LOW;
}//*/

void lcd_write_data(uint8_t data,uint8_t isCommand){
	lcd_reset_line();
	
	if(isCommand == 1){
		LCD_RS_LOW;
	}else{
		LCD_RS_HIGH;
	}
	LCD_E_LOW;
	_delay_ms(5);
	
	//higher nibble
	if (data & 16)	{LCD_DB7_1;}
	if (data & 32)	{LCD_DB6_1;}
	if (data & 64)	{LCD_DB5_1;}
	if (data & 128)	{LCD_DB4_1;}
	
	LCD_E_HIGH;
	_delay_us(460);
	LCD_E_LOW;
	_delay_ms(5);
	
	lcd_reset_line();
	
	//lower nibble
	if (data & 1)	{LCD_DB7_1;}
	if (data & 2)	{LCD_DB6_1;}
	if (data & 4)	{LCD_DB5_1;}
	if (data & 8)	{LCD_DB4_1;}
	
	LCD_E_HIGH;
	_delay_us(460);
	LCD_E_LOW;
}//*/

void lcd_write_str(uint8_t row, char* str,...){
	va_list arg;
	va_start(arg,str); //inicjalizacja listy argumentów za argumentem row
	uint8_t i;
	char data[LCD_BUFFER_SIZE];
	uint8_t len = vsprintf(data,str,arg);
	va_end(arg);
	
	if(row == 0){
		lcd_write_data(0x80,LCD_COMMAND);
	}
	else{
		lcd_write_data(0xC0,LCD_COMMAND);
	}
	
	for(i = 0 ; i < len ; i++){
		lcd_write_data((uint8_t)(data[i]),LCD_DATA);
	}
}//*/

void lcd_init(){
	LCD_PORTS_INIT;
	
	LCD_RS_LOW;
	_delay_ms(20);// wait for lcd to power up

	lcd_write_init_command(0x30);
	_delay_ms(5);
	
	lcd_write_init_command(0x30);
	_delay_us(200);
	
	lcd_write_init_command(0x30);
	_delay_ms(200);
	
	lcd_write_init_command(0x20);
	_delay_ms(5);
	
	lcd_write_data(0x28,LCD_COMMAND); // set interface
	//lcd_write_data(0x08,LCD_COMMAND); // enable cursor
	lcd_write_data(0x01,LCD_COMMAND); // clear and home
	lcd_write_data(0x06,LCD_COMMAND); // set cursor move dir
	lcd_write_data(0x0F,LCD_COMMAND); // clear and home
	lcd_write_data(0x0C,LCD_COMMAND); // disable cursor

}//*/

/* Tak umiera król :(
 * avrdude: verifying ...
 * avrdude: verification error, first mismatch at byte 0x0000
 * 			0x12 != 0x00
 * avrdude: safemode: lfuse changed! Was e4, and is now 0
 * Would you like this fuse to be changed back? [y/n] ^C
 * jacek@jacek-K53SV ~/Projekty/atmega $ sudo ./compile_and_run8 test_current/test
 * avrdude: error: programm enable: target doesn't answer. 1
 */