#ifndef USART_H
#define USART_H

#ifndef F_CPU
	#define F_CPU 8000000
#endif

#include <avr/io.h>

#define USART_BAUD 9600
#define USART_UBRR (F_CPU/16/USART_BAUD-1)

void USART_init();

//0-no, 1-yes
uint8_t USART_isReadyToRead();
uint8_t USART_isReadyToWrite();

void USART_writeByte(uint8_t c);
void USART_writeStr(char* str, uint8_t len);
void USART_writeStdStr(char* str);

uint8_t USART_readByte();
void USART_readStr(char* str, uint8_t len);
void USART_readStdStr(char* str);

#endif