/* zadaniem jest tylko wysyłanie na podany adres danych z sensorów */
/* 7 bajt to ilość max_rt poprzednich (nieudanych wysłań)
 * ostatnie 6 bajtów payloadu to temperatury:
 * X X X ~ failures tint1 treal1 t2int t2real */

//sensor1rom 0x8c0055d7ec628
//sensor2rom 0xa20055da0bb28

#define F_CPU 8000000

#define DEBUG

#ifdef DEBUG
	#include <stdio.h>
	#include "usart.h"
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>

#include "LCD.h"
#include "rfm70.h"
#include "spi.h"

#define PAYLOAD_LENGTH 32
#define SENSORS 2
#define CHANNEL 5

#define MAX_FAILURES 250
#define MAX_RECEIVE_FAILURES 5

#define LED_RED		2
#define LED_GREEN	3

#define SOUND_PORT	4
#define SOUND_OFF	0
#define SOUND_ON	1

#define WATER_TEMP_TRIGGER	85

#ifdef DEBUG
	char buff[150];
	uint8_t debug[5]; //for rfm
#endif

uint64_t roms[SENSORS];

struct DS18B20_Temperature{
	int8_t tempInt;
	uint16_t tempReal;
};

void configureRadio(){
	uint8_t data[5];
	
	// features enabled
	data[0] = 0x73;
	RFM70_writeCommand(ACTIVATE,data,1);
	CE_ONE;
	
	RFM70_setBank(0);
	// configure to rx  + crc2
	data[0] = 0b00001111;
	RFM70_writeRegister(0x00,data,1);
	
	//chanel 5
	data[0] = CHANNEL;
	RFM70_writeRegister(0x05,data,1);
	
	// TX/RX0 address
	uint8_t addr[] = {0xe7,0xe7,0xe7,0xe7,0xe7}; // 0xe7
	RFM70_writeRegister(0x0A,addr,5);
	RFM70_writeRegister(0x10,addr,5);
	
	
	//data[0] = 0b00111111; // 2Mb/s for rfm 70
	data[0] = 0b00110111; // 1Mb/s for rfm 70
	
	RFM70_writeRegister(0x06,data,1);
	
	//enable 32 rx_pr_p0
	data[0] = 32;
	RFM70_writeRegister(0x11,data,1);
	
	#ifdef DEBUG
		RFM70_readRegister(0x00,data,1);
		sprintf(buff,"CONFIG = %d\n\r",data[0]);
		USART_writeStdStr(buff);
		
		RFM70_readRegister(0x06,data,1);
		sprintf(buff,"RF_DR (speed) = %d\n\r",(data[0] & 0b00001000) >> 3);
		USART_writeStdStr(buff);
		
		RFM70_readRegister(0x0A,data,5);
		sprintf(buff,"RX_ADDR_0 = %x:%x:%x:%x:%x\n\r",data[0],data[1],data[2],data[3],data[4]);
		USART_writeStdStr(buff);
		
		RFM70_readRegister(0x10,data,5);
		sprintf(buff,"TX_ADDR = %x:%x:%x:%x:%x\n\r",data[0],data[1],data[2],data[3],data[4]);
		USART_writeStdStr(buff);
		
		RFM70_readRegister(0x05,data,1);
		sprintf(buff,"RF_CH = %d\n\r",data[0]);
		USART_writeStdStr(buff);
	#endif
}

void ledOn(uint8_t color){
	PORTD &= ~_BV(color);
}

void ledOff(uint8_t color){
	PORTD |= _BV(color);
}

void ledInit(){
	DDRD |= _BV(2) | _BV(3);
	ledOff(LED_RED);
	ledOff(LED_GREEN);
}

void ledReverse(uint8_t color){
	if((PIND & _BV(color)) == 0)
		ledOff(color);
	else
		ledOn(color);
}

void toggleSound(uint8_t state){
	if(state == SOUND_ON){
		PORTD |= _BV(SOUND_PORT);
	}else{
		PORTD &= ~_BV(SOUND_PORT);
	}
}

void buzzerInit(){
	DDRD |= _BV(4);
	toggleSound(SOUND_OFF);
}



void initSystem(){
	_delay_ms(500);
	#ifdef DEBUG
		USART_init();
		USART_writeStdStr("init!\r\n");
	#endif
	
	ledInit();
	ledOn(LED_RED);
	
	buzzerInit();
	
	lcd_init();
	lcd_write_str(0,"INIT");
	
	SPI_master_init();
	RFM70_init();
	configureRadio();
	
	lcd_write_str(0,"INIT OK");
	
	ledOff(LED_RED);
	ledOn(LED_GREEN);
}

int main(){
	struct DS18B20_Temperature temp1,temp2;
	uint8_t payload[PAYLOAD_LENGTH] ;
	uint8_t pipeNumber; //rfm sets pipe it reads from
	uint8_t failures, i, receiveFailures = 0;
	#ifdef DEBUG
	uint32_t ratioOk = 0,ratioFail = 0, wrongPacket = 0;
	#endif
	
	temp1.tempInt = 0;
	temp2.tempInt = 0;
	temp1.tempReal = 0;
	temp2.tempReal = 0;
	
	initSystem();
	
	for(;;){
		failures = 0;
		while(RFM70_isRXDataReady() != 1){
			if(failures++ > MAX_FAILURES){
				receiveFailures++;
				break;
			}
			_delay_ms(15);
		}
		
		if(failures > MAX_FAILURES && receiveFailures > MAX_RECEIVE_FAILURES){
			//fail state
			#ifdef DEBUG
				ratioFail++;
				//USART_writeStdStr("Packet receive failed!\r\n");
			#endif
			ledOff(LED_GREEN);
			ledOn(LED_RED);
			for(i = 0 ; i < 50 ; i++){
				ledReverse(LED_RED);
				//toggleSound(SOUND_ON);
				_delay_ms(15);
				//toggleSound(SOUND_OFF);
				_delay_ms(15);
			}
			ledOn(LED_RED);
			lcd_write_str(0,"Woda =  N/A%cC  ",223);
			lcd_write_str(1,"Piec =  N/A%cC  ",223);
		}
		else{
			
			if(failures <= MAX_FAILURES)
				receiveFailures = 0;
			
			ledOff(LED_RED);
			ledOn(LED_GREEN);
			//ok state
			
			if(RFM70_readPayload(&pipeNumber,payload,PAYLOAD_LENGTH) != PAYLOAD_LENGTH){
				#ifdef DEBUG
					//USART_writeStdStr("Wrong packet!");
				#endif
				failures = MAX_FAILURES + 1; // > 5
			}
			#ifdef DEBUG
				else{
					/*sprintf(buff,"Packet Read! Length = %d , Pipe = %d Payload:\r\n", PAYLOAD_LENGTH, 0);
					USART_writeStdStr(buff);
					uint8_t debugi;
					for(debugi = 0 ; debugi < 32 ; debugi++){
						sprintf(buff,"%d,",payload[debugi]);
						USART_writeStdStr(buff);
					}
					USART_writeStdStr("\r\n");*/
				}
			#endif
			
			#ifdef DEBUG
				ratioOk++;
				if(payload[0]!=0){
					wrongPacket++;
					//sprintf(buff,"Packet Read! Length = %d , Pipe = %d Payload:\r\n", PAYLOAD_LENGTH, 0);
					//USART_writeStdStr(buff);
					uint8_t debugi;
					for(debugi = 0 ; debugi < 32 ; debugi++){
						//sprintf(buff,"%d,",payload[debugi]);
						//USART_writeStdStr(buff);
					}
					//sprintf(buff,"ReceiveFail/WrongPacket/OKPacket = %ld/%ld/%ld\r\n\r\n",ratioFail,wrongPacket,ratioOk);
					//USART_writeStdStr(buff);
				}
				//USART_writeStdStr("Packet received! ");
			#endif
			
			if(payload[0]==0){
				temp1.tempInt = payload[PAYLOAD_LENGTH - 6];
				temp2.tempInt = payload[PAYLOAD_LENGTH - 3];
				
				temp1.tempReal = payload[PAYLOAD_LENGTH - 5] << 8 | payload[PAYLOAD_LENGTH - 4];
				temp2.tempReal = payload[PAYLOAD_LENGTH - 2] << 8 | payload[PAYLOAD_LENGTH - 1];
			}
			if(temp1.tempInt == -128) // error temp
				lcd_write_str(0,"Woda =  N/A%cC  ",223);
			else
				lcd_write_str(0,"Woda = %3d.%-1d%cC",temp1.tempInt,temp1.tempReal/1000,223);
			
			if(temp2.tempInt == -128)
				lcd_write_str(1,"Piec =  N/A%cC  ",223);
			else
				lcd_write_str(1,"Piec = %3d.%-1d%cC",temp2.tempInt,temp2.tempReal/1000,223);
				
			sprintf(buff,"water=%d.%d\r\n",temp1.tempInt,temp1.tempReal/1000);
			USART_writeStdStr(buff);
			sprintf(buff,"heater=%d.%d\r\n",temp2.tempInt,temp2.tempReal/1000);
			USART_writeStdStr(buff);
			//BUZZER
			// temp1 must be heater temp
			if(temp1.tempInt >= WATER_TEMP_TRIGGER){
				for(i = 0 ; i < 50 ; i++){
					ledReverse(LED_GREEN);
					toggleSound(SOUND_ON);
					_delay_ms(15);
					toggleSound(SOUND_OFF);
					_delay_ms(15);
				}
				ledOn(LED_GREEN);
			}
			
		}//*/
		#ifdef DEBUG
			//sprintf(buff,"ReceiveFailures/Fail/WrongPacket/OKPacket = %d/%ld/%ld/%ld\r\n",receiveFailures,ratioFail,wrongPacket,ratioOk);
			//USART_writeStdStr(buff);
		#endif
	}
	return 0;
}