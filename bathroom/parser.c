#include "parser.h"
#include "project_config.h"

inline void sendPacketBegin(){
	uint8_t i;
	//1. indicate unknown length
	USART_writeByte('0');
	USART_writeStdStr(",");
	//2. send this on address
	USART_writeByte(RECEIVER_ADDRESS+'0');
	USART_writeStdStr(",");
	//3. send address
	USART_writeByte(DEVICE_ADDRESS+'0');
	USART_writeStdStr(",");
	//4. send name len
	USART_writeByte(DEVICE_NAME_LEN+'0');
	USART_writeStdStr(",");
	//5. send name
	for(i = 0 ; i < DEVICE_NAME_LEN; i++){
		USART_writeByte(DEVICE_NAME[i]);
		USART_writeStdStr(",");
	}
}

void sendArray(uint8_t* array, uint8_t rows, uint8_t cols){
	uint8_t i,j;
	
	USART_writeByte(rows+'0');
	USART_writeStdStr(",");
	
	for(i = 0 ; i < rows ; i++){
		
		uint8_t nameLen = array[i*cols + 0];
		USART_writeByte(nameLen+'0');
		USART_writeStdStr(",");
		
		for(j = 1; j < 1 + nameLen; j++){
			USART_writeByte(array[i * cols + j]);
			USART_writeStdStr(",");
		}
		
		j = nameLen + 1;
		uint8_t paramType = array[i * cols + j];
		USART_writeByte(paramType+'0');
		USART_writeStdStr(",");
		
		if(paramType == 0xFF){ // enum type
			j+=1;
			uint8_t enumLen = array[i * cols + j];
			USART_writeByte(enumLen+'0');
			USART_writeStdStr(",");
			
			j+=1;
			uint8_t k,l;
			//for each enum val
			for(k = 0 ; k < enumLen ; k++){
				uint8_t currEnumLen = array[i * cols + j];
				USART_writeByte(currEnumLen+'0');
				USART_writeStdStr(",");
				j+=1;
				for(l = 0 ; l < currEnumLen ; l++){
					USART_writeByte(array[i * cols + j]);
					USART_writeStdStr(",");
					j+=1;
				}
			}
		}
		else{
			j += 1;
			uint8_t end = j + paramType * 2;
			for(; j < end ; j++){
				USART_writeByte(array[i * cols + j]+'0');
				USART_writeStdStr(",");
			}
		}
	}
}

void sendParams(){
	sendPacketBegin();
	sendArray(&DATA_CONFIG[0][0],DATA_CONFIG_LENGTH, DATA_CONFIG_MAX_LENGTH);
	sendArray(&PARAMS_CONFIG[0][0],PARAMS_CONFIG_LENGTH, PARAMS_CONFIG_MAX_LENGTH);
}

//[0,0,2,8,B,a,t,h,r,o,o,m,6,8,H,u,m,i,d,i,t,y,1,5,7,;,T,e,m,p,e,r,a,t,u,r,e,1,6,8,9,F,a,n, ,s,t,a,t,e,/,2,2,o,n,3,o,f,f,6,T,o,i,l,e,t,2,0,1,2,3,5,L,i,g,h,t,2,4,5,6,7,9,T,e,m,p, ,m,e,a,n,1,8,9,6,9,F,a,n, ,s,t,a,r,t,1,0,N,8,F,a,n, ,s,t,o,p,1,0,N,:,U,s,e,r, ,s,t,a,t,e,/,3,4,a,u,t,o,2,o,n,3,o,f,f,A,T,o,i,l,e,t, ,s,e,c,o,n,d,s, ,o,n,1,0,/,@,T,o,i,l,e,t, ,t,h,r,e,s,h,o,l,d,2,0,0,3,/,?,L,i,g,h,t, ,t,h,r,e,s,h,o,l,d,2,0,0,3,/,]