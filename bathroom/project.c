#define DEBUG
#define F_CPU 8000000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>

#include "LCD.h"
#include "dht22.h"
#include "rfm69hw.h"
#include "eeprom.h"
#include "register.h"

#ifdef DEBUG
#include "usart.h"
#endif

//LIGHT SENSOR
#define LIGHT_DDR	DDRC
#define LIGHT_PORT	PORTC
#define LIGHT_PIN	PINC
#define LIGHT_NO	1

//TOILET SENSOR
#define TOILET_DDR	DDRC
#define TOILET_PORT	PORTC
#define TOILET_PIN	PINC
#define TOILET_NO	0

//FAN
#define FAN_DDR		DDRD
#define FAN_PORT	PORTD
#define FAN_PIN		PIND
#define FAN_NO		7

#define FAN_ON		{FAN_PORT |= _BV(FAN_NO);}
#define FAN_OFF		{FAN_PORT &= ~_BV(FAN_NO);}

#define STATE_ON	1
#define STATE_OFF	0

#define USER_STATE_ON	1
#define USER_STATE_OFF	2

//DEBUG
#ifdef DEBUG
char debug_buffer[64];
#endif

//LOGIC VARIABLES
uint8_t currentStateFan = 0;
volatile uint8_t secondPassed = 0;

volatile uint8_t loopTimePassed = 0; // for radio operation

volatile uint8_t timer2iterator = 0;
volatile uint8_t timer2iteratorSec = 0;
#define TIMER2_MAX_VAL 130
#define TIMER2_MAX_ITERATIONS 60
#define TIMER2_SECONDS 1 // seconds between sensors readings and sending data

uint16_t meanLength = 1000;
float longMean;
int8_t dewPoint;
uint8_t fanStartDiffVal = 7;
uint8_t fanStopDiffVal = 9;
uint8_t userFanState = 0;

//LIGHT SENSOR
uint16_t lightADCThreshold = 512;
uint16_t lightADCCurrentValue = 0xFFFF;
//FORCE SENSOR
uint8_t toiletSecondsOn = 60;
volatile uint8_t toiletTimeOnLeft = 0;
volatile uint8_t toiletOn = 0;

uint16_t toiletADCThreshold = 500; // empirical...
uint16_t toiletADCcurrentValue = 0xFFFF;

//HUM-TEMP SENSOR
struct DHT22Data dht22Data;

//RFM
uint8_t packet[RFM69_PACKET_MAX_SIZE];
uint8_t packetNo = 0;
uint8_t packetReceived = RFM69_PACKET_TYPE_NO_PACKET;

uint8_t deviceRegistered = 0;

/*****************LEDS******************/
inline void lcd_led_on()
{
	PORTD |= _BV(4);
}

inline void lcd_led_off()
{
	PORTD &= ~_BV(4);
}

inline void tx_led_on()
{
	PORTB &= ~_BV(6);
}

inline void tx_led_off()
{
	PORTB |= _BV(6);
}

inline void rx_led_on()
{
	PORTB &= ~_BV(7);
}

inline void rx_led_off()
{
	PORTB |= _BV(7);
}

/*****************FAN******************/
void fan_start()
{
	FAN_ON;
	currentStateFan = STATE_ON;
}//*/

void fan_stop()
{
	FAN_OFF;
	currentStateFan = STATE_OFF;
}//*/

void checkToilet()
{
	ADMUX &= ~_BV(MUX0) | ~_BV(MUX1) | ~_BV(MUX2); // ustawienie z którego ADC czytamy (0)
	ADCSRA |= _BV(ADSC);
	while( ADCSRA & _BV(ADSC) );
	
	toiletADCcurrentValue = ADC;
	
	if(toiletADCcurrentValue > toiletADCThreshold)
	{
		toiletOn = 1;
		toiletTimeOnLeft = toiletSecondsOn;
	}
}//*/

void checkLight()
{
	ADMUX |= _BV(MUX0); // ustawienie z którego ADC czytamy (1)
	ADCSRA |= _BV(ADSC);
	while( ADCSRA & _BV(ADSC) );
	
	lightADCCurrentValue = ADC;
	if(lightADCCurrentValue >= lightADCThreshold)
		lcd_led_on();
	else
		lcd_led_off();
}

/*****************LOGIC******************/
void update_long_mean()
{
	longMean = ((meanLength - 1.0f) * longMean + dht22Data.temperature) / (meanLength * 1.0f);
}//*/

void update_dew_point()
{
	/*	dewPoint = powf(humidity*0.01f,0.125f) *
			(112 + (0.9f * temperature)) +
			0.1f*temperature - 112;*/
	dewPoint = dht22Data.temperature - (100 - dht22Data.humidity) / 5.0f;
}//*/

uint8_t checkOnCondition()
{
	// user state
	if(userFanState == USER_STATE_ON)
	{
		return 1;
	}
	else if(userFanState == USER_STATE_OFF)
	{
		return 0;
	}
	
	// force sensor
	if(toiletOn || toiletTimeOnLeft > 0)
	{
		return 1;
	}
	
	// humidity sensor
	if((int8_t)longMean - fanStartDiffVal <= dewPoint )  // bezwzględne włączenie
	{
		return 1;
	}
	else if((int8_t)longMean - fanStopDiffVal >= dewPoint )  // bezwzględne wyłączenie
	{
		return 0;
	}
	else  // ON/OFF warunkowy (przedział)
	{
		if(currentStateFan == STATE_ON)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
}//*/

/*****************"GUI"******************/
#ifdef DEBUG
void showDebug()
{
	//sprintf(debug_buffer,"long mean = %d.%d\n\r",(int8_t)longMean,(int8_t)((longMean-(int8_t)longMean))*100);
	//USART_writeStdStr(debug_buffer);
	//USART_writeStdStr("\n\n\r");
	USART_writeStdStr("i\r\n");
}//*/
#endif

void showLCD()
{
	uint8_t dif = (currentStateFan == STATE_ON && !toiletOn) ?
				  dewPoint - ((int8_t)longMean - fanStopDiffVal) :
				  (int8_t)longMean - fanStartDiffVal - dewPoint;
				  
	cli();
	if(userFanState == USER_STATE_ON || userFanState == USER_STATE_OFF)
	{
		char *str = "USER=           ";
		itoa(userFanState, str + 5, 10);
		uint8_t i;
		for(i = 0 ; i < 16 ; i++)
		{
			if(str[i] == '\0')
				str[i] = ' ';
		}
		lcd_write_std_str(0, str, 16);
	}
	else
	{
		char *str = "TS=  HS=  DIF=  ";
		itoa(toiletOn, str + 3, 10);
		itoa(currentStateFan, str + 8, 10);
		itoa(dif, str + 14, 10);
		uint8_t i;
		for(i = 0 ; i < 16 ; i++)
		{
			if(str[i] == '\0')
				str[i] = ' ';
		}
		lcd_write_std_str(0, str, 16);
	}
	
	char *str = "T=   H=   DP=   ";
	itoa(dht22Data.temperature, str + 2, 10);
	itoa(dht22Data.humidity, str + 7, 10);
	itoa(dewPoint, str + 13, 10);
	uint8_t i;
	for(i = 0 ; i < 16 ; i++)
	{
		if(str[i] == '\0')
			str[i] = ' ';
	}
	lcd_write_std_str(1, str, 16);
	
	sei();
}

/*****************INIT******************/
void init_adc()
{
	ADCSRA |= _BV(ADEN) // włączenie ADC
			  | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0); // preskaler na 128
}//*/

void init_timers()
{
	//set timer2 for one second interrupt
	OCR2 = TIMER2_MAX_VAL; // ctc compare value
	TIMSK |= _BV(OCIE2); // enable interrupt on compare
	TCCR2 |= _BV(CS22) | _BV(CS21) | _BV(CS20); // set prescaler 1024
	TCCR2 |= _BV(WGM21); // set CTC mode
}

/******************RADIO****************/

void changeConfig()
{
	/**
	 * packet[0]	// packet length
	 * packet[1]	// receiver address
	 * packet[2]	// packet type
	 * packet[3]	// new forced fan state
	 * packet[4]	// new fanStartDiffVal
	 * packet[5]	// new fanStopDiffVal
	 * packet[6]	// new force sensor threshold H
	 * packet[7]	// new force sensor threshold L
	 * packet[8]	// new toiletTimeSecondsOn (0-255)
	 * packet[9]	// new light sensor threshold H
	 * packet[10]	// new light sensor threshold L
	 * packet[11]	// crc (sum of all previous)
	**/
	
	USART_writeStdStr("changing config...\r\n");
	
	userFanState = packet[3];
	save_variable_byte(userFanState, USER_FAN_STATE_EEPROM_ADDR);
	
	fanStartDiffVal = packet[4];
	save_variable_byte(fanStartDiffVal, FAN_START_DIFF_EEPROM_ADDR);
	
	fanStopDiffVal = packet[5];
	save_variable_byte(fanStopDiffVal, FAN_STOP_DIFF_EEPROM_ADDR);
	
	toiletADCThreshold = ((uint16_t)packet[6]) << 8;
	toiletADCThreshold |= packet[7];
	save_variable_word(toiletADCThreshold, TOILET_ADC_EEPROM_ADDR);
	
	toiletSecondsOn = packet[8];
	save_variable_byte(toiletSecondsOn, TOILET_SECONDS_ON_EEPROM_ADDR);
	
	lightADCThreshold = ((uint16_t)packet[9]) << 8;
	lightADCThreshold |= packet[10];
	save_variable_word(toiletADCThreshold, TOILET_ADC_EEPROM_ADDR);
}

void radioOperationSend()
{
	USART_writeStdStr("packetReceived radio send=");
	USART_writeByte('0'+packetReceived);
	USART_writeStdStr("\r\n");
	
	uint8_t packetLength = 14;
	
	if(deviceRegistered == 1)
	{
		//prepare packet for send
		packet[0] = packetLength;
		packet[1] = RFM69_BROADCAST_ADDRESS;	// send broadcast
		packet[2] = RFM69_NODE_ADDRESS;			// send this node address
		packet[3] = RFM69_PACKET_TYPE_DATA;
		packet[4] = packetNo++;					// count packets
		packet[5] = packetReceived;				// send if ack was received
		packet[6] = dht22Data.humidity;			// send humitity
		packet[7] = dht22Data.temperature;		// send temperature
		packet[8] = toiletADCcurrentValue >> 8;	// send toilet adc msb
		packet[9] = toiletADCcurrentValue;		// adc lsb
		packet[10] = currentStateFan;			// send state fan
		packet[11] = lightADCCurrentValue >> 8;	// send light value msb
		packet[12] = lightADCCurrentValue;		// send light value lsb
		packet[13] = 0; 						// for crc
		
		//crc
		uint8_t i = 0;
		for(; i < packetLength - 1 ; i++)
		{
			packet[packetLength - 1] += packet[i];
		}
		
		//send packet
		tx_led_on();
		cli();
		
		rfm69_waitForFreeBand();
		wdt_reset();
		
		rfm69_setMode(RFM69_MODE_TX);
		rfm69_waitForModeSet();
		wdt_reset();
		
		rfm69_sendPacket(packet, RFM69_PACKET_MAX_SIZE);
		
		while(!rfm69_checkPacketSent()); // wait for send
		wdt_reset();
		tx_led_off();
	}
	else
	{
		rfm69_waitForFreeBand();
		wdt_reset();
		
		tx_led_on();
		
		rfm69_setMode(RFM69_MODE_TX);
		rfm69_waitForModeSet();
		wdt_reset();
		
		deviceRegistered = registerDevice(packetReceived);
		
		tx_led_off();
	}
	
	//reset packet received
	packetReceived = RFM69_PACKET_TYPE_NO_PACKET;
	
	//receive ack
	rfm69_setMode(RFM69_MODE_RX);
	rfm69_waitForModeSet(); // wait for mode
	wdt_reset();
	
	TCNT2 = 0;
	timer2iterator = 0;
	timer2iteratorSec = 0; //zerujemy timer, żeby zyskać na czasie
	sei();
}

void radioOperationReceive()
{
	rx_led_on();
	
	cli();
	rfm69_receivePacket(packet);
	sei();
	
	//check crc
	uint8_t crc = 0;
	uint8_t i;
	for(i = 0 ; i < packet[0] - 1; i++)
	{
		crc += packet[i];
	}
	
	if(crc != packet[packet[0]-1])
	{
		packetReceived = RFM69_PACKET_TYPE_NO_PACKET;
	}
	else
	{
		packetReceived = packet[2];
		if(packetReceived == RFM69_PACKET_TYPE_CONFIG && deviceRegistered != 0)
		{
			changeConfig();
		}
	}
	rx_led_off();
	
	USART_writeStdStr("packetReceived radio receive=");
	USART_writeByte('0'+packetReceived);
	USART_writeStdStr("\r\n");
	
	rfm69_setMode(RFM69_MODE_FS);
}

void init_variables()
{
	init_variable_byte(&fanStartDiffVal, FAN_START_DIFF_EEPROM_ADDR);
	init_variable_byte(&fanStopDiffVal, FAN_STOP_DIFF_EEPROM_ADDR);
	init_variable_byte(&userFanState, USER_FAN_STATE_EEPROM_ADDR);
	init_variable_byte(&toiletSecondsOn, TOILET_SECONDS_ON_EEPROM_ADDR);
	init_variable_word(&toiletADCThreshold, TOILET_ADC_EEPROM_ADDR);
}

void init_system()
{
	wdt_enable(WDTO_1S); // watchdog enable
	
#ifdef DEBUG
	USART_init();
	USART_writeStdStr("\r\ninit\r\n");
#endif
	
	PORTD &= ~_BV(5); //power on led
	lcd_led_on();
	
	//led init
	DDRD |= _BV(4) | _BV(5); // lcd led | power on
	DDRB |= _BV(6) | _BV(7); //tx/rx
	
	PORTB &= ~_BV(6) | ~_BV(7); //test rx/tx
	_delay_ms(500);
	PORTB |= _BV(6) | _BV(7); //test
	
	wdt_reset();
	
	lcd_init();
	lcd_write_std_str(0, "init", 4);
	
	init_variables();
	
	sei();
	init_adc();
	init_timers();
	init_sensor(&dht22Data);
	longMean = dht22Data.temperature;
	
	FAN_DDR |= _BV(FAN_NO);		// wyjście tranzystora
	fan_start();				// włączenie testowe
	wdt_reset();
	_delay_ms(800);
	wdt_reset();
	fan_stop();					// wyłączenie wiatraka
	_delay_ms(100);
	
	//USART_writeStdStr("rfm1\r\n");
	rfm69_init();
	wdt_reset();
	//USART_writeStdStr("rfm2\r\n");
	rfm69_setMode(RFM69_MODE_RX);
	wdt_reset();
	//USART_writeStdStr("rfm3\r\n");
	rfm69_waitForModeSet();
	wdt_reset();
	//USART_writeStdStr("rfm4\r\n");
}//*/

int main()
{
	wdt_enable(WDTO_1S); // watchdog enable
	init_system();
	
	for(;;)
	{
		///////////////////
		if (checkOnCondition())
			fan_start();
		else
			fan_stop();
		wdt_reset();
		///////////////////
		checkToilet();
		checkLight();
		///////////////////
		if(secondPassed)
		{
			secondPassed = 0;
			if(toiletOn)
			{
				if(--toiletTimeOnLeft == 0)
				{
					toiletOn = 0;
				}
			}
			
			if((read_sensor(&dht22Data)))
			{
			
				update_long_mean();
				update_dew_point();
			}
			
#ifdef DEBUG
			showDebug();
#endif
			showLCD();
		}
		wdt_reset();
		
		//radio operation may be less often
		if(loopTimePassed)
		{
			loopTimePassed = 0;
			radioOperationSend();
		}
		wdt_reset();
		
		if(rfm69_checkPacketReceived() && (rfm69_getMode() == RFM69_MODE_RX))
		{
			radioOperationReceive();
		}
		wdt_reset();
		
	}//*/
}

/*************INTERRUPTS*****************/

/*ISR(TIMER0_OVF_vect){
}*/

//~~one second interrupt
ISR(TIMER2_COMP_vect)
{
	if(++timer2iterator >= TIMER2_MAX_ITERATIONS)
	{
		timer2iterator = 0;
		secondPassed = 1;
		if(++timer2iteratorSec >= TIMER2_SECONDS)
		{
			timer2iteratorSec = 0;
			loopTimePassed = 1;
		}
	}
}//*/