#ifndef EEPROM_H
#define EEPROM_H

#ifndef F_CPU
#define F_CPU 8000000
#endif

#include <avr/eeprom.h>

#define FAN_START_DIFF_EEPROM_ADDR		0x00
#define FAN_STOP_DIFF_EEPROM_ADDR		0x01
#define USER_FAN_STATE_EEPROM_ADDR		0x02
#define TOILET_SECONDS_ON_EEPROM_ADDR	0x03
#define TOILET_ADC_EEPROM_ADDR			0x04 // 2 bytes
#define LIGHT_ADC_EEPROM_ADDR			0x06 // 2 bytes

void init_variable_byte(uint8_t *variable, uint8_t address);
void save_variable_byte(uint8_t variable, uint8_t address);

void init_variable_word(uint16_t *variable, uint8_t address);
void save_variable_word(uint16_t variable, uint8_t address);

#endif