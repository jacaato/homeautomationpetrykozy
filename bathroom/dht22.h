/**
 * this code uses timer1 and ICP1 pin for sensor reading
 * you cannot use this timer/pin in your application
 **/
#ifndef DHT11_H
#define DHT11_H

#ifndef F_CPU
#define F_CPU 8000000
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/delay.h>

#define SET_ZERO_OUT_SENSOR		{ DDRB |=  _BV(0); PORTB &= ~_BV(0); }
#define SET_IN_SENSOR			{ DDRB &= ~_BV(0); PORTB |=  _BV(0); }

#define SET_INT_RISING_EDGE		{ TCCR1B |= _BV(ICES1); }
#define SET_INT_FALLING_EDGE	{ TCCR1B &= ~_BV(ICES1); }

#define DHT_RESPONSE_SIZE 42

//lengths of states
#define ONE_LENGTH	70
#define ZERO_LENGTH	20
#define DELTA		15 //dispersion of states

struct DHT22Data
{
	int8_t temperature;
	uint8_t humidity;
};

//this turns on intrrupts!
void init_sensor(struct DHT22Data *data);
//read sensor up to one time per second
uint8_t read_sensor(struct DHT22Data *data);

#endif
