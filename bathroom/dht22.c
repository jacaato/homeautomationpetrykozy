#include "dht22.h"

enum DHT22State
{
	risingEdge, fallingEdge
} dhtSignalState;

struct DHT22Response
{
	uint8_t humH;
	uint8_t humL;
	uint8_t tempH;
	uint8_t tempL;
	uint8_t crc;
};

volatile uint8_t sensorTimesArray[DHT_RESPONSE_SIZE];
volatile uint8_t sensorTimesIterator;

uint8_t interpret_bit(uint8_t time)
{
	if((ZERO_LENGTH - DELTA <= time) && (ZERO_LENGTH + DELTA >= time))
		return 0;
	if((ONE_LENGTH - DELTA <= time) && (ONE_LENGTH + DELTA >= time))
		return 1;
	return 2;
}

uint8_t read_field(uint8_t *field, uint8_t fieldNumber)
{
	uint8_t i;
	for(i = fieldNumber * 8 + 2; i < fieldNumber * 8 + 8 + 2; i++)
	{
		switch(interpret_bit(sensorTimesArray[i]))
		{
		case 0:
			(*field) &= ~_BV(7 - ((i - 2) % 8));
			break;
		case 1:
			(*field) |= _BV(7 - ((i - 2) % 8));
			break;
		case 2:
			return 0;
		}
	}
	return 1;
}

uint8_t read_sensor(struct DHT22Data *data)
{
	struct DHT22Response dht22Response;
	
	sensorTimesIterator = 0;
	dhtSignalState = fallingEdge;
	SET_INT_FALLING_EDGE;
	TCCR1B |= _BV(CS11); // włączenie tim1 z preskalerem 8
	
	SET_ZERO_OUT_SENSOR;
	_delay_ms(10);
	SET_IN_SENSOR;
	
	sei();
	TIMSK |= _BV(TICIE1); // ustawienie przerwania na icp1
	
	while(sensorTimesIterator < DHT_RESPONSE_SIZE)
	{
		// wait for interrupt to finish
	}
	TIMSK &= ~_BV(TICIE1); // wyłączenie przerwania na icp1
	
	if(!read_field(&(dht22Response.humH), 0))
		return 0;
	if(!read_field(&(dht22Response.humL), 1))
		return 0;
	if(!read_field(&(dht22Response.tempH), 2))
		return 0;
	if(!read_field(&(dht22Response.tempL), 3))
		return 0;
	if(!read_field(&(dht22Response.crc), 4))
		return 0;
	
	if((uint8_t)(dht22Response.humH + dht22Response.humL
				 + dht22Response.tempH + dht22Response.tempL)
			!= dht22Response.crc)
	{
		return 0;
	}
	else
	{
		uint16_t humidity = 0;
		uint16_t temperature = 0;
		uint8_t sign = 0;
		
		humidity |= ((uint16_t)dht22Response.humH) << 8;
		humidity |= dht22Response.humL;
		humidity /= 10;
		data->humidity = humidity;
		
		sign = dht22Response.tempH & 0b10000000;
		dht22Response.tempH &= 0b01111111;
		
		temperature |= ((uint16_t)dht22Response.tempH) << 8;
		temperature |= dht22Response.tempL;
		temperature /= 10;
		data->temperature = temperature;
		if(sign)
			data->temperature *= (-1);
			
		return 1;
	}
}//*/

/*****************OTHER******************/
void init_sensor(struct DHT22Data *data)
{
	sei();
	data->humidity = 0;
	while(data->humidity == 0)
	{
		read_sensor(data);
		_delay_ms(200); // must delay between readings
	}
	wdt_reset();
}//*/

//check length of signal. count high only
ISR(TIMER1_CAPT_vect)
{
	TCNT1 = 0;
	wdt_reset();
	switch(dhtSignalState)
	{
	case risingEdge:
		SET_INT_FALLING_EDGE;
		dhtSignalState = fallingEdge;
		break;
	case fallingEdge:
		sensorTimesArray[sensorTimesIterator] = ICR1L;
		sensorTimesIterator++;
		SET_INT_RISING_EDGE;
		dhtSignalState = risingEdge;
		break;
	}
}//*/