#ifndef PROJECT_H
#define PROJECT_H

// addres to send config data
#define RECEIVER_ADDRESS 0x00

// this device address
#define DEVICE_ADDRESS 0x02

//define device name
#define DEVICE_NAME_LEN 8
uint8_t DEVICE_NAME[DEVICE_NAME_LEN] = {
	'B','a','t','h','r','o','o','m'
};

// define data config
#define DATA_CONFIG_MAX_LENGTH 19
#define DATA_CONFIG_LENGTH 6
// {name_len,name,type(1,2... bytes),minval,maxval}
// {name_len,name,type(0xFF),amount,len1,name1...lenN,nameN}
uint8_t DATA_CONFIG[DATA_CONFIG_LENGTH][DATA_CONFIG_MAX_LENGTH] = {
	{8,'H','u','m','i','d','i','t','y',1,0,100},
	{11,'T','e','m','p','e','r','a','t','u','r','e',1,10,40},
	{9,'F','a','n',' ','s','t','a','t','e',0xFF,2,2,'o','n',3,'o','f','f'},
	{6,'T','o','i','l','e','t',2,0,0,3,255},
	{5,'L','i','g','h','t',2,0,0,3,255},
	{9,'T','e','m','p',' ','m','e','a','n',1,10,40}
};

// define parameters config
#define PARAMS_CONFIG_LENGTH 6
#define PARAMS_CONFIG_MAX_LENGTH 25
uint8_t PARAMS_CONFIG[PARAMS_CONFIG_LENGTH][PARAMS_CONFIG_MAX_LENGTH] = {
	{9,'F','a','n',' ','s','t','a','r','t',1,0,30},
	{8,'F','a','n',' ','s','t','o','p',1,0,30},
	{10,'U','s','e','r',' ','s','t','a','t','e',0xFF,3,4,'a','u','t','o',2,'o','n',3,'o','f','f'},
	{17,'T','o','i','l','e','t',' ','s','e','c','o','n','d','s',' ','o','n',1,0,255},
	{16,'T','o','i','l','e','t',' ','t','h','r','e','s','h','o','l','d',2,0,0,1023>>8,(uint8_t)1023},
	{15,'L','i','g','h','t',' ','t','h','r','e','s','h','o','l','d',2,0,0,1023>>8,(uint8_t)1023}
};

#endif
