#ifndef REGISTER_H
#define REGISTER_H

#define DEBUG

#define PREAMBLE_SIZE 6

#ifdef DEBUG
#include "usart.h"
#endif

#include <avr/io.h>

uint8_t registerDevice(uint8_t lastReceived);

#endif