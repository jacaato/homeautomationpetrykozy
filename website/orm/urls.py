from django.conf.urls import patterns, url
from orm import views

urlpatterns = patterns('',
	url(r'^login',views.log_in, name='login'),
	url(r'^currentdata',views.get_current_data, name='get_current_data'),
	url(r'^data',views.get_data, name='get_data'), # data for given time and device
	url(r'^configenumdata',views.get_config_enum_data, name='get_config_enum_data'),
	url(r'^configdata',views.get_config_data, name='get_config_data'),
	url(r'^datatypes',views.get_data_types, name='get_data_types'),
	url(r'^saveconfigparams',views.save_config_params, name='save_config_params'),
	url(r'^$',views.index, name='index')
)
