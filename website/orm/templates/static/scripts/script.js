secondInterval = null;
devicesKeys = [];

function onLoad()
{
	prepareSite();
	getCurrentData();
}

function enableIntervals()
{
	secondInterval = setInterval(function(){oneSecondInterval()}, 1000);
}

function disableIntervals()
{
	clearInterval(secondInterval);
}

function oneSecondInterval()
{
	getCurrentData();
}

function prepareSite()
{
	var template = '<p><span id="{0}_device_state_name">{1}</span> device state: <span id="{0}_device_state">N/A</span></p>';
	var deviceStates = document.getElementById("devices_states");
	for(var i in devices)
	{
		devicesKeys.push(i);
		child = template.format(i,devices[i]);
		deviceStates.innerHTML += child;
	}
}

function getCurrentData()
{
	disableIntervals();
	var request = new XMLHttpRequest();
	request.onreadystatechange=function()
	{
		if (request.readyState==4 && request.status==200)
		{
			var currentData = eval(request.responseText);
			updateDevicesStates(currentData);
			enableIntervals();
		}
		else if(request.readyState==4)
		{
			enableIntervals();
		}
	}
	request.open("GET","currentdata",true);
	request.send();
}

function updateDevicesStates(currentData)
{
	devicesTimesNames = {};
	length = 0;
	
	for(var i = 0 ; i < currentData.length ; i++)
	{
		var tmp = currentData[i];
		if (!devicesTimesNames[tmp.device]) {
			devicesTimesNames[tmp.device] = ({'date':tmp.date,'device_name':tmp.device_name});
			length++;
		}
	}
	
	//if devices changed, reload
	if( devicesKeys.length != length)
	{
		location.reload(true);
	}
	
	var currentDataTime = parseInt(new Date().getTime() / 1000);
	for(var i = 0 ; i < devicesKeys.length ; i++)
	{
		var deviceId = devicesKeys[i];
		var deviceStateDiv = document.getElementById(deviceId+"_device_state");
		var deviceStateNameDiv = document.getElementById(deviceId+"_device_state_name");
		//changing name
		deviceStateNameDiv.innerHTML = devicesTimesNames[deviceId]['device_name'];
		
		//changing state
		var deviceLastSeenSec = currentDataTime - parseInt(new Date(devicesTimesNames[deviceId]['date']).getTime() / 1000);
		if(deviceLastSeenSec < 10)
		{
			deviceStateDiv.innerHTML = "Connected";
			deviceStateDiv.style.color = "green";
		}
		else
		{
			deviceStateDiv.innerHTML = "Disconnected";
			deviceStateDiv.style.color = "red";
		}
	}
}

function refreshBathroomParams(){
	var request = new XMLHttpRequest();	
	
	request.onreadystatechange=function(){
		if (request.readyState==4 && request.status==200){
			var params_array = JSON.parse(request.responseText);
			
			var fan_state_param = params_array[0];
			document.getElementById("radio"+fan_state_param).checked=true;
			
			document.getElementById("fan_start_diff").value = params_array[1];
			document.getElementById("fan_stop_diff").value = params_array[2];
			document.getElementById("toilet_threshold").value = params_array[3];
			document.getElementById("toilet_seconds_on").value = params_array[4];
		}
	}
	
	request.open("GET","./php/system.php?bathroom_params=true",true);
	request.send();
}

/** formatting string **/
String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+i+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

/*
function getChartData(time){
	if(chart != null)
		chart.showLoading('Loading data from server...');
	var request = new XMLHttpRequest();
	
	request.onreadystatechange=function(){
		if (request.readyState==4 && request.status==200){
			var charts_data = eval(request.responseText);
			var dataHum = new Array();
			var dataTemp = new Array();
			for(i = 0 ; i < charts_data.length ; i++){
				var tmp = charts_data[i];
				var tmpDate = tmp.date.substr(0,10)+'T'+tmp.date.substr(11,8);
				dataHum[i] = [Date.parse(tmpDate),tmp.humidity == null ? null : parseInt(tmp.humidity)];
				dataTemp[i] = [Date.parse(tmpDate),tmp.temperature == null ? null :parseInt(tmp.temperature)];
			}
			
			if(chart == null){
				chartSettings.xAxis.events.afterSetExtremes = getChartData;
				chartSettings.navigator.series.data = dataHum;
				chartSettings.series[0].data = dataHum;
				chartSettings.series[1].data = dataTemp;
				chartSettings.exporting.buttons.customButton.onclick = getChartData;
				chart = new Highcharts.StockChart(chartSettings);
			}
			else if(chart != null && time == null)
			{
				chartSettings.navigator.series.data = dataHum;
				chart.series[0].setData(dataHum);
				chart.series[1].setData(dataTemp);
				chart.xAxis[0].setExtremes();
				chart.hideLoading();
			}
			else
			{
				chart.series[0].setData(dataHum);
				chart.series[1].setData(dataTemp);
				chart.hideLoading();
			}
		}
	}
	if(time != null)
	{
		request.open("GET","./php/system.php?chart_start="+Math.round(time.min)+"&chart_end="+Math.round(time.max),true);
	}
	else
	{
		request.open("GET","./php/system.php?chart_start="+Math.round(dateStart.getTime())+"&chart_end="+Math.round((new Date(2100)).getTime()),true);
	}
	request.send();
}

function getBathroomSensorsData(){
	var request = new XMLHttpRequest();
	
	request.onreadystatechange=function(){
		if (request.readyState==4 && request.status==200){
			var currentDateTime = parseInt(new Date().getTime() / 1000);
			
			var sensors_array = JSON.parse(request.responseText);
			var sensorsDateTimeDiff = currentDateTime - parseInt(new Date(sensors_array[0]).getTime() / 1000);
			var bathroomDeviceStateDiv = document.getElementById("bathroom_device_state");
			if(sensorsDateTimeDiff < 10){
				bathroomDeviceStateDiv.innerHTML = "Connected";
				bathroomDeviceStateDiv.style.color = "green";
			}
			else{
				document.getElementById("bathroom_device_state").innerHTML = "Disconnected";
				bathroomDeviceStateDiv.style.color = "red";
			}
			
			if(temperatureBathroomGauge != null){ //rest must be null too
				var gaugePoint = humidityBathroomGauge.series[0].points[0];
				gaugePoint.update(parseInt(sensors_array[1]));
				
				gaugePoint = temperatureBathroomGauge.series[0].points[0];
				gaugePoint.update(parseInt(sensors_array[2]));
				
				gaugePoint = toiletBathroomGauge.series[0].points[0];
				gaugePoint.update(parseInt(sensors_array[3]));
			}
			else{
				humidityBathroomGaugeSettings.series[0].data[0] = parseInt(sensors_array[1]);
				temperatureBathroomGaugeSettings.series[0].data[0] = parseInt(sensors_array[2]);
				toiletBathroomGaugeSettings.series[0].data[0] = parseInt(sensors_array[3]);
				 
				humidityBathroomGauge = new Highcharts.Chart(humidityBathroomGaugeSettings); 
				temperatureBathroomGauge = new Highcharts.Chart(temperatureBathroomGaugeSettings);
				toiletBathroomGauge = new Highcharts.Chart(toiletBathroomGaugeSettings);
			}
			var fan_state = sensors_array[4] == 1 ? "ON" : "OFF";
			document.getElementById("fan_current_state").innerHTML = fan_state;
			if(fan_state == "ON"){
				document.getElementById("fan_image").style.webkitAnimationPlayState = "running";
			}
			else{
				document.getElementById("fan_image").style.webkitAnimationPlayState = "paused";
			}
			
			var fan_state_param = sensors_array[5];
			document.getElementById("radio"+fan_state_param).checked=true;
		}
	}
	
	request.open("GET","./php/system.php?bathroom_sensors=true",true);
	request.send();
}
*/