{% autoescape off %}
<script>

devices = {{devices}};


data_config = {{data_config}};


parameters_config = {{parameters_config}};


{% if user.is_authenticated %}
loggedIn = true;
{% else %}
loggedIn = false;
{% endif %}

</script>
{% endautoescape %}
