from django.contrib import admin
import orm.models as models


admin.site.register(models.Configuration)
admin.site.register(models.ConfigurationParameterValues)
admin.site.register(models.ConfigurationParameters)
admin.site.register(models.DataCurrent)
admin.site.register(models.DataDaily)
admin.site.register(models.DataHourly)
admin.site.register(models.DataMinutely)
admin.site.register(models.DataTypeValues)
admin.site.register(models.DataTypes)
admin.site.register(models.Devices)