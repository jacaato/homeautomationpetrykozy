# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models
#from batch_select.models import BatchManager


class Configuration(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	device = models.ForeignKey('Devices', db_column='device', unique=True)
	applied = models.IntegerField()
	
	class Meta:
		managed = False
		db_table = 'configuration'


class ConfigurationParameterValues(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	configuration_parameter = models.ForeignKey('ConfigurationParameters', db_column='configuration_parameter')
	name = models.CharField(max_length=20)
	value = models.IntegerField()
	index = models.IntegerField()

	class Meta:
		managed = False
		db_table = 'configuration_parameter_values'


class ConfigurationParameters(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	configuration = models.ForeignKey(Configuration, db_column='configuration')
	name = models.CharField(max_length=20)
	type = models.IntegerField()
	index = models.IntegerField()
	value = models.IntegerField()
	
	class Meta:
		managed = False
		db_table = 'configuration_parameters'


class DataCurrent(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	data_type = models.ForeignKey('DataTypes', db_column='data_type', unique=True)
	date = models.DateTimeField()
	value = models.IntegerField()

	class Meta:
		managed = False
		db_table = 'data_current'


class DataDaily(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	data_type = models.ForeignKey('DataTypes', db_column='data_type')
	date = models.DateTimeField()
	value = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'data_daily'


class DataHourly(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	data_type = models.ForeignKey('DataTypes', db_column='data_type')
	date = models.DateTimeField()
	value = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'data_hourly'


class DataMinutely(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	data_type = models.ForeignKey('DataTypes', db_column='data_type')
	date = models.DateTimeField()
	value = models.IntegerField(blank=True, null=True)
	
	class Meta:
		managed = False
		db_table = 'data_minutely'


class DataTypeValues(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	data_type = models.ForeignKey('DataTypes', db_column='data_type')
	name = models.CharField(max_length=20)
	value = models.IntegerField()
	index = models.IntegerField()
	
	class Meta:
		managed = False
		db_table = 'data_type_values'


class DataTypes(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	device = models.ForeignKey('Devices', db_column='device')
	name = models.CharField(max_length=20)
	type = models.IntegerField()
	big_chart_id = models.IntegerField()
	big_chart_type = models.IntegerField(blank=True, null=True)
	unit = models.CharField(max_length=10)
	index = models.IntegerField()
	
	class Meta:
		managed = False
		db_table = 'data_types'


class Devices(models.Model):
	id = models.IntegerField(primary_key=True)  # AutoField?
	address = models.IntegerField(unique=True)
	name = models.CharField(max_length=20)
	
	class Meta:
		managed = False
		db_table = 'devices'
