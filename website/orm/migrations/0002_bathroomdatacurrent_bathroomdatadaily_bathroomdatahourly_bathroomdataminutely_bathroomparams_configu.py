# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orm', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BathroomDataCurrent',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('humidity', models.IntegerField()),
                ('temperature', models.IntegerField()),
                ('toilet', models.IntegerField()),
                ('fan_state', models.IntegerField()),
            ],
            options={
                'db_table': 'bathroom_data_current',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BathroomDataDaily',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('humidity', models.IntegerField(null=True, blank=True)),
                ('temperature', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'bathroom_data_daily',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BathroomDataHourly',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('humidity', models.IntegerField(null=True, blank=True)),
                ('temperature', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'bathroom_data_hourly',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BathroomDataMinutely',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('humidity', models.IntegerField()),
                ('temperature', models.IntegerField()),
                ('toilet', models.IntegerField()),
                ('fan_state', models.IntegerField()),
            ],
            options={
                'db_table': 'bathroom_data_minutely',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BathroomParams',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('approved', models.IntegerField()),
                ('user_fan_state', models.IntegerField()),
                ('fan_start_diff', models.IntegerField()),
                ('fan_stop_diff', models.IntegerField()),
                ('toilet_adc_threshold', models.IntegerField()),
                ('toilet_seconds_on', models.IntegerField()),
            ],
            options={
                'db_table': 'bathroom_params',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('applied', models.IntegerField()),
            ],
            options={
                'db_table': 'configuration',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConfigurationParameters',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('type', models.IntegerField()),
                ('index', models.IntegerField()),
            ],
            options={
                'db_table': 'configuration_parameters',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConfigurationParameterValues',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('value', models.IntegerField()),
                ('index', models.IntegerField()),
            ],
            options={
                'db_table': 'configuration_parameter_values',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataCurrent',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('value', models.IntegerField()),
            ],
            options={
                'db_table': 'data_current',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataDaily',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('value', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'data_daily',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataHourly',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('value', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'data_hourly',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataMinutely',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('value', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'data_minutely',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataTypes',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('type', models.IntegerField()),
                ('big_chart_id', models.IntegerField()),
                ('big_chart_type', models.IntegerField(null=True, blank=True)),
                ('index', models.IntegerField()),
            ],
            options={
                'db_table': 'data_types',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataTypeValues',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('value', models.IntegerField()),
                ('index', models.IntegerField()),
            ],
            options={
                'db_table': 'data_type_values',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Devices',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('address', models.IntegerField(unique=True)),
                ('name', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'devices',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Household',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('hash', models.CharField(max_length=32)),
            ],
            options={
                'db_table': 'household',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
