var chartTemplate = {
	chart:
	{
		renderTo: '', //div to render
		animation: false,
		zoomType: 'x',
		backgroundColor: '#DEEFFF',
	},
	
	scrollbar:
	{
		liveRedraw: false
	},
	
	navigator:
	{
		adaptToUpdatedData: false,
		series:
		{
			id: 'nav'
		}
		//todo hack to support multiple series on navigator
	},
	
	exporting:
	{
		buttons:
		{
			contextButton:
			{
				enabled : false
			},
			customButton:
			{
				align: 'left',
				onclick: null, // refresh data function
				symbol: 'circle',
				text: 'refresh chart'
			}
		}
	},
	rangeSelector:
	{
		allButtonsEnabled: true,
		buttons: [
			{
				type: 'day',
				count: 1,
				text: '1d'
			},
			{
				type: 'week',
				count: 1,
				text: '1w'
			},
			{
				type: 'month',
				count: 1,
				text: '1m'
			},
			{
				type: 'year',
				count: 1,
				text: '1y'
			},
			{
				type: 'all',
				text: 'All'
			},
		],
		selected: 2,
	},
	
	title:
	{
		text: '', // name 
	},
	
	xAxis:{
		type: 'datetime',
		minRange: 3600 * 1000,
		//ordinal: false,
		events:
		{
			afterSetExtremes : null
		},
	},
	 
	yAxis:{
		tickAmount : 20,
		//tickInterval: 10,
		//minTickInterval: 1,
		//minorTickInterval: 2,
		min: 0,		//minval
		max: 0,		//maxval
	},
	
	plotOptions:
	{
		series:
		{
			fillOpacity: 0.15
		}
	},
	
	series: [] //append series dictionaries
};

var chartSeriesTemplate = {
	name: '', // serie name
	id : '', //id for accessing data
	dataGrouping:
	{
		enabled: false
	},
	type: 'area',
	color: '#000000', //color
};