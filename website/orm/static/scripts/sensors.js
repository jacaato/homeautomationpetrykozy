var gaugeSettings =
{
	chart:
	{
		type: 'gauge',
		renderTo: '', // div to render
		//animation: false,
		backgroundColor: '#DEEFFF',
	},
	tooltip :
	{
		enabled : false
	},
	title:
	{
		text: '' // name
	},
	credits: false,
	exporting:
	{
		buttons:
		{
			contextButton:
			{
				enabled : false
			}
		}
	},
	pane:
	{
		startAngle: -150,
		endAngle: 150,
		background:
		[
			{
				backgroundColor:
				{
					linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
					stops:
					[
						[0, '#FFF'],
						[1, '#333']
					]
				},
				borderWidth: 0,
				outerRadius: '109%'
			},
			{
				backgroundColor:
				{
					linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
					stops:
					[
						[0, '#333'],
						[1, '#FFF']
					]
				},
				borderWidth: 1,
				outerRadius: '107%'
			}, 
			{
				// default background
			}, 
			{
				backgroundColor: '#DDD',
				borderWidth: 0,
				outerRadius: '105%',
				innerRadius: '103%'
			}
		]
	},
	yAxis: 
	{
		min: 0, // min value
		max: 0, // max value
		
		minorTickInterval: 'auto',
		minorTickWidth: 1,
		minorTickLength: 5,
		minorTickPosition: 'inside',
		minorTickColor: '#666',
		
		tickPixelInterval: 20,
		tickWidth: 2,
		tickPosition: 'inside',
		tickLength: 10,
		tickColor: '#666',
		labels:
		{
			step: 2,
			rotation: 'auto',
		},
		title:
		{
			text: '' // jednostka
		},
	},
	series:
	[
		{
			data: [0],
		}
	]
};

var gaugeEnumSettings = 
{
	chart:
	{
		type: 'gauge',
		renderTo: '', // div name
		backgroundColor: '#DEEFFF',
	},
	tooltip :
	{
		enabled : false
	},
	title:
	{
		text: '' // name
	},
	credits: false,
	exporting:
	{
		buttons:
		{
			contextButton:
			{
				enabled : false
			}
		}
	},
	plotOptions :
	{
		gauge :
		{
			dataLabels :
			{
				enabled : false,
			},
			dial :
			{
				radius: '50%',
				backgroundColor: 'silver',
				borderColor: 'black',
				borderWidth: 1,
				baseWidth: 20,
				topWidth: 10,
				baseLength: '70%', // of radius
				rearLength: '50%'
			},
		},
	},
	pane: {
		startAngle: -90,
		endAngle: 90,
		background : null
	},
	// the value axis
	yAxis: {
		min: 0, // min
		max: 4, // max
		tickLength: 0,
		categories: [], // categories names
		tickPositions: [], // positions of categories
		labels: {
			rotation: 'auto',
			formatter: function() {
				return '<div style="font-size:20px;">' + this.axis.categories[this.value - 0.5] + "</div>";
			}
		},
		plotBands: []
	},
	series: [{
		data: [0.5]
	}]
};

var gaugeEnumPlotBandSettings = {
	borderWidth : 1,
	borderColor : '#000000',
	from: 0,
	to: 1,
	outerRadius: '100%',
	innerRadius: '1%',
	color : '#E8E8E8',
}