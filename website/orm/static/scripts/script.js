var loggingIn = false;
var secondInterval = null;
var devicesKeys = [];
var gauges = [];
var charts = [];

function onLoad()
{
	prepareSite();
	getCurrentData();
}


function enableIntervals()
{
	secondInterval = setInterval(function(){oneSecondInterval()}, 1000);
}


function disableIntervals()
{
	clearInterval(secondInterval);
}


function oneSecondInterval()
{
	getCurrentData();
	if(loggedIn && !loggingIn)
		refreshEnumParams();
}


function prepareSite()
{
	var stateTemplate = '<p><span id="device_state_name_{0}">{1}</span> device state: <span id="device_state_{0}">N/A</span></p>';
	var devicesStates = document.getElementById("devices_states");
	for(var i in devices)
	{
		devicesKeys.push(i);
		child = stateTemplate.format(i,devices[i]);
		devicesStates.innerHTML += child;
	}
	
	if(loggedIn)
	{
		//create devices header and tabs
		var tabTemplate = '<div id="devices_tab_{0}" style="display: {2};"><div id ="tab_sensors_{0}" class="sensors"></div><div id ="tab_parameters_{0}" class="parameters"></div><div id ="tab_charts_{0}" class="charts"></div></div>';
		var navTemplate = '<button id="nav_button_{0}" onClick="changeNav(this.id)">{1}</button>';
		var devicesTabs = document.getElementById("devices_tabs");
		var devicesNav = document.getElementById("devices_nav");
		for(var i in devices) //bug?? can't be in one loop :|
		{
			child = navTemplate.format(i,devices[i]);
			devicesNav.innerHTML += child;
		}
		for(var i in devices)
		{
			child = tabTemplate.format(i,devices[i],"block");
			devicesTabs.innerHTML += child;
		}
		//create charts
		createDevicesTab();
		//get charts data
		getChartData();
	}
}


function getCurrentData()
{
	disableIntervals();
	var request = new XMLHttpRequest();
	request.onreadystatechange=function()
	{
		if (request.readyState==4 && request.status==200)
		{
			document.getElementById("server_state").style.color = "green";
			document.getElementById("server_state").innerHTML = "Up";
			
			var currentData = eval(request.responseText);
			updateDevicesStates(currentData);
			if(loggedIn)
			{
				updateSensors(currentData);
			}
			if(!loggingIn)
			{
				enableIntervals();
			}
		}
		else if(request.readyState==4)
		{
			document.getElementById("server_state").style.color = "red";
			document.getElementById("server_state").innerHTML = "Down";
			setTimeout(enableIntervals, 1000);
		}
	}
	request.open("GET","currentdata",true);
	request.send();
}


function updateDevicesStates(currentData)
{
	devicesTimesNames = {};
	length = 0;
	
	for(var i = 0 ; i < currentData.length ; i++)
	{
		var tmp = currentData[i];
		if (!devicesTimesNames[tmp.device]) {
			devicesTimesNames[tmp.device] = ({'date':tmp.date,'device_name':tmp.device_name});
			length++;
		}
	}
	
	//if new device added, reload
	if( devicesKeys.length < length)
	{
		location.reload(true);
	}
	
	var currentDataTime = parseInt(new Date().getTime() / 1000);
	for(var i = 0 ; i < devicesKeys.length ; i++)
	{
		var deviceId = devicesKeys[i];
		var deviceStateDiv = document.getElementById("device_state_"+deviceId);
		var deviceStateNameDiv = document.getElementById("device_state_name_"+deviceId);
		
		if(typeof devicesTimesNames[deviceId] == 'undefined')
		{
			deviceStateDiv.innerHTML = "No data for device";
			deviceStateDiv.style.color = "red";
			//no data for device
			continue;
		}
		
		//changing name
		deviceStateNameDiv.innerHTML = devicesTimesNames[deviceId]['device_name'];
		
		//changing state
		var deviceLastSeenSec = currentDataTime - parseInt(new Date(devicesTimesNames[deviceId]['date']).getTime() / 1000);
		if(deviceLastSeenSec < 10)
		{
			deviceStateDiv.innerHTML = "Connected";
			deviceStateDiv.style.color = "green";
		}
		else
		{
			deviceStateDiv.innerHTML = "Disconnected";
			deviceStateDiv.style.color = "red";
		}
	}
}


function createDevicesTab()
{
	var sensorDivIdTemplate = 'sensor_data_type_{0}';
	var devTabSensors = document.querySelectorAll('div[id^="tab_sensors_"]');
	var devTabParameters = document.querySelectorAll('div[id^="tab_parameters_"]');
	var devTabCharts = document.querySelectorAll('div[id^="tab_charts_"]');
	
	//sensors gauges and views
	for( var i = 0 ; i < data_config.length ; i++)
	{
		var currentDevTab = null;
		var deviceId = data_config[i]['device'];
		//find current dev tab
		for(var j = 0 ; j < devTabSensors.length ; j++)
		{
			if(deviceId == devTabSensors[j].id.split("_").pop())
			{
				currentDevTab = devTabSensors[j];
				break;
			}
		}
		var dataTypes = data_config[i]['data_type'];
		// we must sort them so store in array first
		var sensorsArray = [];
		for(var j = 0 ; j < dataTypes.length ; j++)
		{
			var dataTypeId = dataTypes[j]['id'];
			var sensorDiv = document.createElement("div");
			sensorDiv.id = sensorDivIdTemplate.format(dataTypeId);
			if( dataTypes[j]['type'] != 0xFF)
			{
				sensorDiv.class = "sensor_gauge";
			}
			else
			{
				sensorDiv.class = "sensor_enum";
				sensorDiv.innerHTML = "none";
			}
			sensorsArray.push({'data_type' : dataTypes[j], 'div' : sensorDiv});
		}
		//sorting
		sensorsArray.sort(function(a,b){
			return a['div'].class > b['div'].class ? -1 : a['div'].class==b['div'].class ? 0 : 1;
		});
		//inserting
		for(var j = 0 ; j < sensorsArray.length ; j++)
		{
			currentDevTab.appendChild(sensorsArray[j]['div']);
			if(sensorsArray[j]['div'].innerHTML != "none")
			{
				gaugeSettings.chart.renderTo = sensorsArray[j]['div'];
				gaugeSettings.title.text = sensorsArray[j]['data_type'].name;
				
				var val1 = sensorsArray[j]['data_type']['values'][0].value;
				var val2 = sensorsArray[j]['data_type']['values'][1].value;
				var min = Math.min(val1,val2);
				var max = Math.max(val1,val2);
				
				gaugeSettings.yAxis.min = parseInt(min);
				gaugeSettings.yAxis.max = parseInt(max);
				gaugeSettings.series[0].data[0] = parseInt(min);
				
				if(sensorsArray[j]['data_type'].unit != "null")
				{
					gaugeSettings.yAxis.title.text = sensorsArray[j]['data_type'].unit;
				}
				//normal gauge
				var gauge = new Highcharts.Chart(gaugeSettings);
			}
			else
			{
				gaugeEnumSettings.chart.renderTo = sensorsArray[j]['div'];
				gaugeEnumSettings.title.text = sensorsArray[j]['data_type'].name;
				gaugeEnumSettings.yAxis.max = sensorsArray[j]['data_type']['values'].length;
				for( var k = 0 ; k < sensorsArray[k]['data_type']['values'].length ; k++)
				{
					gaugeEnumSettings.yAxis.categories.push(sensorsArray[j]['data_type']['values'][k].name);
					gaugeEnumSettings.yAxis.tickPositions.push(k+0.5);
					
					var newEnumPlotBand = Object.create(gaugeEnumPlotBandSettings);
					newEnumPlotBand.from = k;
					newEnumPlotBand.to = k+1;
					gaugeEnumSettings.yAxis.plotBands.push(newEnumPlotBand);
				}
				// gauge for enums
				var gauge = new Highcharts.Chart(gaugeEnumSettings);
			}
			gauges.push({'data_type_id' : sensorsArray[j]['data_type'].id,'data_type' : sensorsArray[j]['data_type'].type, 'gauge' : gauge});
		}
		// big charts view
		var newCharts = [];
		for(var j = 0 ; j < dataTypes.length ; j++)
		{
			if(dataTypes[j].big_chart_id != 0)
			{
				var min;
				var max;
				//find min and max
				for(var k = 0 ; k < dataTypes[j].values.length; k++)
				{
					if(dataTypes[j].values[k].name == 'min')
					{
						min = dataTypes[j].values[k].value;
					}
					if(dataTypes[j].values[k].name == 'max')
					{
						max = dataTypes[j].values[k].value;
					}
				}
				
				//prepare series
				var newSeries = JSON.parse(JSON.stringify(chartSeriesTemplate));
				newSeries.name = dataTypes[j].name;
				newSeries.id = dataTypes[j].id;
				newSeries.color = Highcharts.getOptions().colors[j]; // make colors!:)
				
				// check if chart exists
				var idx = -1;
				for(var k = 0 ; k < newCharts.length ; k++)
				{
					if(newCharts[k].id == dataTypes[j].big_chart_id)
					{
						idx = k;
						break;
					}
				}
				if(idx != -1)
				{
					newCharts[idx].chart.title.text += dataTypes[j].name + " ";
					newCharts[idx].chart.series.push(newSeries);
					if(newCharts[idx].chart.yAxis.min > min)
					{
						newCharts[idx].chart.yAxis.min = min;
					}
					if(newCharts[idx].chart.yAxis.max < max)
					{
						newCharts[idx].chart.yAxis.max = max;
					}
				}
				else
				{
					var newChart = {};
					newChart['id'] = dataTypes[j].big_chart_id;
					newChart['chart'] = JSON.parse(JSON.stringify(chartTemplate));
					newChart['chart'].series.push(newSeries);
					newChart['chart'].title.text += dataTypes[j].name + " ";
					newChart['chart'].yAxis.min = min;
					newChart['chart'].yAxis.max = max;
					newChart['chart'].xAxis.events.afterSetExtremes = getChartData;
					newChart['chart'].exporting.buttons.customButton.onclick = getChartData;
					newCharts.push(newChart);
				}
			}
		}
		for(var j = 0 ; j < newCharts.length ; j++)
		{
			var currentChartsDiv = null;
			//find current chart tab
			for(var k = 0 ; k < devTabCharts.length ; k++)
			{
				if(deviceId == devTabCharts[k].id.split("_").pop())
				{
					currentChartsDiv = devTabCharts[k];
					break;
				}
			}
			var newChartDiv = document.createElement("div");
			newChartDiv.setAttribute("id","chart_" + deviceId + "_" + newCharts[j].id);
			currentChartsDiv.appendChild(newChartDiv);
			newCharts[j]['chart']['chart']['renderTo'] = newChartDiv.id;
			var chart = new Highcharts.StockChart(newCharts[j].chart);
			charts.push({'device_id' : deviceId ,'big_chart_id' : newCharts[j].id, 'chart' : chart});
		}
	}
	
	//parameters
	for( var i = 0 ; i < parameters_config.length ; i++)
	{
		var currentDevTab = null;
		var deviceId = parameters_config[i]['device'];
		//find current dev tab
		for(var j = 0 ; j < devTabParameters.length ; j++)
		{
			if(deviceId == devTabParameters[j].id.split("_").pop())
			{
				currentDevTab = devTabParameters[j];
				break;
			}
		}
		
		currentDeviceParam = parameters_config[i]['config_params'];
		parametersFieldset = null; // form for numeric parameters
		for(var j = 0 ; j < currentDeviceParam.length ; j++)
		{
			config = currentDeviceParam[j];
			if(config['type'] != 0xFF)
			{
				if(parametersFieldset == null)
				{
					parametersFieldset = document.createElement("fieldset");
					parametersFieldset.id = "parameters_form_" + deviceId;
					
					var legend = document.createElement("legend");
					legend.innerHTML = "<h3>Parameters</h3>";
					
					parametersFieldset.appendChild(legend);
				}
				
				var paramValues = config['config_values'];
				var label = document.createElement("label");
				var input = document.createElement("input");
				input.type = "number";
				input.id = "input_" + config['id'];
				for(var k = 0 ; k < paramValues.length; k++)
				{
					input.setAttribute(paramValues[k].name.toLowerCase(),""+paramValues[k].value);
				}
				input.setAttribute("step","1");
				input.setAttribute("value",config['value']);
				
				label.appendChild(input);
				label.innerHTML += "<span>"+config.name+"</span>";
				parametersFieldset.appendChild(label);
			}
			else //enums
			{
				var parameterDiv = document.createElement("div");
				var fieldset = document.createElement("fieldset");
				var legend = document.createElement("legend");
				legend.innerHTML = "<h3>"+config.name+"</h3>";
				fieldset.appendChild(legend);
				
				var paramValues = config['config_values'];
				for(var k = 0 ; k < paramValues.length; k++)
				{
					var div = document.createElement("div");
					//div.id = "param_label_" + config['id'] + "_" + paramValues[k]['value'];
					
					var radioButton = document.createElement("input");
					radioButton.type = "radio";
					radioButton.value = paramValues[k]['value'];
					radioButton.setAttribute("name", "radio_" + config['id']);
					radioButton.setAttribute("class", "radio_" + config['id']);
					radioButton.setAttribute("onclick","changeEnumParameter(this)");
					
					if(paramValues[k]['value'] == config['value'])
					{
						radioButton.setAttribute("checked","true");
					}
					
					div.appendChild(radioButton);
					div.innerHTML += "<span>"+paramValues[k].name+"</span>";
					
					fieldset.appendChild(div);
				}

				parameterDiv.appendChild(fieldset);
				currentDevTab.appendChild(parameterDiv);
			}
		}
		if(parametersFieldset != null)
		{
			var label = document.createElement("label");
			var submitButton = document.createElement("input");
			var refreshButton = document.createElement("input");
			submitButton.type = "button";
			refreshButton.type = "button";
			submitButton.value = "Submit";
			submitButton.id = "submit_" + deviceId;
			refreshButton.value = "Refresh";
			submitButton.setAttribute("onclick","sendNewParams(this)");
			refreshButton.setAttribute("onclick","refreshParams()");
			label.appendChild(submitButton);
			label.appendChild(refreshButton);
			parametersFieldset.appendChild(label);
			currentDevTab.appendChild(parametersFieldset);
		}
	}
	
	//only one device tab visible
	changeNav(devicesKeys[0]);
}


function updateSensors(currentData)
{
	for (var i  = 0 ; i < currentData.length ; i++)
	{
		for (var j  = 0 ; j < currentData[i]['data'].length ; j++)
		{
			var data_type_id = currentData[i]['data'][j]['data_type'];
			var newValue = currentData[i]['data'][j]['value'];
			for( var k = 0 ; k < gauges.length ; k++)
			{
				if(gauges[k]['data_type_id'] == data_type_id)
				{
					if(gauges[k]['data_type'] != 0xFF)
					{
						gauges[k]['gauge'].series[0].points[0].update(parseInt(newValue));
					}
					else
					{
						gauges[k]['gauge'].series[0].points[0].update(parseInt(newValue) + 0.5);
					}
					break;
				}
			}
		}
	}
}


function changeNav(buttonId)
{
	devId = buttonId.split("_").pop();
	devTabs = document.querySelectorAll('div[id^="devices_tab_"]');
	
	for(var i = 0 ; i < devTabs.length ; i++)
	{
		if(devId != devTabs[i].id.split("_").pop())
		{
			devTabs[i].style.display = 'none';
		}
		else
		{
			devTabs[i].style.display = 'block';
		}
		
	}
}


function refreshEnumParams()
{
	var request = new XMLHttpRequest();
	request.onreadystatechange=function(){
		if (request.readyState==4 && request.status==200)
		{
			var params_array = JSON.parse(request.responseText);
			
			for(var i = 0 ; i < params_array.length ; i++)
			{
				var radios = document.getElementsByClassName("radio_"+params_array[i].id);
				var value = params_array[i].value;
				
				for(var j = 0 ; j < radios.length; j++)
				{
					if(radios[j].value == value)
					{
						radios[j].checked = true;
						break;
					}
				}
			}
		}
	}
	request.open("GET","configenumdata",true);
	request.send();
}


function refreshParams()
{
	var request = new XMLHttpRequest();
	request.onreadystatechange=function(){
		if (request.readyState==4 && request.status==200)
		{
			var params_array = JSON.parse(request.responseText);
			
			for(var i = 0 ; i < params_array.length ; i++)
			{
				var id = params_array[i].id;
				var value = params_array[i].value;
				if(params_array[i].type != 0xFF)
				{
					var input = document.getElementById("input_"+id);
					input.value = value
				}
				else
				{
					var radios = document.getElementsByClassName("radio_"+id);
					var value = params_array[i].value;
					
					for(var j = 0 ; j < radios.length; j++)
					{
						if(radios[j].value == value)
						{
							radios[j].checked = true;
							break;
						}
					}
				}
			}
		}
	}
	request.open("GET","configdata",true);
	request.send();
}


function changeEnumParameter(radio)
{
	var data = null;
	
	var idConfig = radio.name.split("_").pop();
	var value = radio.value;
	
	data = {"id":idConfig,"value":value};
	
	var request = new XMLHttpRequest();
	request.open("POST", "saveconfigparams");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	request.send("csrfmiddlewaretoken=" + getCookie("csrftoken") + "&data=" + JSON.stringify(data));
}


function sendNewParams(button)
{
	var data = {};
	
	var deviceId = button.id.split("_").pop();
	data["device"] = deviceId;
	data["values"] = [];
	
	var deviceConfig = null;
	for(var i = 0 ; i < parameters_config.length; i++)
	{
		if(parameters_config[i].device == deviceId)
		{
			deviceConfig = parameters_config[i].config_params;
			break;
		}
	}
	
	for(var i = 0 ; i < deviceConfig.length ; i++)
	{
		var idConfig = deviceConfig[i].id;
		var value = null;
		if(deviceConfig[i].type != 0xFF)
		{
			value = document.getElementById("input_"+idConfig).value;
		}
		else
		{
			var radios = document.getElementsByClassName("radio_"+idConfig);
			for(var j = 0 ; j < radios.length; j++)
			{
				if(radios[j].checked == true)
				{
					value = radios[j].value;
					break;
				}
			}
		}
		data["values"].push({"id":idConfig,"value":value});
	}
	
	var request = new XMLHttpRequest();
	request.open("POST", "saveconfigparams");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	request.send("csrfmiddlewaretoken=" + getCookie("csrftoken") + "&data=" + JSON.stringify(data));
}


function getChartData(time)
{
	var deviceId = "None";
	var chartId = "None";
	var timestart = "None"; // start far in the past (start of data)
	var timeend = "None";
	var type = "None";
	
	if(this == window)
	{
		//page load
		for(var i = 0 ; i < charts.length ; i++)
		{
			charts[i].chart.showLoading('Loading data from server...');
		}
		type = "window";
	}
	else
	{
		//button
		if(typeof time == 'undefined')
		{
			var splitId = this.container.parentElement.id.split("_");
			chartId = splitId.pop();
			deviceId = splitId.pop();
			this.showLoading('Loading data from server...');
			type = "button";
		}
		//aftersetextremes
		else
		{
			var splitId;
			if(typeof this.container !== 'undefined')
			{
				splitId = this.container.parentElement.id.split("_");
				this.showLoading('Loading data from server...');
			}
			else
			{
				splitId = this.chart.container.parentElement.id.split("_");
				this.chart.showLoading('Loading data from server...');
			}
			chartId = splitId.pop();
			deviceId = splitId.pop();
			
			if(typeof time.min !== 'undefined' && typeof time.max !== 'undefined')
			{
				timestart = Math.round(time.min / 1000);
				timeend = Math.round(time.max / 1000);
			}
			type = "setextremes";
		}
	}
	
	var request = new XMLHttpRequest();
	request.onreadystatechange=function(){
		if (request.readyState==4 && request.status==200){
			var response_data = eval(request.responseText);
			
			if(typeof response_data == 'undefined' || response_data[3] === "None")
			{
				return;
			}
			
			//prepare data
			var charts_data = eval(response_data[3]);
			var response_device = response_data[0];
			var response_chart_id = response_data[1];
			var response_type = response_data[2];
			
			var newDataParsed = [];
			for(var i = 0 ; i < charts_data.length ; i++)
			{
				var exists = false;
				var j;
				for(j = 0 ; j < newDataParsed.length ; j++)
				{
					if(newDataParsed[j].id === charts_data[i][0]){
						exists = true;
						break;
					}
				}
				if(exists)
				{
					var tmp = [Date.parse(charts_data[i][1].substr(0,10)+'T'+charts_data[i][1].substr(11,8)),charts_data[i][2]];
					newDataParsed[j]['values'].push(tmp);
				}
				else
				{
					var tmp = { "id" : charts_data[i][0], "values" : [ [Date.parse(charts_data[i][1].substr(0,10)+'T'+charts_data[i][1].substr(11,8)),charts_data[i][2]] ] };
					newDataParsed.push(tmp);
				}
			}
			// set new data
			for(var i = 0 ; i < charts.length; i++)
			{
				var series = null;
				if(response_device === "None" || 
					(charts[i].chart.options.chart.renderTo.split("_")[1] === response_device && 
					charts[i].chart.options.chart.renderTo.split("_")[2] === response_chart_id) )
				{
					for(var j = 0 ; j < newDataParsed.length ; j++)
					{
						series = charts[i].chart.get(parseInt(newDataParsed[j].id));
						if(series != null)
						{
							var newSeriesData = [];
							for(var k = 0 ; k < newDataParsed[j].values.length ; k++){
								newSeriesData.push([newDataParsed[j].values[k][0],
													newDataParsed[j].values[k][1] == "null" ? null : parseInt(newDataParsed[j].values[k][1])]);
							}
							newSeriesData.sort(function(a, b) {return a[0] - b[0];});
							if(response_device === "None") // it means that the page was loaded
							{
								series.setData(newSeriesData);
								charts[i].chart.options.navigator.adaptToUpdatedData = true;
								charts[i].chart.get('nav').setData(newSeriesData);
								charts[i].chart.options.navigator.adaptToUpdatedData = false;
							}
							else
							{
								series.setData(newSeriesData);
							}
						}
					}
					if(response_type !== "setextremes"){
						charts[i].chart.xAxis[0].setExtremes();
						if(response_device === "None") // first load - set one week
							charts[i].chart.rangeSelector.clickButton(1,1,true);
					}
					if(response_device !== "None") // loaded week after loading site
						charts[i].chart.hideLoading();
				}
			}
		}
		//if (request.readyState==4 && (request.status==500 || request.status==503 || request.status==403)){}
	}
	request.open("GET","./data?timestart="+timestart+"&timeend="+timeend+"&device="+deviceId+"&chartid="+chartId +"&type="+type ,true);
	request.send();
}


/** get csrf token from cookie **/
function getCookie(name)
{
	var cookieValue = null;
	if (document.cookie && document.cookie != '')
	{
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++)
		{
			if(document.cookie.split(';')[0].trim().substring(0,name.length + 1) == name + "=")
			{
				cookieValue = decodeURIComponent(document.cookie.split(';')[0].trim().substring(name.length + 1))
				break;
			}
		}
	}
	return cookieValue;
}


/** formatting string **/
String.prototype.format = function()
{
	var formatted = this;
	for (var i = 0; i < arguments.length; i++)
	{
		var regexp = new RegExp('\\{'+i+'\\}', 'gi');
		formatted = formatted.replace(regexp, arguments[i]);
	}
	return formatted;
};