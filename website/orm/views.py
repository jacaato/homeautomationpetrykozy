from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden, HttpResponseBadRequest
from django.template import RequestContext, loader
from django.contrib.auth import authenticate, login, logout
from django.db.models import Prefetch
from django.db import connection
import json
import time
from orm.models import *


def index(request):
	index_template = loader.get_template("index.html")
	request_context = RequestContext(request)
	path = request.path_info
	request_context['path'] = path
	_get_devices(request_context)
	if request.user.is_authenticated():
		request_context['data_config'] = _get_data_types()
		request_context['parameters_config'] = _get_config_parameters()
	else:
		request_context['data_config'] = "null"
		request_context['parameters_config'] = "null"
	return HttpResponse(index_template.render(request_context))


def log_in(request):
	if request.user.is_authenticated():
		logout(request)
		return HttpResponseRedirect("/django/")
	else:
		user = authenticate(username = request.POST['username'], password = request.POST['password'])
		if user is not None:
			login(request, user)
			return HttpResponseRedirect("/django/")
		else:
			return HttpResponseRedirect("/django/")


def get_current_data(request):
	#data_type__device ← magic! __ prefetch devices and data_types
	authenticated = request.user.is_authenticated()
	current_data = DataCurrent.objects.using('home_data').prefetch_related('data_type__device').prefetch_related().all()
	data = []
	for x in current_data:
		try:
			device = next(item for item in data if item['device']== x.data_type.device.id)
			if authenticated:
				device['data'].append(dict())
				new_data = device['data'][-1]
				new_data['data_type'] = x.data_type.id
				new_data['value'] = x.value
		except (StopIteration, KeyError):
			new_device = dict()
			new_device['device'] = x.data_type.device.id
			new_device['device_name'] = x.data_type.device.name.decode('utf-8')
			new_device['date'] = str(x.date).split('+')[0]
			#moar data for authenticated users
			if authenticated:
				new_device['data'] = [dict()]
				new_data = new_device['data'][-1]
				new_data['data_type'] = x.data_type.id
				new_data['value'] = x.value
			data.append(new_device)
	return HttpResponse(json.dumps(data))


def get_data_types(request):
	authenticated = request.user.is_authenticated()
	if authenticated:
		return HttpResponse(_get_data_types())
	else:
		return HttpResponseForbidden()


def get_data(request):
	'''data for charts'''
	authenticated = request.user.is_authenticated()
	if authenticated and request.META['REQUEST_METHOD'] == "GET":
		get_time_start = request.GET['timestart']
		get_time_end = request.GET['timeend']
		get_device = request.GET['device']
		get_chart_id = request.GET['chartid']
		get_type = request.GET['type']
		
		if get_time_start == 'NaN' or get_time_start == 'NaN':
			return HttpResponse(json.dumps([get_device,get_chart_id,get_type,'[]']))
		
		if get_time_start != "None" and get_time_end != "None":
			time_range = int(get_time_end) - int(get_time_start) # in seconds
		else:
			get_time_end = int(time.time())
			get_time_start = 0
			time_range = int(get_time_end) - int(get_time_start) # in seconds
		
		if time_range <= 1 * 24 * 60 * 60:
			table_name = 'data_minutely'
			print(int(get_time_end) - int(time.time()))
			if int(get_time_end) - int(time.time()) < 2 * 60:
				get_time_end = int(time.time())
		elif time_range <= 365 * 30 * 24 * 60 * 60:
			table_name = 'data_hourly'
		else:
			table_name = 'data_daily'
			
		print("ts "  + str(get_time_start))
		print("te " + str(get_time_end))
		
		row = [""]
		cursor = connection.cursor()
		#load all
		if get_device == "None":
			query ='''SELECT CONCAT("[", GROUP_CONCAT(CONCAT("['", `data_type`, "'"),CONCAT(",'", `date`, "'"),CONCAT(",'", IFNULL(`value`,'null'), "']")), "]") 
						AS json FROM (
						SELECT `data_type`,`date`,`value` FROM `home_automation_petrykozy`.`{table}` AS a
						JOIN `home_automation_petrykozy`.`data_types` 
						ON `home_automation_petrykozy`.`data_types`.id = `data_type` 
						WHERE `big_chart_id` != '0'
						UNION ALL 
						SELECT `data_type`,`date`,`value` FROM `home_automation_petrykozy`.`data_current` AS b 
						JOIN `home_automation_petrykozy`.`data_types` 
						ON `home_automation_petrykozy`.`data_types`.id = `data_type` 
						WHERE `big_chart_id` != '0')
						AS conc'''.format(table = table_name)
		#load only one chart
		elif get_device != "None" and get_chart_id != "None":
			query = '''SELECT CONCAT("[", GROUP_CONCAT(CONCAT("['", `data_type`, "'"),CONCAT(",'", `date`, "'"),CONCAT(",'", IFNULL(`value`,'null'), "']")), "]") 
						AS json FROM (
						SELECT `data_type`,`date`,`value` FROM `home_automation_petrykozy`.`{table}` AS a
						JOIN `home_automation_petrykozy`.`data_types` 
						ON `home_automation_petrykozy`.`data_types`.id = `data_type` 
						WHERE `big_chart_id` = {chart_id} AND `device` = {device} AND `date` BETWEEN from_unixtime({start}) AND from_unixtime({end})
						UNION ALL 
						SELECT `data_type`,`date`,`value` FROM `home_automation_petrykozy`.`data_current` AS b 
						JOIN `home_automation_petrykozy`.`data_types` 
						ON `home_automation_petrykozy`.`data_types`.id = `data_type` 
						WHERE `big_chart_id` = {chart_id} AND `device` = {device} AND `date` BETWEEN from_unixtime({start}) AND from_unixtime({end})
						)
						AS conc'''.format(table = table_name, chart_id = get_chart_id, device = get_device, start = get_time_start, end= get_time_end)
		else:
			return HttpResponseBadRequest()
		
		print (query)
		
		cursor.execute(query)
		row = cursor.fetchone()
		
		
		
		return HttpResponse(json.dumps([get_device,get_chart_id,get_type,str(row[0])]))
	else:
		return HttpResponseForbidden()


def _get_config_parameters():
	data = []
	config_params_values = ConfigurationParameterValues.objects.using('home_data').prefetch_related('configuration_parameter__configuration__device').all()
	for x in config_params_values:
		try:
			config_device = next(item for item in data if item['device'] == x.configuration_parameter.configuration.device.id)
			try:
				config_param = next(item for item in config_device['config_params'] if item['id'] == x.configuration_parameter.id)
				n = dict()
				n['name'] = x.name
				n['value'] = x.value
				config_param['config_values'].append(n)
			except (StopIteration, KeyError):
				n = dict()
				n['id'] = x.configuration_parameter.id
				n['name'] = x.configuration_parameter.name
				n['type'] = x.configuration_parameter.type
				n['value'] = x.configuration_parameter.value
				n['config_values'] = []
				config_value = dict()
				config_value['name'] = x.name
				config_value['value'] = x.value
				n['config_values'].append(config_value)
				config_device['config_params'].append(n)
		except (StopIteration, KeyError):
			n = dict()
			n['device'] = x.configuration_parameter.configuration.device.id;
			n['config_params'] = []
			config_param = dict()
			config_param['id'] = x.configuration_parameter.id
			config_param['name'] = x.configuration_parameter.name
			config_param['type'] = x.configuration_parameter.type
			config_param['value'] = x.configuration_parameter.value
			config_param['config_values'] = []
			config_value = dict()
			config_value['name'] = x.name
			config_value['value'] = x.value
			config_param['config_values'].append(config_value)
			n['config_params'].append(config_param)
			data.append(n)
	return json.dumps(data)


def get_config_data(request):
	authenticated = request.user.is_authenticated()
	if authenticated:
		data = []
		config_params = ConfigurationParameters.objects.using('home_data').all()
		for param in config_params:
			n = dict()
			n['id'] = param.id
			n['value'] = param.value
			n['type'] = param.type
			data.append(n)
		return HttpResponse(json.dumps(data))
	else:
		return HttpResponseForbidden()


def get_config_enum_data(request):
	authenticated = request.user.is_authenticated()
	if authenticated:
		data = []
		config_params = ConfigurationParameters.objects.using('home_data').all()
		for param in config_params:
			if(param.type == 0xFF):
				n = dict()
				n['id'] = param.id
				n['value'] = param.value
				data.append(n)
		return HttpResponse(json.dumps(data))
	else:
		return HttpResponseForbidden()


def _get_data_types():
	data = []
	data_types = DataTypeValues.objects.using('home_data').prefetch_related('data_type').all()
	for x in data_types:
		device = dict()
		try:
			#find device
			dtype = next(item for item in data if item['device']== x.data_type.device.id)
			try:
				#find data_type
				dtypevals = next(item for item in dtype['data_type'] if item['id']== x.data_type.id)
				dtypevals['values'].append(dict())
				new_dtypevals = dtypevals['values'][-1]
				new_dtypevals['name'] = x.name.decode('utf-8')
				new_dtypevals['value'] = x.value
				new_dtypevals['index'] = x.index
			except (StopIteration, KeyError):
				pass
				dtype['data_type'].append(dict())
				new_dtype = dtype['data_type'][-1]
				new_dtype['id'] = x.data_type.id
				new_dtype['name'] = x.data_type.name.decode('utf-8')
				new_dtype['type'] = x.data_type.type
				new_dtype['big_chart_id'] = x.data_type.big_chart_id
				new_dtype['big_chart_type'] = x.data_type.big_chart_type
				new_dtype['unit'] = x.data_type.unit.decode('utf-8')
				new_dtype['values'] = []
				new_dtype['values'].append(dict())
				dtypevals = new_dtype['values'][-1]
				dtypevals['name'] = x.name.decode('utf-8')
				dtypevals['value'] = x.value
				dtypevals['index'] = x.index
		except (StopIteration, KeyError):
			device['device'] = x.data_type.device.id;
			device['data_type'] = []
			device['data_type'].append(dict())
			dtype = device['data_type'][-1]
			dtype['id'] = x.data_type.id
			dtype['name'] = x.data_type.name.decode('utf-8')
			dtype['type'] = x.data_type.type
			dtype['big_chart_id'] = x.data_type.big_chart_id
			dtype['big_chart_type'] = x.data_type.big_chart_type
			dtype['unit'] = x.data_type.unit.decode('utf-8')
			dtype['values'] = []
			dtype['values'].append(dict())
			dtypevals = dtype['values'][-1]
			dtypevals['name'] = x.name.decode('utf-8')
			dtypevals['value'] = x.value
			dtypevals['index'] = x.index
			data.append(device);
	return json.dumps(data)


def _get_devices(request_context):
	from orm.models import Devices
	dictionary_devices = dict()
	cursor = Devices.objects.using('home_data').all()
	for x in cursor:
		dictionary_devices[x.id] = x.name.decode('utf-8');
	request_context['devices'] = dictionary_devices


def save_config_params(request):
	authenticated = request.user.is_authenticated()
	if authenticated and request.META['REQUEST_METHOD'] == "POST":
		data = json.loads(request.POST['data'])
		if "device" in data.keys():
			device = data['device']
			config = ConfigurationParameters.objects.using('home_data').prefetch_related('configuration__device').filter(configuration__device=device)
			values = data['values']
			for item in config:
				for new_config in data['values']:
					if item.id == new_config['id']:
						item.value = new_config['value']
						break
				item.configuration.applied = 0;
				item.save(force_update=True, using='home_data')
				item.configuration.save(force_update=True, using='home_data')
		else:
			config = ConfigurationParameters.objects.using('home_data').prefetch_related('configuration').get(id=data['id'])
			config.configuration.applied = 0
			config.value = data['value']
			config.save(force_update=True, using='home_data')
			config.configuration.save(force_update=True, using='home_data')
		return HttpResponse("ok")
	else:
		return HttpResponseForbidden()