\select@language {polish}
\contentsline {chapter}{\numberline {1}Wprowadzenie}{7}
\contentsline {section}{\numberline {1.1}Opis problemu}{7}
\contentsline {section}{\numberline {1.2}Cel pracy}{7}
\contentsline {section}{\numberline {1.3}Zakres pracy}{7}
\contentsline {section}{\numberline {1.4}Motywacje}{7}
\contentsline {section}{\numberline {1.5}Struktura pracy}{8}
\contentsline {chapter}{\numberline {2}Historia automatyki domowej}{9}
\contentsline {section}{\numberline {2.1}Definicja}{9}
\contentsline {section}{\numberline {2.2}Historia}{9}
\contentsline {section}{\numberline {2.3}Obecne rozwi\IeC {\k a}zania}{10}
\contentsline {subsection}{\numberline {2.3.1}Protoko\IeC {\l }y komunikacji}{11}
\contentsline {subsection}{\numberline {2.3.2}Przyk\IeC {\l }adowe rozwi\IeC {\k a}zania}{12}
\contentsline {chapter}{\numberline {3}G\IeC {\l }\IeC {\'o}wny kontroler}{13}
\contentsline {section}{\numberline {3.1}Opis sprz\IeC {\k e}tu}{13}
\contentsline {subsection}{\numberline {3.1.1}Raspberry Pi model B+}{13}
\contentsline {subsection}{\numberline {3.1.2}ATmega8A}{14}
\contentsline {section}{\numberline {3.2}Baza danych}{14}
\contentsline {section}{\numberline {3.3}Webserver}{17}
\contentsline {section}{\numberline {3.4}G\IeC {\l }\IeC {\'o}wny modu\IeC {\l } radiowy}{18}
\contentsline {subsection}{\numberline {3.4.1}Schemat uk\IeC {\l }adu}{19}
\contentsline {section}{\numberline {3.5}Kontroler modu\IeC {\l }u radiowego}{20}
\contentsline {chapter}{\numberline {4}Komunikacja radiowa}{23}
\contentsline {section}{\numberline {4.1}Modu\IeC {\l } radiowy}{23}
\contentsline {subsection}{\numberline {4.1.1}Konfiguracja modu\IeC {\l }u radiowego}{23}
\contentsline {section}{\numberline {4.2}Rodzaje pakiet\IeC {\'o}w}{26}
\contentsline {section}{\numberline {4.3}Opis komunikacji}{27}
\contentsline {subsection}{\numberline {4.3.1}Schemat pakietu danych uk\IeC {\l }adu RFM69HW}{28}
\contentsline {subsection}{\numberline {4.3.2}Struktura danych w pakietach}{29}
\contentsline {subsubsection}{\numberline {4.3.2.1}Pakiet rejestracyjny}{29}
\contentsline {subsubsection}{\numberline {4.3.2.2}Pakiet konfiguracyjny}{31}
\contentsline {subsubsection}{\numberline {4.3.2.3}Pakiet danych}{31}
\contentsline {section}{\numberline {4.4}Wykrywanie kolizji pakiet\IeC {\'o}w}{32}
\contentsline {chapter}{\numberline {5}Modu\IeC {\l }y zewn\IeC {\k e}trzne}{33}
\contentsline {section}{\numberline {5.1}Algorytm dzia\IeC {\l }ania}{33}
\contentsline {section}{\numberline {5.2}Modu\IeC {\l } \IeC {\l }azienki}{33}
\contentsline {subsection}{\numberline {5.2.1}Zakres dzia\IeC {\l }ania}{33}
\contentsline {subsection}{\numberline {5.2.2}Opis czujnik\IeC {\'o}w}{35}
\contentsline {subsection}{\numberline {5.2.3}Schemat uk\IeC {\l }adu}{36}
\contentsline {section}{\numberline {5.3}Modu\IeC {\l } piwnicy}{37}
\contentsline {subsection}{\numberline {5.3.1}Zakres dzia\IeC {\l }ania}{37}
\contentsline {subsection}{\numberline {5.3.2}Opis czujnik\IeC {\'o}w}{37}
\contentsline {subsection}{\numberline {5.3.3}Schemat uk\IeC {\l }adu}{39}
\contentsline {chapter}{\numberline {6}Interfejs u\IeC {\.z}ytkownika}{41}
\contentsline {section}{\numberline {6.1}Mo\IeC {\.z}liwe rozwi\IeC {\k a}zania}{41}
\contentsline {section}{\numberline {6.2}Interfejs webowy}{42}
\contentsline {section}{\numberline {6.3}Interfejs modu\IeC {\l }\IeC {\'o}w}{43}
\contentsline {chapter}{\numberline {7}Dalszy rozw\IeC {\'o}j}{45}
\contentsline {section}{\numberline {7.1}Skrypty}{45}
\contentsline {section}{\numberline {7.2}Wirtualne modu\IeC {\l }y zewn\IeC {\k e}trzne}{45}
\contentsline {section}{\numberline {7.3}Kontroler skrypt\IeC {\'o}w i wirtualnych modu\IeC {\l }\IeC {\'o}w}{46}
\contentsline {section}{\numberline {7.4}Rozw\IeC {\'o}j protoko\IeC {\l }u komunikacji}{46}
\contentsline {section}{\numberline {7.5}Serwer www}{47}
\contentsline {chapter}{\numberline {8}Podsumowanie}{49}
