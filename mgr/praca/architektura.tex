\chapter{Architektura systemu}
\label{sec:architektura_systemu}

W rozdziale opisane zostały aktualne rozwiązania dominujące w systemach inteligentnych domów, ich wady, 
zalety oraz architektura systemu, którego opisem jest ta praca.

\section{Możliwe rozwiązania}
\label{sec:mozliwe_rozwiazania}

Każdy układ do automatyki domowej musi składać się z elementów umożliwiających jego działanie. 
Najważniejszymi z nich są:
\begin{itemize}
\item sensory/czujniki -- są to urządzenia, których zadaniem jest zbieranie informacji z otoczenia i 
dostarczanie danych do kontrolera logiki. Przykładami czujników są: czujnik światła, temperatury, 
wilgotności, gazu, ruchu.
\item urządzenia wyjściowe -- elementy, których zadaniem jest uruchamianie bądź dostrajanie konkretnych zadań 
jak np. włączenie światła, zasłonięcie rolety, przekręcenie zaworu w kaloryferze.
\item kontrolery logiki -- urządzenia, którymi, w zależności od potrzeb, mogą być mikrokontrolery lub 
zwykłe komputery. Ich zadaniem jest zbieranie danych z sensorów, analizowanie ich i na tej podstawie 
zarządzanie urządzeniami wyjściowymi.
\item interfejs użytkownika -- najczęściej w formie programu komputerowego lub panelu naściennego, który 
umożliwia podgląd danych z czujników, stanów wyjść oraz posiadający możliwość konfiguracji parametrów 
kontrolerów logiki.
\end{itemize}

Można wyróżnić różne podejścia do tworzenia systemów inteligentnego domu, czyli do integracji wymienionych 
powyżej elementów. Głównymi z nich są:
\begin{itemize}
\item autonomiczne urządzenia,
\item rozproszone czujniki oraz urządzenia wyjściowe,
\item systemy hybrydowe.
\end{itemize}

\subsection{Autonomiczne urządzenia}
\label{subsec:autonomiczne_urzadzenia}

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{images/moduly_autonomiczne}
\caption{Przykład architektury z autonomicznymi urządzeniami}
\label{fig:autonomiczne_urzadzenia}
\end{figure}

System automatycznego domu złożony z urządzeń autonomicznych [rys. \ref{fig:autonomiczne_urzadzenia}] składa 
się z dowolnej ilości modułów kontrolowanych przez różne interfejsy użytkownika. Moduły te działają 
niezależnie od siebie i nie mają dostępu do danych pozostałych urządzeń. Systemy takie występują często jako 
wynik użytkowania modułów różnych firm, których protokoły nie są ze sobą zgodne, a także używania urządzeń, 
które z założenia nie mają się integrować z innymi (np. kontroler temperatury dołączony do pieca C.O.).

Zaletami takiego rozwiązania są zmniejszone koszty, ponieważ wszystkie czujniki, urządzenia wyjściowe oraz 
kontroler logiki mogą być złożone w jedno urządzenie. Dzięki temu minimalizujemy ilość potrzebnych przewodów 
lub energii potrzebnej do komunikacji radiowej, a także ilość samych urządzeń. Kolejną istotną zaletą jest 
to, że~awaria jednego z modułów nie powoduje awarii całego systemu, ponieważ nie są one w żaden sposób 
zintegrowane.

Wadami, które można wyróżnić są brak komunikacji między urządzeniami, przez co tracimy możliwość tworzenia 
dowolnych kombinacji czujników i urządzeń wyjściowych w celu jak najlepszego dopasowania do potrzeb. Kolejną 
z wad jest brak integracji interfejsów. Użytkownik chcąc kontrolować konkretną część systemu jest zmuszony do 
przełączania się pomiędzy aplikacjami lub przechodzenia między panelami naściennymi, co zmniejsza w istotny 
sposób wygodę użytkowania.


\subsection{Rozproszone czujniki i urządzenia wyjściowe}
\label{subsec:rozproszone_czujniki}

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth]{images/rozproszone_czujniki}
\caption{Przykład architektury rozproszonej}
\label{fig:rozproszone_czujniki}
\end{figure}

Inteligentny dom zaprojektowany przy użyciu modułów rozproszonych [rys. \ref{fig:rozproszone_czujniki}] 
składa się z czujników i~urządzeń wyjściowych działających niezależnie od siebie. Jest to najczęściej 
występujący rodzaj systemu, gdyż jest najłatwiejszy do konstrukcji i rozszerzania.

Istotną różnicą pomiędzy systemem rozproszonych urządzeń a autonomicznymi modułami jest jeden interfejs 
użytkownika, co odczuwalnie zwiększa komfort korzystania z całego systemu. Kolejną zauważalną zmianą jest 
zwiększona ilość kontrolerów. Przy każdym sensorze i urządzeniu wyjściowym musi znaleźć się układ, który 
potrafi co najmniej komunikować się z kontrolerem logiki, sczytywać dane z czujników lub podtrzymywać stan 
wyjść.

Zaletami takiego rozwiązania jest łatwa integracja nowych czujników i urządzeń wyjściowych z systemem, 
ponieważ wszystkie działają niezależnie od siebie. Kolejną zaletą jest możliwość, czy wręcz konieczność 
zdefiniowania skryptów, na podstawie których działa kontroler logiki. Dzięki temu możliwości konfiguracji są 
ograniczone jedynie wyobraźnią użytkownika.

Niestety niezależność modułów skutkuje zwiększeniem i rozproszeniem elementów systemu, przez co rośnie 
zużycie energii a także konieczność użycia większej ilości przewodów lub modułów radiowych do połączenia 
między urządzeniami. Skutkuje to zwiększeniem kosztów eksploatacji systemu. Istotną wadą jest także to, 
że~w~przypadku awarii jednego z czujników przestają działać lub zaczynają działać nieprawidłowo wszystkie 
powiązane z~nim skrypty.


\subsection{Systemy hybrydowe}
\label{subsec:systemy_hybrydowe}

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{images/system_schemat}
\caption{Przykład architektury systemu hybrydowego}
\label{fig:system_hybrydowy}
\end{figure}

Systemu inteligentnego domu, zaprojektowany jako system hybrydowy [rys. \ref{fig:system_hybrydowy}] łączy 
zalety obu powyższych rozwiązań. Składa się on z autonomicznych modułów, które spaja jeden główny kontroler 
wraz z jednolitym interfejsem użytkownika. Dzięki takiemu podejściu możliwa jest integracja modułów ze sobą 
przy zachowaniu zmniejszonego zużycia energii. Istnienie głównego kontrolera zarządzającego wszystkimi 
modułami umożliwia przejęcie kontroli za pomocą skryptów lub żądań użytkownika nad poszczególnymi 
urządzeniami wyjściowymi.

Z tej architektury korzysta system opisany w pracy, ponieważ stanowi ona dobry kompromis pomiędzy 
elastycznością a niezawodnością.

\section{Schemat systemu}
\label{sec:opis_systemu}

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{images/system_kontroler}
\caption{Schemat systemu}
\label{fig:system}
\end{figure}

W opracowanym systemie [rys. \ref{fig:system}] można wyróżnić elementy typowe dla systemu hybrydowego: moduły 
zewnętrzne, główny kontroler oraz interfejs użytkownika. Strzałki przerywane oznaczają, że dany komponent 
może pracować w komunikacji bezprzewodowej lub za pośrednictwem sieci internet.

Najbardziej rozbudowanym elementem jest główny kontroler. Składa się on z elementów:
\begin{itemize}
\item webservera, czyli elementu odpowiedzialnego za tworzenie i interakcję z interfejsem użytkownika.
\item bazy danych odpowiedzialnej za przechowywanie danych z czujników oraz ich konsolidowanie, a także 
konfiguracji i stanów poszczególnych urządzeń.
\item kontrolera modułu radiowego, którego zadaniem jest interakcja pomiędzy bazą danych a poszczególnymi 
modułami za pośrednictwem modułu radiowego.
\item głównego modułu radiowego, który odpowiada za komunikację pomiędzy modułami zewnętrznymi a głównym 
kontrolerem logiki.
\end{itemize}

Powyższe elementy zostały umieszczone na komputerze Raspberry Pi model B+. Mogą one jednakże działać na 
dowolnej innej maszynie z systemem GNU/Linux i nie muszą znajdować się na jednym urządzeniu. Wyjątek stanowi 
tu kontroler modułu radiowego, który musi działać na komputerze, do którego podłączony jest główny moduł 
radiowy i musi znajdować się on w zasięgu zewnętrznych modułów, z którymi ma współpracować.