\chapter{Komunikacja radiowa}
\label{cha:radio}

W tym rozdziale opisano moduł radiowy użyty do bezprzewodowej komunikacji pomiędzy elementami systemu, jego 
konfiguracja, a także rodzaje pakietów używanych do przesyłania danych oraz sposób ich wymiany.

\section{Moduł radiowy}
\label{sec:modul_radiowy}

Jako moduł radiowy został wybrany transciever RFM69HW-433S. Urządzenie to działa na częstotliwości 433~MHz. 
W odróżnieniu od modułów działających na wyższych częstotliwościach np. RFM70/73 oraz WiFi (moduł ESP8366EX) 
działające na 2.4 GHz moduł ten ma większą przenikliwość oraz zasięg, co jest istotne jeżeli zajdzie potrzeba 
zastosowania systemu na dużym obszarze. Przykładowo, na rozległym podwórku na wsi, gdzie komunikacja działała 
w promieniu około 100 m przez kilka zabudowań gospodarczych, WiFi przy standardowych dostępnych routerach nie 
jest w stanie w tym przypadku objąć całego domu, a moduły RFM70/73 nie radziły sobie z komunikacją przez 
jedną ścianę.
Głównymi cechami modułu RFM69HW są \cite{rfm69datasheet}:
\begin{itemize}
\item programowalna prędkość wysyłania do 300 kbps,
\item silnik obsługi pakietów zawierający:
\begin{itemize}
\item wykrywanie błędów za pomocą CRC-16,
\item szyfrowanie AES-128,
\item 66-bitowa kolejka FIFO,
\end{itemize}
\item wykrywanie słowa synchronizującego,
\item moc wyjściowa +20 dBm (100 mW),
\item czułość do -120 dBm przy 1.2 kbps,
\item kodowanie manchester,
\item wbudowany czujnik temperatury.
\end{itemize}

\subsection{Konfiguracja modułu radiowego}
\label{subsec:konfiguracja_modulu_radiowego}

Transciever RFM69HW posiada bardzo rozbudowane możliwości konfiguracji. Dla potrzeb realizowanego systemu 
poniżej przedstawiono ustawienia ważniejszych rejestrów:
\begin{itemize}
\item 0x07 do 0x09 -- ustawienie częstotliwości fali nośnej na $\approx434$ MHz zgodnie ze 
wzorem:
$$F_{RF} = \frac{F_{XOSC}}{2^{19}} * Frf(23,0),$$
gdzie $F_{XOSC} = 32$ MHz, a $Frf(23,0)$ jest zawartością rejestrów.

Częstotliwość ta wynika z konstrukcji modułu i nie powinna być znacznie zmieniana.
\item 0x02 -- ustawienie typu modulacji na FSK. Poza nim istnieje również możliwość ustawienia typu OOK. FSK 
jest lepszy przy interferujących falach, jednakże trudniejszy w implementacji \cite{modulations}. Ponieważ 
obsługa obu jest domyślnie wbudowana, można wybrać dowolny z nich.
\item 0x03 oraz 0x04 -- ustawienie chip-rate na 2.4 kbps zgodnie ze wzorem:
$$ChipRate = \frac{F_{XOSC}}{ChipRate(15,0)},$$
gdzie $ChipRate(15,0)$ jest wartością z rejestrów.
Chip-rate nie musi być tożsamy z bit-rate. Jest to zależne od wybranej metody kodowania.
\item 0x05 oraz 0x06 -- ustawienie dewiacji częstotliwości, czyli odchyłu od nośnej przy danym sygnale 
modulującym. Dewiacja powinna znajdować się w zakresie od 600 Hz. Suma dewiacji i połowy chip-rate nie 
powinna przekraczać 500 kHz. Jest określona wzorem:
$$F_{DEV} = \frac{F_{XOSC}}{2^{19}}*F_{DEV}(15,0),$$
gdzie $F_{DEV}(15,0)$ jest wartością rejestrów.
\item 0x13 -- wyłączona została funkcja overload current protection (OCP) w celu zapewnienia największej 
możliwej mocy urządzenia w trybie Tx. Podczas włączania trybu Rx OCP jest włączane.
\item 0x19 -- wybrane zostało pasmo przenoszenia o zakresie 83.3 kHz. Musi ono być większe od dwukrotności 
chip-rate (częstotliwość Nquista). Obliczana jest ona dla modulacji FSK zgodnie ze wzorem:
$$RxBw = \frac{F_{XOSC}}{RxBwMant * 2^{RxBwExp + 2}},$$
gdzie:\\$F_{XOSC} = 32$ MHz,\\$RxBwMant, RxBwExp$ są wartościami z rejestru
\item 0x2C oraz 0x2D -- rozmiar preambuły służący do synchronizacji urządzeń na początku transmisji. Ustalony 
został na 5 bajtów. Preambuła składa się z ciągu zamiennie powtarzających się zer i jedynek (1010…).
\item 0x2E -- włączenie słowa synchronizującego, który może być utożsamiany z adresem sieci. Jego długość 
została ustalona na 3 bajty.
\item 0x2F do 2x31 -- arbitralne ustawienie słowa synchronizującego na wartość 0x900315.
\item 0x37 -- wartości tego rejestru zależne są od urządzenia na którym pracuje. Inne są dla modułów 
zewnętrznych a inne dla głównego modułu radiowego. Zawartość tego rejestru pokazuje tabela 
\ref{tab:register0x37}.

\begin{table}[h!]
\begin{tabularx}{\textwidth}{|c|c|c|c|X|X|}
\hline \textbf{Bity} & \textbf{Nazwa zmiennej} & \textbf{Tryb} & \textbf{Wartość domyślna} & \textbf{Opis} \\
\hline 7    & PacketFormat     & rw   & 0                & Format pakietu: \begin{itemize}
\item 0 -- Stały rozmiar
\item 1 -- Zmienny rozmiar
\end{itemize}\\
\hline 6-5  & DcFree           & rw   & 00               & Typ kodowania DC-free: \begin{itemize}
\item 00 -- Brak
\item 01 -- Manchester
\item 10 -- Whitening
\item 11 -- zarezerwowane
\end{itemize}\\
\hline 4    & CrcOn            & rw   & 1                & Generowanie/sprawdzanie sumy kontrolnej CRC 
(Tx/Rx):
\begin{itemize}
\item 0 -- Wyłączone
\item 1 -- Włączone
\end{itemize}\\
\hline 3    & CrcAutoClearOff  & rw   & 0                & Definiuje zachowanie w przypadku błędnego CRC: 
\begin{itemize}
\item 0 -- Wyczyść kolejkę FIFO i oczekuj na kolejny pakiet
\item 1 -- Nie czyść kolejki FIFO. Ustaw przerwanie \textit{PayloadReady}
\end{itemize}\\
\hline 2-1  & AddressFiltering & rw   & 00               & Filtrowanie bazujące na adresie: \begin{itemize}
\item 00 -- Wyłączone
\item 01 -- Pole adresu musi zgadzać się z wartością \textit{NodeAddress}
\item 10 -- Pole adresu musi zgadzać się z wartością \textit{NodeAddress} lub \textit{BroadcastAddress}
\item 11 -- zarezerwowane
\end{itemize}\\
\hline 0    & -                & rw   & 0                & nieużywana \\
\hline
\end{tabularx}
\caption{Schemat rejestru 0x37 transcievera RFM69HW. Opracowano na podstawie \cite{rfm69datasheet}}
\label{tab:register0x37}
\end{table}
Format pakietu został ustalony na pakiety o zmiennym rozmiarze. Dzięki temu pomimo narzutu jednego bajtu na 
określenie długości pakietu znacznie ograniczona jest ilość przesyłanych danych.

Typ kodowania/dekodowania DC-free został ustalony na Manchester. Przy braku kodowania ilość 
utraconych/błędnych pakietów znajdowała się na poziomie 5-10\%. Po ustawieniu Manchester przy działającym 
jednym urządzeniu wysyłającym i jednym odbiorczym (zapewnienie braku kolizji pakietów), po 1500 wysłanych 
pakietach, utraconych zostało 0. Z przyczyn czasowych testy na tym etapie zostały przerwane.

Sprawdzanie poprawności za pomocą CRC zostało włączone. Pakiety z błędną sumą kontrolną są odrzucane.

Filtrowanie bazujące na adresie jest włączone zarówno dla głównego modułu radiowego jak i modułów 
zewnętrznych. Jednakże moduły zewnętrze mają ustawione filtrowanie po adresie (01) a główny moduł ma również 
po adresie broadcast (10).

\item 0x38 -- Maksymalna długość pakietu. Ustawiona na najwyższą dopuszczalną wartość 64 (0x40).
\item 0x39 -- Ustawienie adresu urządzenia.
\item 0x3C -- Ustawienie momentu rozpoczęcia wysyłania pakietu.
\item 0x3D -- Ustawienie bitu odpowiedzialnego za automatyczny restart trybu Rx po odczytaniu aktualnego 
odebranego pakietu. W tym rejestrze jest również możliwe ustawienie szyfrowania AES-128, klucz jest ustawiany 
w rejestrach od 0x3E do 0x4D.
\item 0x25 -- Wraz z częścią rejestru 0x26 służy do mapowania pinów DIO0 - DIO5. Ustawione zostało mapowanie 
pinu DIO0 na \textit{PayloadReady} dla trybu Rx oraz \textit{PacketSent} dla trybu Tx.
\item rejestry 0x11, 0x5A, 0x5C, oraz 0x13 służą do ustalania mocy nadajnika/odbiornika. Powinny być ustalane 
dla każdego urządzenia z osobna w zależności od potrzeb. Zarówno zbyt niska moc przy dużej odległości oraz 
ilości przeszkód, jak również zbyt duża moc przy małej odległości, mogą powodować problemy z~komunikacją.
\end{itemize}

\section{Rodzaje pakietów}
\label{sec:rodzaje_pakietow}
Komunikacja pomiędzy modułami a głównym modułem radiowym odbywa się za pośrednictwem pakietów. Dla 
poszczególnych rodzajów komunikatów potrzeba wyszczególnić osobne pakiety. Na potrzeby projektowanego systemu 
utworzono cztery rodzaje pakietów [rys \ref{fig:radio_pakiety}]:
\begin{itemize}
\item rejestracyjne -- służące do rejestracji urządzenia w systemie.
\item potwierdzenia rejestracji -- odpowiadające za wiadomości zwrotne dla rejestracji urządzenia. Mogą być 
zarówno pozytywne jak i negatywne.
\item danych -- służące do przesyłania danych z czujników oraz stanów urządzeń wyjściowych.
\item konfiguracji -- odpowiadające za zmianę konfiguracji modułów zewnętrznych.
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{images/radio_pakiety}
\caption{Schemat kierunku przesyłu pakietów komunikacji radiowej}
\label{fig:radio_pakiety}
\end{figure}

\section{Opis komunikacji}
\label{sec:opis_komunikacji}
Komunikacja pomiędzy urządzeniami rozpoczyna się z chwilą włączenia urządzenia. Poszczególne moduły zaczynają 
rozsyłać swoją konfigurację. Po udanej rejestracji czyli wysłaniu konfiguracji, moduły zaczynają wysyłać 
dane. W odpowiedzi na dane, główny kontroler wysyła parametry do modułów, jeżeli zostały zmienione 
[rys.~\ref{fig:radio_komunikacja_main_receiver},~\ref{fig:radio_komunikacja_moduly}].

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{images/radio_komunikacja_main_receiver}
\caption{Uproszczony schemat komunikacji dla głównego odbiornika}
\label{fig:radio_komunikacja_main_receiver}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=0.6\textwidth]{images/radio_komunikacja_modul}
\caption{Uproszczony schemat komunikacji dla modułów}
\label{fig:radio_komunikacja_moduly}
\end{figure}

\subsection{Schemat pakietu danych układu RFM69HW}
\label{subsec:schemat_pakietu_danych_rfm69hw}
Pakiet składa się z sześciu części [rys. \ref{fig:radio_packet_rfm}]:
\begin{itemize}
\item preambuła -- dzięki preambule możliwe jest wykrycie momentu rozpoczęcia nadawania pakietu oraz 
synchronizacja odbiornika z nadajnikiem w celu właściwej interpretacji zmian modulacji.
\item słowo synchronizacji -- pełni rolę adresu sieci. Tylko odbiorniki, które mają słowo synchronizacji o 
tej samej wartości, będą odbierały pozostałą część pakietu.
\item zawartość pakietu [ang. payload] -- zmienia się w zależności od ustawionych parametrów urządzenia oraz 
trybu działania. Poniżej opisano konfigurację dla stworzonego systemu.
\begin{itemize}
\item bajt długości -- określa długość wysyłanego pakietu. Wraz z bajtem adresu oraz danymi jest wliczany w 
długość pakietu.
\item bajt adresu -- adres urządzenia, do którego wysyłany jest pakiet.
\item dane -- właściwe dane wysyłane do urządzenia. W przypadku włączenia szyfrowania AES maksymalna długość 
wynosi 64bajty (minus bajt długości oraz adresu).
\end{itemize}
\item suma kontrolna -- obliczana na podstawie zawartości pakietu. Pozwala określić prawidłowość komunikacji.
\end{itemize}

Kodowanie DC free oznacza, że część danych została zakodowana przy użyciu kodowania Manchester lub Whitening. 
W celu poprawnej interpretacji pakietu, zarówno nadajnik jak i odbiornik muszą mieć ustawiony ten sam typ 
kodowania.

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{images/pakiet_rfm69}
\caption{Schemat pakietu modułu RFM69HW. Opracowano na podstawie \cite{rfm69datasheet}}
\label{fig:radio_packet_rfm}
\end{figure}

\subsection{Struktura danych w pakietach}
\label{subsec:struktura_danych_w_pakietach}
Ponieważ pakiety mają ograniczony rozmiar dlatego w celu maksymalnego ograniczenia ich rozmiaru zastosowano 
reguły umożliwiające jednoznaczne określenie ich typu oraz znaczenia.

\subsubsection{Pakiet rejestracyjny}
\label{subsubsec:pakiet_rejestracyjny}
Pakiet rejestracyjny zawiera nazwę urządzenia, opis wysyłanych danych, oraz opis parametrów.
\begin{lstlisting}[style=customc]
#define DEVICE_NAME_LEN 8
uint8_t DEVICE_NAME[DEVICE_NAME_LEN] =
{
	'B', 'a', 't', 'h', 'r', 'o', 'o', 'm'
};
\end{lstlisting}
Podczas wysyłania napisów wysyłana jest najpierw długość, a następnie dopiero właściwy ciąg znaków.
Konfiguracja parametrów wysyłanych do głównego kontrolera wygląda następująco:
\begin{lstlisting}[style=customc]
uint8_t DATA_CONFIG[DATA_CONFIG_LENGTH] =
{
	8, 'H', 'u', 'm', 'i', 'd', 'i', 't', 'y', 1, 0, 1, 0, 100, 1, 37,
...
	6, 'T', 'o', 'i', 'l', 'e', 't', 0, 0, 2, 0, 0, 3, 255, 1, 37/*%*/,
	9, 'F', 'a', 'n', ' ', 's', 't', 'a', 't', 'e', 0, 0, 0xFF, 2, 3, 'o', 'f', 'f', 2, 'o', 'n',
...
};
...
uint8_t PARAMS_CONFIG[PARAMS_CONFIG_LENGTH] =
{
	10, 'U', 's', 'e', 'r', ' ', 's', 't', 'a', 't', 'e', 0xFF, 
		3, 4, 'a', 'u', 't', 'o', 2, 'o', 'n', 3, 'o', 'f', 'f',
...
	8, 'F', 'a', 'n', ' ', 's', 't', 'o', 'p', 1, 0, 30,
	16, 'T', 'o', 'i', 'l', 'e', 't', ' ', 't', 'h', 'r', 'e', 's', 'h', 'o', 'l', 'd',
		2, 0, 0, 1023 >> 8, (uint8_t)1023,
	17, 'T', 'o', 'i', 'l', 'e', 't', ' ', 's', 'e', 'c', 'o', 'n', 'd', 's', ' ', 'o', 'n',
		1, 0, 255,
...
};
\end{lstlisting}
Każdy parametr rejestracyjny danych posiada jeden schemat, co ułatwia parsowanie:
\begin{itemize}
\item liczba znaków nazwy parametru.
\item nazwa parametru.
\item id dużego wykresu -- określa czy dane mają się wyświetlać na wykresach (na wskaźnikach będą zawsze), 
oraz czy mają być prezentowane wspólnie z innymi, jeżeli mają to samo id. Parametr wskazuje również, czy~dane 
będą zapisywane w tabelach konsolidujących w bazie danych.
\item typ wykresu -- aktualnie nie jest wykorzystywany.
\item typ danych -- wartości liczbowe tego pola określają wysyłany typ.
\begin{itemize}
\item 1-200 -- dla wartości poniżej 100 typ unsigned int. wartość określa ilość bajtów. Przykładowo dla 
wartości 2 będą wysyłane 2 bajty. dla wartości powyżej 100 interpretowany jest typ signed int analogicznie 
jak unsigned.
\item 255 -- typ wyliczeniowy (enum). Oznacza, że wartości będą mapowane na konkretne ciągi znaków zrozumiałe 
dla użytkownika.
\end{itemize}
\end{itemize}
W zależności od typu danych, kolejne bajty interpretowane mogą być na dwa sposoby. Jeżeli został określony 
typ wyliczeniowy:
\begin{itemize}
\item ilość parametrów wyliczenia,
\item długość nazwy n-tego parametru,
\item nazwa n-tego parametru.
\end{itemize}
\pagebreak
W przeciwnym razie:
\begin{itemize}
\item minimalna wartość danej,
\item maksymalna wartość danej,
\item długość nazwy jednostki,
\item nazwa jednostki.
\end{itemize}

Dane, znajdujące się w pakietach danych oraz konfiguracji interpretowane są na podstawie kolejności ich 
występowania w pakiecie rejestracyjnym.

\subsubsection{Pakiet konfiguracyjny}
\label{subsubsec:pakiet_konfiguracyjny}
Schemat pakietu konfiguracyjnego dla urządzenia obsługującego łazienkę:
\begin{lstlisting}[style=customc]
/**
* packet[0]	// packet length
* packet[1]	// receiver address
* packet[2]	// packet type
* packet[3]	// new forced fan state
...
* packet[11]	// crc (sum of all previous)
**/
\end{lstlisting}
Pierwsze dwa bajty są integralną częścią pakietu rfm69hw (\ref{subsec:schemat_pakietu_danych_rfm69hw}). 
Kolejny bajt określa typ pakietu, w celu poprawnej interpretacji jego zawartości. Następne bajty służą do 
przesyłu odpowiednich danych. Ostatni bajt jest sumą kontrolną obliczaną jako suma poprzednich bajtów 
rzutowana na ośmiobitowy typ unsigned int. Jest on obliczany niezależnie od CRC zawartego w pakiecie rfm69, 
ponieważ ogranicza on możliwość wystąpienia błędu wynikającego z przesyłu danych do i z odbiornika radiowego.

\subsubsection{Pakiet danych}
Schemat pakietu danych dla urządzenia obsługującego łazienkę:
\label{subsubsec:pakiet_danych}
\begin{lstlisting}[style=customc]
packet[0] = packetLength;
packet[1] = RFM69_BROADCAST_ADDRESS;    // send broadcast
packet[2] = RFM69_NODE_ADDRESS;         // send this node address
packet[3] = RFM69_PACKET_TYPE_DATA;
packet[4] = packetNo++;                 // count packets
packet[5] = packetReceived;             // send if ack was received
packet[6] = dht22Data.humidity;         // send humitity
...
packet[8] = toiletADCcurrentValue >> 8; // send toilet adc msb
packet[9] = toiletADCcurrentValue;      // adc lsb
...
packet[13] = 0;                         // for crc
\end{lstlisting}
Analogicznie jak w przypadku pakietu konfiguracyjnego (\ref{subsubsec:pakiet_konfiguracyjny}), jednak z 
drobnymi zmianami. Na pozycji 2, wysyłany jest adres urządzenia wysyłającego, w celu łatwej identyfikacji 
nadawcy. Pozycja 4 określa numer pakietu. Wraz z bajtem 5, umożliwia stwierdzenie poprawności przesyłu 
pakietu konfiguracji, ponieważ bajt 5 ustawiany jest w momencie odebrania jakiegokolwiek pakietu przez 
urządzenie. Bajty 8 i 9 pokazują sposób przesyłu danych 2-bajtowych.

\section{Wykrywanie kolizji pakietów}
\label{sec:wykrywanie_kolizji_pakietow}

W przypadku istnienia możliwości jednoczesnego nadawania przez kilka urządzeń następuje problem kolizji 
pakietów. Polega on na jednoczesnym nadawaniu kilku urządzeń, przez co dane z obu są zaburzone na tyle, że 
nie da się ich odebrać poprawnie. Układ RFM69HW umożliwia częściowe rozwiązanie tego problemu.

W rejestrze flag przerwań (0x27) znajduje się bit odpowiedzialny za wykrycie słowa synchronizacji (adresu 
sieciowego). Przed wysłaniem pakietu należy sprawdzić czy został on wykryty, dzięki czemu możemy uniknąć 
kolizji. Nie jest to rozwiązanie idealne, ponieważ pomiędzy sprawdzeniem pojawienia się adresu sieci a 
wysłaniem pakietu inne urządzenie może zacząć nadawać. Również nie zostanie wykryty nadajnik, który zaczął 
przesył danych, a wciąż go nie zakończył. Możliwe jest również wykrywanie kolizji za pomocą RSSI, jednakże 
wykrycie nośnej nie musi oznaczać przesyłania pakietu.