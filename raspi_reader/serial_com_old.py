import threading
import signal
import sys
import mysql.connector
import serial
import datetime
import time

exit = 0

#read data and save it to a database
def read_usart():
	while ( exit == 0 ):
		i = 0
		data = usart.readline()
		print data
		if(data.split(' ')[0].split('x')[0] == '0'):
			#sys.stdout.write("packet received at:" + str(datetime.datetime.now()) + " packet: " + data + "\n")
			write_db(data)
	print "read_usart exitted!"	
	
#write data to usart
def write_usart(data):
	usart.write(data)
	
#read database and send data to mega8
def read_db():
	# packet[0]	// receiver address
	# packet[1]	// packet no
	# packet[2]	// config changed
	# packet[3]	// forced fan state changed
	# packet[4]	// new forced fan state
	# packet[5]	// fanStartDiffVal changed
	# packet[6]	// new fanStartDiffVal
	# packet[7]	// fanStopDiffVal changed
	# packet[8]	// new fanStopDiffVal
	# packet[9]	// force sensor threshold changed
	# packet[10]	// new force sensor threshold H
	# packet[11]	// new force sensor threshold L
	# packet[12]	// toilet time seconds changed
	# packet[13]	// new toiletTimeSecondsOn (0-255)
	query = "SELECT * FROM  `bathroom_params` WHERE `id` = 1"
	cursor.execute(query)
	row = cursor.fetchone()
	if (row[1] == 0):
		print "packet to send:" + str(row[0]) + "," + str(row[1]) + "," + str(row[2]) + "," + str(row[3]) + "," + str(row[4]) + "," + str(row[5]) + "," + str(row[6])
 		#blad kolejnosci! powinno byc tak jak zakomentowane
		#bytes = [int(0x02),0,1,1,int(row[2]),1,int(row[3]),1,int(row[4]),1,int(row[5])>>8,int(row[5])&int(0x00FF),1,int(row[6])]
		bytes = [1,int(row[2]),1,int(row[3]),1,int(row[4]),1,int(row[5])>>8,int(row[5])&int(0x00FF),1,int(row[6]), int(0x02),0,1]
		write_usart("".join(map(chr, bytes)))
		time.sleep(1)
	while( exit == 0 ):
		time.sleep(1)
		print "packet to send:" + str(row[0]) + "," + str(row[1]) + "," + str(row[2]) + "," + str(row[3]) + "," + str(row[4]) + "," + str(row[5]) + "," + str(row[6])
		cursor.execute(query)
		row = cursor.fetchone()
		if (row[1] != 0):
			continue
		#prepare data
		bytes = [1,int(row[2]),1,int(row[3]),1,int(row[4]),1,int(row[5])>>8,int(row[5])&int(0x00FF),1,int(row[6]), int(0x02),0,1]
		write_usart("".join(map(chr, bytes)))
	print "read_db exitted!"

#write mega8 data to database
def write_db(data):
	tmpData = data.split(' ')
	humidity = str(int(tmpData[4],16))
	temperature = str(int(tmpData[5],16))
	tmptoilet = 0
	tmptoilet = int(tmpData[6],16) << 8
	tmptoilet |= int(tmpData[7],16)
	toilet = str(tmptoilet)
	fan = str(int(tmpData[8],16))
	query = "INSERT INTO `home_automation_petrykozy`.`bathroom_data` (`id`, `date`, `humidity`, `temperature`, `toilet`, `fan_state`) VALUES (NULL, CURRENT_TIMESTAMP, \'"+ humidity +"\', \'"+ temperature +"\', \'"+ toilet +"\', \'"+ fan +"\');";
	cursor.execute(query)
	con.commit()

#handle sigint
def signal_handler(signal, frame):
	global exit
	print('exitting')
	exit = 1

#main part#
usart = serial.Serial("/dev/ttyAMA0")
exit = 0
#connet to database
con = mysql.connector.connect(host = '127.0.0.1', user = 'root', password = 'iksiksel', database = 'home_automation_petrykozy')
cursor = con.cursor()

# Create two threads as follows
try:
	thread1 = threading.Thread( target = read_usart )
	thread2 = threading.Thread( target = read_db )
except:
	print "Error: unable to start thread"

signal.signal(signal.SIGINT, signal_handler)

thread1.start()
thread2.start()

#without loop, interrupt does not work
while (exit == 0):
	time.sleep(1)

#waiting for threads to finish
thread1.join()
thread2.join()

#exitting program
usart.close()
cursor.close()
con.close()

print "shutdown..."

sys.exit(0)
