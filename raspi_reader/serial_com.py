import threading
import signal
import sys
import mysql.connector
import serial
import datetime
import time

#read data and save it to a database
def read_usart():
	global remoteApplied
	global paramsChanged
	paramsIter = 0 # two zeros in row after sending packet
	while ( exitting == 0 ):
		i = 0
		data = usart.readline()
		print data
		print "params iter " + str(paramsIter)
		if(data.split(' ')[0].split('x')[0] == '0'):
			if(paramsChanged == True):
				if(data.split(' ')[3] == "0x0"):
					paramsIter += 1
					if(paramsIter == 2):
						paramsIter = 0
						remoteApplied = True
						paramsChanged = False
				else:
					paramsIter = 0
			#sys.stdout.write("packet received at:" + str(datetime.datetime.now()) + " packet: " + data + "\n")
			write_db(data)
	print "read_usart exitted!"	
	
#write data to usart
def write_usart(data):
	usart.write(data)
	
#read database and send data to mega8
def read_db():
	global paramsChanged
	global remoteApplied
	# packet[0]	// receiver address
	# packet[1]	// packet no
	# packet[2]	// config changed
	# packet[3]	// forced fan state changed
	# packet[4]	// new forced fan state
	# packet[5]	// fanStartDiffVal changed
	# packet[6]	// new fanStartDiffVal
	# packet[7]	// fanStopDiffVal changed
	# packet[8]	// new fanStopDiffVal
	# packet[9]	// force sensor threshold changed
	# packet[10]	// new force sensor threshold H
	# packet[11]	// new force sensor threshold L
	# packet[12]	// toilet time seconds changed
	# packet[13]	// new toiletTimeSecondsOn (0-255)
	q = "UPDATE `bathroom_params` SET `approved`='1' WHERE `bathroom_params`.`id` = 1"
	query = "SELECT * FROM  `bathroom_params` WHERE `id` = 1"
	cursor_read.execute(query)
	row = cursor_read.fetchone()
	oldrow = row
	if (row[1] == 0):
		print "packet to send:" + str(row[0]) + "," + str(row[1]) + "," + str(row[2]) + "," + str(row[3]) + "," + str(row[4]) + "," + str(row[5]) + "," + str(row[6])
 		#blad kolejnosci! powinno byc tak jak zakomentowane
		#bytes = [int(0x02),0,1,1,int(row[2]),1,int(row[3]),1,int(row[4]),1,int(row[5])>>8,int(row[5])&int(0x00FF),1,int(row[6])]
		data = [1,int(row[2]),1,int(row[3]),1,int(row[4]),1,int(row[5])>>8,int(row[5])&int(0x00FF),1,int(row[6]), int(0x02),0,1]
		write_usart("".join(map(chr, data)))
		paramsChanged = True
		remoteApplied = False
		time.sleep(1)
	while( exitting == 0 ):
		#if remote device received applied, set applied to 1 and make row - oldrow
		if(remoteApplied == True):
			print("applied!")
			cursor_read.execute(q)
			con_read.commit()
			oldrow = row
			
			time.sleep(1)
			con_read.commit()
			cursor_read.execute(query)
			row = cursor_read.fetchone()
			if (row[1] != 0):
				print("nothing to send")
				continue
			print "packet to send in loop:" + str(row[0]) + "," + str(row[1]) + "," + str(row[2]) + "," + str(row[3]) + "," + str(row[4]) + "," + str(row[5]) + "," + str(row[6])
			#prepare data #TODO change it for USB
			data = [1,int(row[2]),1,int(row[3]),1,int(row[4]),1,int(row[5])>>8,int(row[5])&int(0x00FF),1,int(row[6]), int(0x02),0,1]
			if(row[2] != oldrow[2]):
				data[0] = 0
			if(row[3] != oldrow[3]):
				data[2] = 0
			if(row[4] != oldrow[4]):
				data[4] = 0
			if(row[5] != oldrow[5]):
				data[6] = 0
			if(row[6] != oldrow[6]):
				data[8] = 0
			write_usart("".join(map(chr, data)))
			paramsChanged = True
			remoteApplied = False
		else:
			write_usart("".join(map(chr, data)))
			time.sleep(1)
	print "read_db exitted!"

#write mega8 data to database
def write_db(data):
	tmpData = data.split(' ')
	humidity = str(int(tmpData[4],16))
	temperature = str(int(tmpData[5],16))
	tmptoilet = 0
	tmptoilet = int(tmpData[6],16) << 8
	tmptoilet |= int(tmpData[7],16)
	toilet = str(tmptoilet)
	fan = str(int(tmpData[8],16))
	query = "INSERT INTO `home_automation_petrykozy`.`bathroom_data` (`id`, `date`, `humidity`, `temperature`, `toilet`, `fan_state`) VALUES (NULL, CURRENT_TIMESTAMP, \'"+ humidity +"\', \'"+ temperature +"\', \'"+ toilet +"\', \'"+ fan +"\');";
	cursor_write.execute(query)
	con_write.commit()

#handle sigintmysql.connector.pooling.PooledMySQLConnection

def signal_handler(signal, frame):
	global exitting
	print('user interrupt!')
	exitting = 1

#main part#
usart = serial.Serial("/dev/ttyAMA0")
exitting = 0
remoteApplied = True
paramsChanged = False
#connet to database
con_read = mysql.connector.connect(pool_name = 'db_pool', pool_size = 2, host = '127.0.0.1', user = 'root', password = 'iksiksel', database = 'home_automation_petrykozy')
con_write = mysql.connector.connect(pool_name = 'db_pool')

cursor_read = con_read.cursor()
cursor_write = con_write.cursor() 

# Create two threads as follows
try:
	thread1 = threading.Thread( target = read_usart )
	thread2 = threading.Thread( target = read_db )
except:
	print("Error: unable to start thread")

signal.signal(signal.SIGINT, signal_handler)

thread1.start()
thread2.start()

#without loop, interrupt does not work
while (exitting == 0):
	time.sleep(1)

#waiting for threads to finish
thread1.join()
thread2.join()

#exitting program
usart.close()
cursor_read.close()
cursor_write.close()
con_read.close()
con_write.close()

print("shutdown...")

sys.exit(0)
