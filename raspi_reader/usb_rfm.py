import usb.core
import usb.util
import mysql.connector
import time
import datetime
import signal
import Queue
import threading
import sys
import os
import traceback

#rfm69 modes
MODE_RX			= 0x00
MODE_TX			= 0x01

#for usb commands
USB_TRANSFER_HOST_TO_DEVICE = 0x40
USB_TRANSFER_DEVICE_TO_HOST = 0xC0

#usb commands
SET_MODE			= 0x00
CHECK_MODE_SET		= 0x01
CHECK_PACKET_RT		= 0x02 # check if packet was send(tx) or received(rx)
DOWNLOAD_PACKET		= 0x03 # download packet if there is any in rfm fifo
UPLOAD_PACKET		= 0x04 # upload packet to send
RESEND_PACKET		= 0x05 # resends last uploaded packet

#interrupt messages
PACKET_RECEIVED		= 0x01
MODE_SET			= 0x02
PACKET_SENT			= 0x03

#packet acks
RFM69_PACKET_TYPE_NO_PACKET		= 0x00 #indicate that no packet was received
RFM69_PACKET_TYPE_DATA			= 0x01
RFM69_PACKET_TYPE_CONFIG		= 0x02
RFM69_PACKET_TYPE_REGISTER		= 0x03
RFM69_PACKET_TYPE_REGISTER_ACK	= 0x04
RFM69_PACKET_TYPE_REGISTER_FAIL	= 0x05

#database
UPDATE_DEVICES = 0x01

params_queue = Queue.Queue()
data_for_average_queue = Queue.Queue()

readComplete_queue = Queue.Queue()
read_queue = Queue.Queue()
write_queue = Queue.Queue()

devices = {} # dictionary {device address : dictionary {index:type}}
parametersToSend = [] # list of parameters to send to devices
registeringDevices = [] # list of currently registering devices
exitting = False # indicate threads to end

def signal_handler(signal, frame):
	global exitting
	print('user interrupt!')
	exitting = True


def setupUsb():
	usbDev = None
	while(usbDev == None and exitting == False):
		print("Waiting for device...")
		#idVendor=0x16C0, idProduct=0x06DE
		usbDev = usb.core.find(idVendor=0x16C0, idProduct=0x06DE)
		if(usbDev == None):
			time.sleep(1)
	try:
		usbDev.set_configuration()
	except Exception as error:
		print type(error)
		print error.args
		print "line " + str(sys.exc_info()[-1].tb_lineno)
		os.kill(os.getpid(),signal.SIGINT)
	print("Device Found!")
	return usbDev


def read_db():
	#selecting db for current data
	'''
	 * packet[0]	// packet length
	 * packet[1]	// receiver address
	 * packet[2]	// packet type
	 * packet[3:n-1]// params
	 * packet[n]	// crc (sum of all previous)
	'''
	#select devices data
	query_select_devices = "SELECT `address`,`type`,`index` FROM `data_types`,`devices` WHERE `data_types`.`device`=`devices`.`id`"
	cursor_read.execute(query_select_devices)
	rows = cursor_read.fetchall()
	for row in rows:
		if not row[0] in devices:
			devices[row[0]] = {}
		devices[row[0]][row[2]] = row[1]
		
	#first select new params
	query_select_params = '''SELECT `devices`.`address`, `configuration`.`id`, `configuration_parameters`.`type`, `configuration_parameters`.`index`,`configuration_parameters`.`value` 
						FROM `configuration` 
						JOIN `configuration_parameters` ON `configuration`.`id`  = `configuration_parameters`.`configuration` 
						JOIN `devices` ON `configuration`.`device`  = `devices`.`id` 
						WHERE `applied` = 0'''
	cursor_read.execute(query_select_params)
	devices_params = []
	new_params = cursor_read.fetchall()
	con_read.commit()
	for param in new_params:
		try:
			device = next(item for item in devices_params if item['address'] == param[0])
			new_param = {'type' : param[2], 'index' : param[3], 'value' : param[4]}
			device['params'].append(new_param)
		except (StopIteration, KeyError):
			new_device = dict()
			new_device['address'] = param[0]
			new_device['config_id'] = param[1]
			new_device['params'] = [{'type' : param[2], 'index' : param[3], 'value' : param[4]}]
			devices_params.append(new_device)
	#sort by index
	for device_params in devices_params:
		device_params['params'] = sorted(device_params['params'], key=lambda param: param['index'])
		params_queue.put(prepareParamsPacket(device_params))
	
	devices_params_old = devices_params
	
	while( exitting == False ):
		#queue for updating devices
		if(read_queue.empty() == False):
			read_queue.get()
			cursor_read.execute(query_select_devices)
			rows = cursor_read.fetchall()
			con_read.commit()
			for row in rows:
				if not row[0] in devices:
					devices[row[0]] = {}
				devices[row[0]][row[2]] = row[1]
			print(devices)
			readComplete_queue.put(True);
		
		#check if there are new params and send new only
		cursor_read.execute(query_select_params)
		new_params = cursor_read.fetchall()
		con_read.commit()
		devices_params = []
		for param in new_params:
			try:
				device = next(item for item in devices_params if item['address'] == param[0])
				new_param = {'type' : param[2], 'index' : param[3], 'value' : param[4]}
				device['params'].append(new_param)
			except (StopIteration, KeyError):
				new_device = dict()
				new_device['address'] = param[0]
				new_device['config_id'] = param[1]
				new_device['params'] = [{'type' : param[2], 'index' : param[3], 'value' : param[4]}]
				devices_params.append(new_device)
		for device_params in devices_params:
			device_params['params'] = sorted(device_params['params'], key=lambda param: param['index'])
			
		#print("\n\n")
		#print("new before " + str(devices_params))
		#print("old before " + str(devices_params_old))
		new_to_send = [item for item in devices_params if item not in devices_params_old]
		devices_params_old = devices_params
		#print("new send " + str(new_to_send))
		for param in new_to_send:
				params_queue.put(prepareParamsPacket(param))
		#print("old after " + str(devices_params_old))
		#print("\n\n")
		#sleep for one second
		time.sleep(1)
	print "read_db exitted!"


def prepareParamsPacket(device_params):
	''' used by readDb'''
	packet = [0] # we don't know length now
	packet.append(device_params['address'])
	packet.append(RFM69_PACKET_TYPE_CONFIG)
	#assume sorted
	for param in device_params['params']:
		if int(param['type']) == 1 or int(param['type']) == 0xFF:
			packet.append(int(param['value']))
		else:
			# multi-byte param
			value = int(param['value'])
			for i in range(param['type']):
				packet.append((value & 0xFF << 8*(int(param['type']) - i - 1)) >> 8*(int(param['type']) - i - 1));
	packet.append(0) # crc
	#length
	packet[0] = len(packet)
	#compute crc
	for i in range(len(packet) - 1):
		packet[len(packet) - 1] += packet[i]
	packet[len(packet) - 1] = packet[len(packet) - 1] % 256
	#prepare update query, for updating when params applied
	query = '''UPDATE `configuration`
				SET `applied` = 1
				WHERE `id` = {config_id}; '''.format(config_id = device_params['config_id'])
	for i in range(len(device_params['params'])):
		query += '''UPDATE `configuration_parameters`
					SET `value` = {value}
					WHERE `configuration` = {config_id} AND `index` = {index} 
					; '''.format(config_id = device_params['config_id'], value = device_params['params'][i]['value'],index = i)
	query = ' '.join(query.split()) # for being readable
	return {'packet' : packet, 'address' : device_params['address'], 'send_on' : None, 'update_query' : query}


def write_db():
	while (exitting == False):
		if(write_queue.empty() == True):
				time.sleep(1)
				continue
		#print "Write db"
		insertQuery = write_queue.get()
		#for multi = True we must iterate over results (why?)
		for result in cursor_write.execute(insertQuery,multi = True):
			pass
		con_write.commit()
	print "write_db exitted!"


def compute_average():
	average_data = []
	startTime = int(time.time())
	while( exitting == False ):
		if(data_for_average_queue.empty() == True):
				time.sleep(1)
				#send to database
				if int(time.time()) - startTime >= 60: # one minute
					query = '''INSERT INTO `data_minutely`(`data_type`, `date`, `value`) VALUES '''
					date_query_part = '''),DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:00'),'''
					for device_data in average_data:
						device_id = device_data['device']
						for data in device_data['avg_data']:
							find_id_query_part = '''((SELECT `id` FROM `data_types` WHERE `index`={index} AND `device`=(SELECT `id` FROM `devices` WHERE `address`={address})'''.format(index=data['index'], address=device_id)
							#compute avg
							#print("data = " + str(data['value']))
							#print("iter = " + str(data['iterations']))
							if data['value'] != None:
								avg = data['value'] / data['iterations']
							else:
								avg = "NULL"
							#reset data
							data['value'] = None
							data['iterations'] = 0
							values_query = find_id_query_part + date_query_part + str(avg) + "),"
							query += values_query
					query = query[:-1] #remove last ,
					if len(average_data) != 0: # no data acquired
						write_queue.put(query)
					print("minute interval!")
					startTime = int(time.time())
				continue
		#append new data
		new_data = data_for_average_queue.get()
		try:
			device = next(item for item in average_data if item['device'] == new_data['device'])
			try:
				index = next(item for item in device['avg_data'] if item['index'] == new_data['index'])
				if index['value'] != None:
					index['value'] += new_data['value']
				else:
					index['value'] = new_data['value']
				index['iterations'] += 1
			except (StopIteration, KeyError):
				new_index = {'index' : new_data['index'], 'value' : new_data['value'], 'iterations' : 1}
				device['avg_data'].append(new_index)
		except (StopIteration, KeyError):
			new_device = dict()
			new_device['device'] = new_data['device']
			new_device['avg_data'] = [{'index' : new_data['index'], 'value' : new_data['value'], 'iterations' : 1}]
			average_data.append(new_device)
	print "compute_average exitted!"


def setMode(usbDev,newMode):
	while(not exitting):
		try:
			usbDev.ctrl_transfer(USB_TRANSFER_HOST_TO_DEVICE, SET_MODE, newMode, 0, 1)
			ret = usbDev.read(0x81, 2, 2000)
			if (len(ret) == 2 and ret[0] == MODE_SET and ret[1] == newMode):
				print('mode set ' + str(ret))
				mode = newMode
				break
		except usb.core.USBError as error:
			if error.args[1] == 'Connection timed out':
				continue
			else:
				print type(error)
				print error.args
				print "line " + str(sys.exc_info()[-1].tb_lineno)
				os.kill(os.getpid(),signal.SIGINT)
		except Exception as error:
			print type(error)
			print error.args
			print "line " + str(sys.exc_info()[-1].tb_lineno)
			os.kill(os.getpid(),signal.SIGINT)


def sendPacket(usbDev,packet):
	setMode(usbDev,MODE_TX)
	print("MODE TX")
	length = usbDev.ctrl_transfer(USB_TRANSFER_HOST_TO_DEVICE, UPLOAD_PACKET, 0, 0, packet)
	assert length == len(packet)
	print "SENDING ACK..."
	try:
		ret = [0]
		while ret[0] != PACKET_SENT: # timeout
			ret = usbDev.read(0x81, 2, 1000)
			if len(ret) != 2 or ret[0] != PACKET_SENT:
				pass
				#bug - there is sometimes interrupt from setmode tx left. why?
				print("failed to send ack, device returned: " + str(ret))
			else:
				print("ack ok " + str(ret))
		print 'ACK SENT'
	except usb.core.USBError as error:
		if error.args[1] == 'Connection timed out':
			print "Sending ACK timeout"
			pass
		else:
			print type(error)
			print error.args
			print "line " + str(sys.exc_info()[-1].tb_lineno)
			os.kill(os.getpid(),signal.SIGINT)
	except Exception as error:
		print type(error)
		print error.args
		print "line " + str(sys.exc_info()[-1].tb_lineno)
		os.kill(os.getpid(),signal.SIGINT)
	#set RX again
	setMode(usbDev,MODE_RX)
	print "MODE RX"
	''''''
	pass


def packetDataHandler(usbDev,deviceAddress,deviceData):
	global parametersToSend
	print("data packet. device address: " + str(deviceAddress) +" packet=" + str(deviceData))
	
	#update parametersToSend
	while True:
		try:
			parametersToSend.append(params_queue.get_nowait())
		except:
			break;
	
	#check if device is in database
	if not deviceAddress in devices.keys():
		print("device address " + str(deviceAddress) + " is not in database!")
		return
		
	#save to database
	deviceDataParams = devices[deviceAddress]
	data = deviceData[2:]
	for key in sorted(deviceDataParams.keys()):
		typeSize = 0
		dataType = deviceDataParams[key]
		if(dataType == 0xFF):
			typeSize = 1 # enum
		else:
			typeSize = dataType % 100
		
		newData = 0
		for j in range(typeSize): 
			newData |= data.pop(0) << (8*(typeSize - j - 1))
		
		if dataType < 200 and dataType > 100: # signed?
			if newData & (0x80 << 8 * (typeSize - 1)) != 0: #got sign?
				newData = -((newData - 1)  ^ ((1 << 8 * typeSize) - 1 ))
		
		insertData = '''INSERT INTO `data_current`(`data_type`, `date`, `value`)
			VALUES ((SELECT `id` FROM `data_types` WHERE `device`= (SELECT `id` FROM `devices` WHERE `address`={address}) and `index`={index}),NOW(),{value})
			ON DUPLICATE KEY UPDATE `date` = NOW(), `value`={value}'''.format(address = deviceAddress, index=key,value = newData)
		write_queue.put(insertData)
		
		#avg data, without enums
		if dataType != 0xFF:
			data_for_average_queue.put({'device':deviceAddress,'index':key,'value':newData})
		
	#if there is/was something to send for this device
	try:
		params = next(item for item in parametersToSend if item['address'] == deviceAddress)
		#check if there are two packets with ack in row
		if params['send_on'] != None: #indicate that config was sent
			if deviceData[1] != 0: # indicate that config was received
				if params['send_on'] + 1 == deviceData[0]:
					#update database
					#print(params['update_query'])
					write_queue.put(params['update_query'])
					#remove from list
					parametersToSend.remove(params)
					print("Parameters applied!")
				else: # was sent, and received ack, but in wrong packet idx
					send_params(usbDev,params,deviceData)
			else: # not received
				send_params(usbDev,params,deviceData)
		else: # not sent
			send_params(usbDev,params,deviceData)
	except (StopIteration, KeyError):
		#nothing to send
		pass
	pass


def send_params(usbDev,params,deviceData):
	print("something to send here! " + str(params['address']) + " " + str(params['packet']))
	sendPacket(usbDev,params['packet'])
	params['send_on'] =  deviceData[0] if deviceData[0] != 255 else -1


def send_register_ack(usbDev,deviceAddress,value):
	ackToSend = [4,deviceAddress,value,0]
	crc = sum(ackToSend[0:-1]) % 256
	ackToSend[-1] = crc
	print("Register Ack: len=" + str(len(ackToSend)) + " data=" + str(ackToSend))
	sendPacket(usbDev,ackToSend)


def packetRegisterHandler(usbDev,deviceAddress,deviceData):
	global registeringDevices
	print("register packet address :" + str(deviceAddress) +" packet=" + str(deviceData))
	
	#check if device is already registering and add to list
	try:
		device = next(item for item in registeringDevices if item['address'] == deviceAddress)
		if device['last_received'] + 1 != deviceData[0]: # wrong packet index
			registeringDevices.remove(device)
			send_register_ack(usbDev,deviceAddress,RFM69_PACKET_TYPE_REGISTER_FAIL)
			return
		device['last_received'] = deviceData[0]
		device['registering_data'] += deviceData[2:]
		"""
		send ack. acks are sent before processing data
		because it can take too long (especially database operations)
		however if script sends register fail, device will register again
		"""
		send_register_ack(usbDev,deviceAddress,RFM69_PACKET_TYPE_REGISTER_ACK)
	except (StopIteration, KeyError):
		if deviceData[0] != 0: # wrong packet index
			send_register_ack(usbDev,deviceAddress,RFM69_PACKET_TYPE_REGISTER_FAIL)
			return
		new_device = dict()
		new_device['address'] = deviceAddress
		new_device['last_received'] = deviceData[0]
		new_device['registering_data'] = deviceData[2:]
		registeringDevices.append(new_device)
		#send ack
		send_register_ack(usbDev,deviceAddress,RFM69_PACKET_TYPE_REGISTER_ACK)
	
	if(deviceData[1] == 0):
		#find packet
		try:
			device = next(item for item in registeringDevices if item['address'] == deviceAddress)
			registeringData = device['registering_data']
			registeringDevices.remove(device)
		except (StopIteration, KeyError):
			send_register_ack(usbDev,deviceAddress,RFM69_PACKET_TYPE_REGISTER_FAIL)
			return
			
		try:
			#print("register data = " + str(registeringData))
			#parse device name
			n=registeringData.pop(0)
			deviceName = ""
			for i in range(n):
				deviceName+=chr(registeringData.pop(0))
			#print("deviceName = " + deviceName)
			#parse types
			typesData = registeringData[1:registeringData[0] + 1]
			#print("types data = " + str(typesData))
			registeringData = registeringData[registeringData[0] + 1:]
			configData = registeringData[1:registeringData[0] + 1]
			#print("config data = " + str(configData))
			registeringData = registeringData[registeringData[0] + 1:]
			#print("registering data = " + str(registeringData))
			if not len(registeringData) == 0: # will not be able to parse this packet
				send_register_ack(usbDev,deviceAddress,RFM69_PACKET_TYPE_REGISTER_FAIL)
				return
			types = []
			while not (len(typesData) == 0):
				#parse name
				n = typesData.pop(0)
				name = ""
				for i in range(n):
					name += chr(typesData.pop(0))
				#parse big chart
				bigChart = ()
				bigChart += (typesData.pop(0),)
				bigChart += (typesData.pop(0),)
				#parse type
				dataType = typesData.pop(0) # in bytes
				typeConfig = []
				if(dataType != 0xFF):
					# min / max vals
					for i in range (2): #always two values
						x = 0
						for j in range(dataType % 100): # for signed and unsigned 
							x |= typesData.pop(0) << (8*((dataType % 100) - j - 1));
						if dataType < 200 and dataType > 100: #change value when signed
							if x & (0x80 << 8 * ((dataType % 100) - 1)) != 0: #got sign?
								x = -((x - 1) ^ ((1 << 8 * (dataType % 100)) - 1 ))
						typeConfig.append(x)
					#unit
					unitLen = typesData.pop(0)
					unit = ""
					for i in range(unitLen):
						unit += unichr(typesData.pop(0))
					unit = unit.encode('utf-8')
					types.append((name,bigChart,dataType,typeConfig,unit))
				#enum type
				if(dataType == 0xFF):
					n = typesData.pop(0)
					#parse names for enum
					for i in range(n):
						m = typesData.pop(0)
						enumName = ""
						for i in range(m):
							enumName += chr(typesData.pop(0))
						typeConfig.append(enumName)
					types.append((name,bigChart,dataType,typeConfig))
			print(str(types))
			
			#parse config
			config = []
			while not (len(configData) == 0):
				#parse name
				n = configData.pop(0)
				name = ""
				for i in range(n):
					name += chr(configData.pop(0))
				#parse type
				configType = configData.pop(0) # in bytes
				typesConfig = []
				if(configType != 0xFF):
					for i in range (2): #always two values
						x = 0
						for j in range(configType % 100): # signed and unsigned 
							x |= configData.pop(0) << (8 * ((configType % 100) - j - 1));
						if configType < 200 and configType > 100: #change value when signed
							if x & (0x80 << 8 * ((configType % 100) - 1)) != 0: #got sign?
								x = -((x - 1) ^ ((1 << 8 * (configType % 100)) - 1 ))
						typesConfig.append(x)
				#enum type
				if(configType == 0xFF):
					n = configData.pop(0)
					#parse names for enum
					for i in range(n):
						m = configData.pop(0)
						enumName = ""
						for i in range(m):
							enumName += chr(configData.pop(0))
						typesConfig.append(enumName)
				config.append((name,configType,typesConfig))
			print(str(config))
			
			#save to db
			deviceInsert = "INSERT INTO `devices` (address,name) VALUES ({address},'{name}') ON DUPLICATE KEY UPDATE name='{name}'".format(address=deviceAddress,name=deviceName)
			write_queue.put(deviceInsert)
			configurationInsert = "INSERT IGNORE INTO `configuration` (device,applied) VALUES ((SELECT `id` FROM `devices` WHERE `address`={address}),1)".format(address=deviceAddress)
			write_queue.put(configurationInsert)
			for index in range(len(types)):
				if(types[index][2] != 0xFF):
					dataTypeInsert = '''INSERT INTO data_types (`device`,`name`,`type`,`big_chart_id`,`big_chart_type`,`index`,`unit`) 
							VALUES ((SELECT `id` FROM `devices` WHERE `address`={address}),
							'{name}',{_type},{chart_id},{chart_type},{index},'{unit}') 
							ON DUPLICATE KEY UPDATE 
							`name`='{name}',`type`={_type},`big_chart_id`={chart_id},`big_chart_type`={chart_type}, `unit`='{unit}'
							'''.format(address=deviceAddress,name=types[index][0],_type=types[index][2],chart_id=types[index][1][0],chart_type=types[index][1][1],index=index,unit=types[index][4])
				else:
					dataTypeInsert = '''INSERT INTO data_types (`device`,`name`,`type`,`big_chart_id`,`big_chart_type`,`index`) 
							VALUES ((SELECT `id` FROM `devices` WHERE `address`={address}),
							'{name}',{_type},{chart_id},{chart_type},{index}) 
							ON DUPLICATE KEY UPDATE 
							`name`='{name}',`type`={_type},`big_chart_id`={chart_id},`big_chart_type`={chart_type}
							'''.format(address=deviceAddress,name=types[index][0],_type=types[index][2],chart_id=types[index][1][0],chart_type=types[index][1][1],index=index)
				write_queue.put(dataTypeInsert)
			for i in range(len(types)):
				for index in range(len(types[i][3])):
					if types[i][2] != 0xFF and index == 0 :
						name = "min"
						value = str(types[i][3][index])
					elif types[i][2] != 0xFF and index == 1:
						name = "max"
						value = str(types[i][3][index])
					else:
						name = types[i][3][index]
						value = index
					dataTypeValuesInsert = '''INSERT INTO data_type_values (`data_type`,`name`,`value`,`index`) 
							VALUES ((SELECT `id` FROM data_types WHERE `device`=(SELECT `id` FROM `devices` WHERE `address`={address} AND `index`={i})),'{name}','{value}',{index})
							ON DUPLICATE KEY UPDATE
							`name`='{name}',`value`='{value}'
							'''.format(address=deviceAddress,name=name,value=value,i=i,index=index)
					write_queue.put(dataTypeValuesInsert)
			for index in range(len(config)):
				configInsert = '''INSERT INTO `configuration_parameters` (`configuration`,`name`,`type`,`index`) 
									VALUES ((SELECT `id` FROM `configuration` WHERE `device`=(SELECT `id` FROM `devices` WHERE `address`={address})),
									'{name}',{_type},{index}) 
									ON DUPLICATE KEY UPDATE `name`='{name}',`type`={_type}
									'''.format(address=deviceAddress,name=config[index][0],_type=config[index][1],index=index)
				write_queue.put(configInsert)
			for i in range(len(config)):
				for index in range(len(config[i][2])):
					if config[i][1] != 0xFF and index == 0 :
						name = "min"
						value = str(config[i][2][index])
					elif config[i][1] != 0xFF and index == 1:
						name = "max"
						value = str(config[i][2][index])
					else:
						name = config[i][2][index]
						value = index
					configValuesInsert = '''INSERT INTO configuration_parameter_values (`configuration_parameter`,`name`,`value`,`index`) 
							VALUES ((SELECT `id` FROM `configuration_parameters` WHERE `configuration`=(SELECT `id` FROM `configuration` WHERE `device`=(SELECT `id` FROM `devices` WHERE `address`={address} AND `index`={i}))),'{name}','{value}',{index})
							ON DUPLICATE KEY UPDATE
							`name`='{name}',`value`='{value}'
							'''.format(address=deviceAddress,name=name,value=value,i=i,index=index)
					write_queue.put(configValuesInsert)
			#update devices
			read_queue.put(1)
			readComplete_queue.get() #wait for update!!!
		except: # if anything goes wrong
			send_register_ack(usbDev,deviceAddress,RFM69_PACKET_TYPE_REGISTER_FAIL)
			return
	pass


#must be after handlers definitions
packetHandler = {
	RFM69_PACKET_TYPE_DATA : packetDataHandler,
	RFM69_PACKET_TYPE_REGISTER : packetRegisterHandler,
}


def main():
	mode = None
	usbDev = setupUsb()
	params_send = False
	paramsApplied = False
	
	print "Setting starting mode TX..."
	setMode(usbDev,MODE_TX)
	print "Setting starting mode RX..."
	setMode(usbDev,MODE_RX)
	
	while (not exitting):
		#wait for packet receive
		packetLength = 0
		try:
			print "WAITING FOR PACKET..."
			ret = usbDev.read(0x81, 2, 2000)
			if ret[0] != PACKET_RECEIVED:
				continue
			packetLength = ret[1]
			if packetLength == 0:
				continue
			print 'Packet received! length = ' + str(packetLength)
		except usb.core.USBError as error:
			if error.args[1] == 'Connection timed out':
				continue
			else:
				print type(error)
				print error.args
				print "line " + str(sys.exc_info()[-1].tb_lineno)
				os.kill(os.getpid(),signal.SIGINT)
				continue
		except IndexError as error:
			print ret
			os.kill(os.getpid(),signal.SIGINT)
		except Exception as error:
			print type(error)
			print error.args
			print "line " + str(sys.exc_info()[-1].tb_lineno)
			os.kill(os.getpid(),signal.SIGINT)
			continue
			
		#if no (timeout) exception
		#receive packet
		ret = usbDev.ctrl_transfer(USB_TRANSFER_DEVICE_TO_HOST, DOWNLOAD_PACKET, int(packetLength), 0, int(packetLength) - 1) # to check size (?)
		
		#check packet crc
		crc = packetLength # len field
		crc += sum(ret[0:-1])
		crc = crc % 256 # cast to uint8
		
		if crc != ret[-1]:
			print("Wrong crc!!! computed=" + str(crc) + " expected=" + str(ret[-1]) + " for packet " + str(ret))
			continue
		
		#prepare packet for handling
		packet = [None] * (packetLength - 5) # preamble and crc
		i = 0
		for x in range(3,packetLength - 2):
			packet[i] = ret[x]
			i = i + 1
		
		packetHandler[ret[2]](usbDev,ret[1],packet)
	print("Exitting...")


if __name__ == "__main__":
	signal.signal(signal.SIGINT, signal_handler)
	try:
		connected = False
		while(not connected and exitting == False):
			try:
				con_read = mysql.connector.connect(pool_name = 'db_pool', pool_size = 2, host = '127.0.0.1', user = 'root', password = 'madafaka', database = 'home_automation_petrykozy')
				con_write = mysql.connector.connect(pool_name = 'db_pool')
			except:
				print("Waiting for database...")
				time.sleep(1)
				continue
			connected = True
		
		cursor_read = con_read.cursor()
		cursor_write = con_write.cursor() 
		
		thread1 = threading.Thread( target = read_db )
		thread2 = threading.Thread( target = write_db )
		thread3 = threading.Thread( target = compute_average )
		
		thread1.start()
		thread2.start()
		thread3.start()
		
		main()
		
		thread1.join()
		thread2.join()
		thread3.join()
		
		cursor_read.close()
		cursor_write.close()
		con_read.close()
		con_write.close()
	except:
		print("FATAL! " + str(sys.exc_info()))
		print "line " + str(sys.exc_info()[-1].tb_lineno)
		traceback.print_exc(file=sys.stdout)
		os.kill(os.getpid(),signal.SIGINT)
