//16c0:06de myusb

#include <stdio.h>
#include <usb.h>
#include <termios.h>

#define LED_RED 0
#define LED_GREEN 1

struct usb{
        struct usb_bus *busses;
        struct usb_device *device;
        struct usb_device_descriptor descriptor;
        struct usb_dev_handle *dev_handle;
};

int myGetch(void){
        struct termios oldt, newt;
    int ch;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    ch = getchar_unlocked();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    return ch;
}

int findUSBDevice(int vid, int pid, struct usb * usbData){
        int isDeviceFound = 0;

        usb_find_busses();
        usb_find_devices();
        usbData->busses = usb_get_busses();

        while( usbData->busses != NULL ){
                //printf("bus: %s\n", usbData->busses->dirname);
                usbData->device = usbData->busses->devices;
                while( usbData->device != NULL ){
                        usbData->descriptor = usbData->device->descriptor;
                        //printf("descriptor: %x:%x\n",usbData->descriptor.idVendor,usbData->descriptor.idProduct);
                        if( (usbData->descriptor.idVendor == vid) && (usbData->descriptor.idProduct == pid) ){
                                isDeviceFound = 1;
                                goto find;
                        }
                        usbData->device = usbData->device->next;
                }
                usbData->busses = usbData->busses->next;
        }
        find:
        if(isDeviceFound == 0){
                printf("Device not found");
                return 1;
        }
        printf("device ok: %x:%x\n",usbData->descriptor.idVendor,usbData->descriptor.idProduct);
        usbData->dev_handle = usb_open(usbData->device);
        if(usbData->dev_handle == NULL){
                printf("ERROR OPENING DEVICE: %s",usb_strerror());
                return 1;
        }

        if( usb_set_configuration(usbData->dev_handle, 1) < 0 ){
                printf("ERROR SET CONFIG: %s",usb_strerror());
                return 1;
        }

        if(usb_claim_interface(usbData->dev_handle, 0) < 0){
                printf("ERROR CLAIM INTERFACE: %s",usb_strerror());
                return 1;
        }

        return 0;
}

int main(void){
        struct usb usbData;
        char c;
        char buffer[1];

        usb_init();
        if(findUSBDevice(0x16c0, 0x06de, &usbData) != 0){
		printf("Error opening device!");
		return 1;
	}

        for(;;){
                c = myGetch();
                switch(c){
                case '1':
                        usb_control_msg(usbData.dev_handle, 
                                USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, 
                                LED_RED, 0, 0, (char *)buffer, sizeof(buffer), 5000);
                        printf("green\n");
                        break;
                case '2':
                        usb_control_msg(usbData.dev_handle, 
                                USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, 
                                LED_GREEN, 0, 0, (char *)buffer, sizeof(buffer), 5000);
                        printf("red\n");
                        break;
                default:
                        goto brk;
                }
        }
        brk:

        usb_release_interface(usbData.dev_handle, 0); 
        usb_close(usbData.dev_handle); 

        return 0;
}
