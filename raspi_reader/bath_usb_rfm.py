#idVendor=0x16C0, idProduct=0x06DE
import usb.core
import usb.util
import mysql.connector
import time
import datetime
import signal
import Queue
import threading
import sys
import os
import traceback

#rfm69 modes
MODE_RX			= 0x00
MODE_TX			= 0x01

#for usb commands
USB_TRANSFER_HOST_TO_DEVICE = 0x40
USB_TRANSFER_DEVICE_TO_HOST = 0xC0

#usb commands
SET_MODE			= 0x00
CHECK_MODE_SET		= 0x01
CHECK_PACKET_RT		= 0x02 # check if packet was send(tx) or received(rx)
DOWNLOAD_PACKET		= 0x03 # download packet if there is any in rfm fifo
UPLOAD_PACKET		= 0x04 # upload packet to send
RESEND_PACKET		= 0x05 # resends last uploaded packet

#interrupt messages
PACKET_RECEIVED		= 0x01
MODE_SET			= 0x02
PACKET_SENT			= 0x03

#packet acks
RFM69_PACKET_TYPE_NO_PACKET		= 0x00 #indicate that no packet was received
RFM69_PACKET_TYPE_DATA			= 0x01
RFM69_PACKET_TYPE_CONFIG		= 0x02
RFM69_PACKET_TYPE_REGISTER		= 0x03
RFM69_PACKET_TYPE_REGISTER_ACK	= 0x04
RFM69_PACKET_TYPE_REGISTER_FAIL	= 0x05

#database
UPDATE_DEVICES = 0x01

exitting = False

params_queue = Queue.Queue()
params_applied_queue = Queue.Queue()

readComplete_queue = Queue.Queue()
read_queue = Queue.Queue()
write_queue = Queue.Queue()

devices = {} # dictionary {device address : dictionary {index:type}}

def signal_handler(signal, frame):
	global exitting
	print('user interrupt!')
	exitting = True

def setupUsb():
	usbDev = None
	while(usbDev == None and exitting == False):
		print("Waiting for device...")
		usbDev = usb.core.find(idVendor=0x16C0, idProduct=0x06DE)
		if(usbDev == None):
			time.sleep(1)
	try:
		usbDev.set_configuration()
	except Exception as error:
		print type(error)
		print error.args
		print "line " + str(sys.exc_info()[-1].tb_lineno)
		os.kill(os.getpid(),signal.SIGINT)
	print("Device Found!")
	return usbDev

#read database and prepare packet to send to mega8
def read_db():
	#selecting db for current data
	'''SELECT `devices`.`address`,`devices`.`name` AS `device_name`,`data_types`.`name` AS `type_name`,`data_types`.`index`,`data_current`.`date`,`data_current`.`value` FROM `devices`,`data_types` , `data_current` WHERE devices.id = data_types.device AND data_types.id = data_current.data_type ORDER BY `devices`.`address`,`data_types`.`index`'''
	'''SELECT `address`,`type`,`index` FROM `data_types`,`devices` WHERE `data_types`.`device`=`devices`.`id`'''
	'''
	 * packet[0]	// packet length
	 * packet[1]	// receiver address
	 * packet[2]	// packet type
	 * packet[3]	// new forced fan state
	 * packet[4]	// new fanStartDiffVal
	 * packet[5]	// new fanStopDiffVal
	 * packet[6]	// new force sensor threshold H
	 * packet[7]	// new force sensor threshold L
	 * packet[8]	// new toiletTimeSecondsOn (0-255)
	 * packet[9]	// new light sensor threshold H
	 * packet[10]	// new light sensor threshold L
	 * packet[11]	// crc (sum of all previous)
	'''
	query_select = "SELECT `address`,`type`,`index` FROM `data_types`,`devices` WHERE `data_types`.`device`=`devices`.`id`"
	cursor_read.execute(query_select)
	rows = cursor_read.fetchall()
	for row in rows:
		if not row[0] in devices:
			devices[row[0]] = {}
		devices[row[0]][row[2]] = row[1]
	
	query_select = "SELECT * FROM `bathroom_params` WHERE `id` = 1"
	cursor_read.execute(query_select)
	row = cursor_read.fetchone()
	oldrow = row
	if (row[1] == 0):
		print "packet to send:" + str(row[0]) + "," + str(row[1]) + "," + str(row[2]) + "," + str(row[3]) + "," + str(row[4]) + "," + str(row[5]) + "," + str(row[6])
		packet = [12,int(0x02),RFM69_PACKET_TYPE_CONFIG,1,int(row[2]),int(row[3]),int(row[4]),int(row[5])>>8,int(row[5])&int(0x00FF),int(row[6]),0,0,0]
		for i in range(0,10):
			packet[11] += packet[i]
		packet[11] = packet[11] % 256
		params_queue.put(packet)
	while( exitting == False ):
		if(read_queue.empty() == False):
			read_queue.get()
			cursor_read.execute(query_select)
			rows = cursor_read.fetchall()
			for row in rows:
				if not row[0] in devices:
					devices[row[0]] = {}
				devices[row[0]][row[2]] = row[1]
			print(devices)
			readComplete_queue.put(True);
		
		if(params_applied_queue.empty() == False):
			print("params applied!")
			params_applied_queue.get() # nothing to do with it for now
			query_update = "UPDATE `bathroom_params` SET `approved`='1', `user_fan_state`='"+str(row[2])+"', `fan_start_diff`='"+str(row[3])+"', `fan_stop_diff`='"+str(row[4])+"', `toilet_adc_threshold`='"+str(row[5])+"', `toilet_seconds_on`='"+str(row[6])+"' WHERE `bathroom_params`.`id` = "+str(row[0])
			print query_update
			cursor_read.execute(query_update)
			con_read.commit()
		
		con_read.commit()
		cursor_read.execute(query_select)
		row = cursor_read.fetchone()
		
		if (row[1] != 0 or row == oldrow):
			#print("nothing to send")
			time.sleep(1)
			continue
		
		print "packet to send (inside loop):" + str(row[0]) + "," + str(row[1]) + "," + str(row[2]) + "," + str(row[3]) + "," + str(row[4]) + "," + str(row[5]) + "," + str(row[6])
		packet = [12,int(0x02),RFM69_PACKET_TYPE_CONFIG,1,int(row[2]),int(row[3]),int(row[4]),int(row[5])>>8,int(row[5])&int(0x00FF),int(row[6]),0,0,0]
		for i in range(0,10):
			packet[11] += packet[i]
		packet[11] = packet[11] % 256
		oldrow = row
		params_queue.put(packet)
	print "read_db exitted!"

def write_db():
	current_minute_readings = 0
	current_minute_temperature_sum = None
	current_minute_humidity_sum = None
	current_minute_toilet = None
	current_minute_fan_state = None
	newMinute = True
	startTimeSeconds = int(round(time.time())) 
	while (exitting == False):
		if(write_queue.empty() == True):
			time.sleep(1)
			continue
		#print "Write db"
		data = write_queue.get()
		address = data[0]
		packet = data[1]
		if(address == 0): # plain sql
			cursor_write.execute(packet)
			con_write.commit()
		else:
			humidity = int(packet[2])
			temperature = int(packet[3])
			fan = int(packet[6])
			
			tmptoilet = 0
			tmptoilet = packet[4] << 8
			tmptoilet |= packet[5]
			toilet = int(tmptoilet)
			
			tmplight = 0
			tmplight = packet[7] << 8
			tmplight |= packet[8]
			light = int(tmplight)
			
			if(current_minute_readings != 0): #same for humidity
				current_minute_humidity_sum += humidity
				current_minute_temperature_sum += temperature
				current_minute_toilet = current_minute_toilet if current_minute_toilet > toilet else toilet
				current_minute_fan_state = current_minute_fan_state if current_minute_fan_state >= fan else fan
			else:
				current_minute_humidity_sum = humidity
				current_minute_temperature_sum = temperature
				current_minute_toilet = toilet
				current_minute_fan_state = fan
			
			if(newMinute == True):
				update_query = "UPDATE `home_automation_petrykozy`.`bathroom_data_current` SET `date` = CURRENT_TIMESTAMP, `humidity` = \'" + str(humidity) + "\', `temperature` = \'"+ str(temperature) +"\', `toilet` = \'"+ str(toilet) +"\', `fan_state` = \'"+ str(fan) +"\' WHERE `bathroom_data_current`.`id` = 0;"
				#print(update_query)
				cursor_write.execute(update_query)
				con_write.commit()
			
			current_minute_readings += 1
			currentTimeSeconds = int(round(time.time()))
			if( currentTimeSeconds - startTimeSeconds >= 60): # one minute
				humidity = str(int(round(current_minute_humidity_sum / current_minute_readings)))
				temperature = str(int(round(current_minute_temperature_sum / current_minute_readings)))
				toilet = str(int(round(current_minute_toilet)))
				fan = str(int(round(current_minute_fan_state)))
				
				write_query = "INSERT INTO `home_automation_petrykozy`.`bathroom_data_minutely` (`id`, `date`, `humidity`, `temperature`, `toilet`, `fan_state`) VALUES (NULL, DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:00'), \'"+ humidity +"\', \'"+ temperature +"\', \'"+ toilet +"\', \'"+ fan +"\');";
				#print ( str(datetime.datetime.now()) + " " + write_query )
				cursor_write.execute(write_query)
				con_write.commit()
				
				current_minute_readings = 1
				current_minute_humidity_sum = int(humidity)
				current_minute_temperature_sum = int(temperature)
				current_minute_toilet = int(toilet)
				current_minute_fan_state = int(fan)
				
				startTimeSeconds = currentTimeSeconds
				newMinute = True
	print "write_db exitted"

def setMode(device,newMode):
	while(not exitting):
		try:
			device.ctrl_transfer(USB_TRANSFER_HOST_TO_DEVICE, SET_MODE, newMode, 0, 1)
			ret = device.read(0x81, 2, 2000)
			#ret = device.ctrl_transfer(USB_TRANSFER_DEVICE_TO_HOST, CHECK_MODE_SET, 0, 0, 1)
			if (len(ret) == 2 and ret[0] == MODE_SET and ret[1] == newMode):
				print 'mode set ' + ''.join([str(x)+',' for x in ret])
				mode = newMode
				break
		except usb.core.USBError as error:
			if error.args[1] == 'Connection timed out':
				continue
			else:
				print type(error)
				print error.args
				print "line " + str(sys.exc_info()[-1].tb_lineno)
				os.kill(os.getpid(),signal.SIGINT)
		except Exception as error:
			print type(error)
			print error.args
			print "line " + str(sys.exc_info()[-1].tb_lineno)
			os.kill(os.getpid(),signal.SIGINT)

def sendPacket(usbDev,packet):
	setMode(usbDev,MODE_TX)
	print("MODE TX")
	length = usbDev.ctrl_transfer(USB_TRANSFER_HOST_TO_DEVICE, UPLOAD_PACKET, 0, 0, packet)
	assert length == len(packet)
	print "SENDING ACK..."
	try:
		ret = [0]
		while ret[0] != PACKET_SENT:
			ret = usbDev.read(0x81, 2, 1000)
			if ret[0] != PACKET_SENT:
				print("failed to send ack")
				return
		print 'ACK SENT'
	except usb.core.USBError as error:
		if error.args[1] == 'Connection timed out':
			print "Sending ACK timeout"
			pass
		else:
			print type(error)
			print error.args
			print "line " + str(sys.exc_info()[-1].tb_lineno)
			os.kill(os.getpid(),signal.SIGINT)
	except Exception as error:
		print type(error)
		print error.args
		print "line " + str(sys.exc_info()[-1].tb_lineno)
		os.kill(os.getpid(),signal.SIGINT)
	#set RX again
	setMode(usbDev,MODE_RX)
	print "MODE RX"
	''''''
	pass

paramsToSend = None
def packetDataHandler(usbDev,deviceAddress,deviceData):
	global paramsToSend
	print("data packet address:" + str(deviceAddress) +" " + str(deviceData))
	
	#save to database
	deviceDataParams = devices[deviceAddress]
	data = deviceData[2:]
	for key in sorted(deviceDataParams.keys()):
		typeSize = deviceDataParams[key]
		if(typeSize == 0xFF):
			typeSize = 1
		x = 0
		for j in range(typeSize): 
			x |= data.pop(0) << (8*(typeSize - j - 1))
		insertData = '''INSERT INTO `data_current`(`data_type`, `date`, `value`)
			VALUES ((SELECT `id` FROM `data_types` WHERE `device`= (SELECT `id` FROM `devices` WHERE `address`={address}) and `index`={index}),NOW(),{value})
			ON DUPLICATE KEY UPDATE `date` = NOW(), `value`={value}'''.format(address = deviceAddress, index=key,value = x)
		write_queue.put((0,insertData))
	#old write
	write_queue.put((deviceAddress,deviceData))
	
	#check if there are two packets with ack in row
	if(deviceData[4] != 0):
		paramsApplied = True
	else:
		paramsApplied = False
	
	#check if there is something to send
	params_send = False
	try:
		paramsToSend = params_queue.get_nowait()
		paramsApplied = False
		params_send = True
	except:
		pass
	
	#send ack
	if(paramsApplied == False and params_send == True):
		print("params to send" + str(paramsToSend))
		sendPacket(usbDev,paramsToSend)
		print("send " + str(len(paramsToSend)) + " bytes to address: " + str(paramsToSend[0]) + " params: " + str(paramsToSend))
	elif(paramsApplied == True and params_send == True):
		params_send = False
		params_applied_queue.put(paramsToSend[0])
		print("Parameters applied!")
	''''''
	pass

registeringDevice = []
def packetRegisterHandler(usbDev,deviceAddress,deviceData): #TODO multiple registration at once | wrong data reckognition (if not len) | wrong packet indexing
	global registeringDevice
	print("register packet address:" + str(deviceAddress) +" " + str(deviceData))
	#send ack
	paramsToSend = [4,deviceAddress,RFM69_PACKET_TYPE_REGISTER_ACK,0]
	crc = sum(paramsToSend[0:-1]) % 256
	paramsToSend[-1] = crc
	print("len= " + str(len(paramsToSend)) + " data=" + str(paramsToSend))
	sendPacket(usbDev,paramsToSend)
	
	registeringDevice += deviceData[2:]
	if(deviceData[1] == 0):
		#print("register data = " + str(registeringDevice))
		#parse device name
		n=registeringDevice.pop(0)
		deviceName = ""
		for i in range(n):
			deviceName+=chr(registeringDevice.pop(0))
		#print("deviceName = " + deviceName)
		#parse types
		typesData = registeringDevice[1:registeringDevice[0] + 1]
		#print("types data = " + str(typesData))
		registeringDevice = registeringDevice[registeringDevice[0] + 1:]
		configData = registeringDevice[1:registeringDevice[0] + 1]
		#print("config data = " + str(configData))
		registeringDevice = registeringDevice[registeringDevice[0] + 1:]
		#print("registering data = " + str(registeringDevice))
		if not len(registeringDevice) == 0:
			#send wrong register?
			pass
		types = []
		while not (len(typesData) == 0):
			#parse name
			n = typesData.pop(0)
			name = ""
			for i in range(n):
				name += chr(typesData.pop(0))
			#parse big chart
			bigChart = ()
			bigChart += (typesData.pop(0),)
			bigChart += (typesData.pop(0),)
			#parse type
			dataType = typesData.pop(0) # in bytes
			typeConfig = []
			if(dataType != 0xFF):
				# min / max vals
				for i in range (2): #always two values
					x = 0
					for j in range(dataType): 
						x |= typesData.pop(0) << (8*(dataType - j - 1));
					typeConfig.append(x)
				#unit
				unitLen = typesData.pop(0)
				unit = ""
				for i in range(unitLen):
					unit += unichr(typesData.pop(0))
				unit = unit.encode('utf-8')
				types.append((name,bigChart,dataType,typeConfig,unit))
			#enum type
			if(dataType == 0xFF):
				n = typesData.pop(0)
				#parse names for enum
				for i in range(n):
					m = typesData.pop(0)
					enumName = ""
					for i in range(m):
						enumName += chr(typesData.pop(0))
					typeConfig.append(enumName)
				types.append((name,bigChart,dataType,typeConfig))
		print(str(types))
		
		#parse config
		config = []
		while not (len(configData) == 0):
			#parse name
			n = configData.pop(0)
			name = ""
			for i in range(n):
				name += chr(configData.pop(0))
			#parse type
			configType = configData.pop(0) # in bytes
			typesConfig = []
			if(configType != 0xFF):
				for i in range (2): #always two values
					x = 0
					for j in range(configType): 
						x |= configData.pop(0) << (8 * (configType - j - 1));
					typesConfig.append(x)
			#enum type
			if(configType == 0xFF):
				n = configData.pop(0)
				#parse names for enum
				for i in range(n):
					m = configData.pop(0)
					enumName = ""
					for i in range(m):
						enumName += chr(configData.pop(0))
					typesConfig.append(enumName)
			config.append((name,configType,typesConfig))
		print(str(config))
		
		#save to db
		deviceInsert = "INSERT INTO `devices` (address,name) VALUES ({address},'{name}') ON DUPLICATE KEY UPDATE name='{name}'".format(address=deviceAddress,name=deviceName)
		write_queue.put((0,deviceInsert))
		configurationInsert = "INSERT IGNORE INTO `configuration` (device,applied) VALUES ((SELECT `id` FROM `devices` WHERE `address`={address}),1)".format(address=deviceAddress)
		write_queue.put((0,configurationInsert))
		for index in range(len(types)):
			if(types[index][2] != 0xFF):
				dataTypeInsert = '''INSERT INTO data_types (`device`,`name`,`type`,`big_chart_id`,`big_chart_type`,`index`,`unit`) 
						VALUES ((SELECT `id` FROM `devices` WHERE `address`={address}),
						'{name}',{_type},{chart_id},{chart_type},{index},'{unit}') 
						ON DUPLICATE KEY UPDATE 
						`name`='{name}',`type`={_type},`big_chart_id`={chart_id},`big_chart_type`={chart_type}, `unit`='{unit}'
						'''.format(address=deviceAddress,name=types[index][0],_type=types[index][2],chart_id=types[index][1][0],chart_type=types[index][1][1],index=index,unit=types[index][4])
			else:
				dataTypeInsert = '''INSERT INTO data_types (`device`,`name`,`type`,`big_chart_id`,`big_chart_type`,`index`) 
						VALUES ((SELECT `id` FROM `devices` WHERE `address`={address}),
						'{name}',{_type},{chart_id},{chart_type},{index}) 
						ON DUPLICATE KEY UPDATE 
						`name`='{name}',`type`={_type},`big_chart_id`={chart_id},`big_chart_type`={chart_type}
						'''.format(address=deviceAddress,name=types[index][0],_type=types[index][2],chart_id=types[index][1][0],chart_type=types[index][1][1],index=index)
			write_queue.put((0,dataTypeInsert))
		for i in range(len(types)):
			for index in range(len(types[i][3])):
				if types[i][2] != 0xFF and index == 0 :
					name = "min"
					value = str(types[i][3][index])
				elif types[i][2] != 0xFF and index == 1:
					name = "max"
					value = str(types[i][3][index])
				else:
					name = types[i][3][index]
					value = index
				dataTypeValuesInsert = '''INSERT INTO data_type_values (`data_type`,`name`,`value`,`index`) 
						VALUES ((SELECT `id` FROM data_types WHERE `device`=(SELECT `id` FROM `devices` WHERE `address`={address} AND `index`={i})),'{name}','{value}',{index})
						ON DUPLICATE KEY UPDATE
						`name`='{name}',`value`='{value}'
						'''.format(address=deviceAddress,name=name,value=value,i=i,index=index)
				write_queue.put((0,dataTypeValuesInsert))
		for index in range(len(config)):
			configInsert = '''INSERT INTO `configuration_parameters` (`configuration`,`name`,`type`,`index`) 
								VALUES ((SELECT `id` FROM `configuration` WHERE `device`=(SELECT `id` FROM `devices` WHERE `address`={address})),
								'{name}',{_type},{index}) 
								ON DUPLICATE KEY UPDATE `name`='{name}',`type`={_type}
								'''.format(address=deviceAddress,name=config[index][0],_type=config[index][1],index=index)
			write_queue.put((0,configInsert))
		for i in range(len(config)):
			for index in range(len(config[i][2])):
				if config[i][1] != 0xFF and index == 0 :
					name = "min"
					value = str(config[i][2][index])
				elif config[i][1] != 0xFF and index == 1:
					name = "max"
					value = str(config[i][2][index])
				else:
					name = config[i][2][index]
					value = index
				configValuesInsert = '''INSERT INTO configuration_parameter_values (`configuration_parameter`,`name`,`value`,`index`) 
						VALUES ((SELECT `id` FROM `configuration_parameters` WHERE `configuration`=(SELECT `id` FROM `configuration` WHERE `device`=(SELECT `id` FROM `devices` WHERE `address`={address} AND `index`={i}))),'{name}','{value}',{index})
						ON DUPLICATE KEY UPDATE
						`name`='{name}',`value`='{value}'
						'''.format(address=deviceAddress,name=name,value=value,i=i,index=index)
				write_queue.put((0,configValuesInsert))
		#update devices
		read_queue.put(1)
		readComplete_queue.get() #wait for update!!!
	''''''
	pass

#must be after handlers definitions
packetHandler = {
	RFM69_PACKET_TYPE_DATA : packetDataHandler,
	RFM69_PACKET_TYPE_REGISTER : packetRegisterHandler,
}

def main():
	global registeringDevice
	
	mode = None
	usbDev = setupUsb()
	params_send = False
	paramsApplied = False
	
	print "Setting starting mode TX..."
	setMode(usbDev,MODE_TX)
	print "Setting starting mode RX..."
	setMode(usbDev,MODE_RX)
	
	while (not exitting):
		#wait for packet receive
		packetLength = 0
		try:
			print "WAITING FOR PACKET..."
			ret = usbDev.read(0x81, 2, 2000)
			if ret[0] != PACKET_RECEIVED:
				continue
			packetLength = ret[1]
			if packetLength == 0:
				continue
			print 'Packet received! length = ' + str(packetLength)
		except usb.core.USBError as error:
			if error.args[1] == 'Connection timed out':
				continue
			else:
				print type(error)
				print error.args
				print "line " + str(sys.exc_info()[-1].tb_lineno)
				os.kill(os.getpid(),signal.SIGINT)
				continue
		except IndexError as error:
			print ret
			os.kill(os.getpid(),signal.SIGINT)
		except Exception as error:
			print type(error)
			print error.args
			print "line " + str(sys.exc_info()[-1].tb_lineno)
			os.kill(os.getpid(),signal.SIGINT)
			continue
			
		#if no (timeout) exception
		#receive packet
		ret = usbDev.ctrl_transfer(USB_TRANSFER_DEVICE_TO_HOST, DOWNLOAD_PACKET, int(packetLength), 0, int(packetLength) - 1) # to check size (?)
		
		#check packet crc
		crc = packetLength # len field
		crc += sum(ret[0:-1])
		crc = crc % 256 # cast to uint8
		
		if crc != ret[-1]:
			print("Wrong crc!!! computed=" + str(crc) + " expected=" + str(ret[-1]) + " registering device reset!")
			#print( str(datetime.datetime.now()) + ' '.join([str(chr(x)) for x in ret]) )
			print( str(datetime.datetime.now()) + ' ' + str(ret))
			registeringDevice = []
			continue
		
		#prepare packet for handling
		packet = [None] * (packetLength - 5) # preamble and crc
		i = 0
		for x in range(3,packetLength - 2):
			packet[i] = ret[x]
			i = i + 1
		
		packetHandler[ret[2]](usbDev,ret[1],packet)
	print("Exitting...")

if __name__ == "__main__":
	signal.signal(signal.SIGINT, signal_handler)
	try:
		connected = False
		while(not connected and exitting == False):
			try:
				con_read = mysql.connector.connect(pool_name = 'db_pool', pool_size = 2, host = '127.0.0.1', user = 'root', password = 'madafaka', database = 'home_automation_petrykozy')
				con_write = mysql.connector.connect(pool_name = 'db_pool')
			except:
				print("Waiting for database...")
				time.sleep(1)
				continue
			connected = True
		
		cursor_read = con_read.cursor()
		cursor_write = con_write.cursor() 
		
		thread1 = threading.Thread( target = read_db )
		thread2 = threading.Thread( target = write_db )
		thread1.start()
		thread2.start()
		
		main()
		
		thread1.join()
		thread2.join()
		
		cursor_read.close()
		cursor_write.close()
		con_read.close()
		con_write.close()
	except:
		#exitting = 0
		print("FATAL! " + str(sys.exc_info()))
		print "line " + str(sys.exc_info()[-1].tb_lineno)
		traceback.print_exc(file=sys.stdout)
		os.kill(os.getpid(),signal.SIGINT)
