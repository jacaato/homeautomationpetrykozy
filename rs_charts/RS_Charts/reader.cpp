#include "reader.h"

Reader::Reader(QObject *parent) :
    QObject(parent)
{
    end = false;
	mean = -100;
	currentTime = QDateTime::currentDateTime();

    openDevice();
}

Reader::~Reader()
{
    if(device){
        device->close();
        delete device;
    }
}

void Reader::startRead(){
    read();
}

void Reader::read(){
	QString strData,name;
	QByteArray data;
	QStringList strList;
	int type=0;
	QDateTime newTime;
	QSqlQuery query;
	
	//int failures = 0;
	int id = 0;
	while(!end){
		data.clear();
		device->waitForReadyRead(1000);
		if(device->canReadLine()){
			data = device->readLine();
			strData = data.data();
			strList = strData.split("=");
			if(strList.size() != 2)
				continue;
			type = strList[0] == "water" ? Drawer::WATER : Drawer::HEATER;
			qreal newTemp = strList[1].toDouble();
			qDebug() << "temperature read!" << newTemp;
			emit sendFastData( newTemp, type, id++ );
			calcMean(newTemp);
			qDebug() << "new mean = " << mean;
			
			newTime = QDateTime::currentDateTime();
			if(currentTime.secsTo(newTime) >= SEND_TIME){
				currentTime = QDateTime::currentDateTime();
				name = strList[0]; // must be "water" or "heater"
				query.exec("INSERT INTO temperature (device,time,value) VALUES ((SELECT id FROM device WHERE name='"+name+"'),"+QString::number(currentTime.toMSecsSinceEpoch())+","+QString::number(mean)+")");
				qDebug() << query.lastQuery();
				emit sendData( mean, type );
			}
		}else{
			//failures++;
			//if(failures > 5){
			//    openDevice();
			//    failures = 0;
			//}
			//qDebug() << "cant read file";
		}
	}
}

void Reader::calcMean(qreal newData){
	if(mean == -100)
		mean = newData;
	else
		mean = (mean*MEAN_WEIGHT + newData)/(MEAN_WEIGHT + 1);
}

void Reader::destroy(){
	end = true;
}

void Reader::openDevice(){
	// if(device != 0 ){
	//    device->deleteLater();
	// }

	device = new QSerialPort(this);
    device->setPortName("/dev/ttyAMA0");

	while(!device->open(QIODevice::ReadOnly)){
		qDebug() << "cant open device!";
		QThread::sleep(1);
		qDebug() << "trying again...";
		
	}

	device->setBaudRate(QSerialPort::Baud9600);
	device->setDataBits(QSerialPort::Data8);
	device->setParity(QSerialPort::NoParity);
	device->setStopBits(QSerialPort::OneStop);
	device->setFlowControl(QSerialPort::NoFlowControl);
}
