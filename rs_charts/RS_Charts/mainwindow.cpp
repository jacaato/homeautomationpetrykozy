#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	createDatabaseConnection("temp.db");

	//reader = new Reader();
	drawer = new Drawer(ui->customPlot,ui->calendar,ui->horizontalSliderAlpha,ui->horizontalSliderBeta,this);
	server = new Server(ui->customPlot);

	QObject::connect(ui->pushButton,SIGNAL(clicked()),drawer,SLOT(refresh()));
	//QObject::connect(reader,SIGNAL(sendData(qreal,int)),drawer,SLOT(getData(qreal,int)));
	//QObject::connect(reader,SIGNAL(sendFastData(qreal,int, int)),server,SLOT(getData(qreal,int,int)));
	//QObject::connect(this,SIGNAL(startRead()),reader,SLOT(startRead()));
	//QObject::connect(this,SIGNAL(destroy()),reader,SLOT(destroy()));
	QObject::connect(ui->calendar,SIGNAL(activated(QDate)),drawer,SLOT(changeTo(QDate)));
	QObject::connect(this,SIGNAL(startServer()),server,SLOT(startServer()));
	QObject::connect(ui->horizontalSliderAlpha,SIGNAL(sliderReleased()),drawer,SLOT(sliderChanged()));
	QObject::connect(ui->horizontalSliderBeta,SIGNAL(sliderReleased()),drawer,SLOT(sliderChanged()));
	
	//reader->moveToThread(&readerThread);
	//readerThread.start();

	//server->moveToThread(&serverThread);
	//serverThread.start();

	//emit startRead();
	emit startServer();
}

void MainWindow::createDatabaseConnection(QString name){
	db = QSqlDatabase::addDatabase("QSQLITE");
	db.setDatabaseName(name);
	if(!db.open()){
		qDebug() << "error opening database!";
		exit(EXIT_FAILURE);
	}
	
	QSqlQuery query;
	query.exec("CREATE TABLE IF NOT EXISTS device "
			   "(id INTEGER NOT NULL PRIMARY KEY ASC AUTOINCREMENT,"
			   "name VARCHAR(20) NOT NULL UNIQUE ON CONFLICT IGNORE)");
	query.exec("CREATE TABLE IF NOT EXISTS temperature"
			   "(id INTEGER NOT NULL PRIMARY KEY ASC AUTOINCREMENT,"
			   "device INTEGER NOT NULL CONSTRAINT device_type REFERENCES device(id),"
			   "time INTEGER NOT NULL,"
			   "value REAL NOT NULL)");
	
	query.exec("INSERT OR ABORT INTO device (name) VALUES ('water')");
	query.exec("INSERT OR ABORT INTO device (name) VALUES ('heater')");
	
}

MainWindow::~MainWindow()
{
	emit destroy();
	db.close();
	delete ui;
}
