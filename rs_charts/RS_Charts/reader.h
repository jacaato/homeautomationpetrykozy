#ifndef READER_H
#define READER_H

#define SEND_TIME 10
#define MEAN_WEIGHT 60

#include <QObject>
#include <QDateTime>
#include <QDebug>
#include <QThread>
#include <QSqlQuery>
#include <QSerialPort>

#include "drawer.h"

class Reader : public QObject
{
    Q_OBJECT

public:
    explicit Reader(QObject *parent = 0);
    ~Reader();

private:
    QDateTime currentTime;
    QSerialPort *device;
    bool end;
	qreal mean;

    void read();
    void calcMean(qreal newData);
    void openDevice();

public slots:
    void startRead();
    void destroy();

signals:
    void sendData(qreal,int);
    void sendFastData(qreal,int,int);
};

#endif // READER_H
