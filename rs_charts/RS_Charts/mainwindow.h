#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlQuery>

#include "reader.h"
#include "drawer.h"
#include "server.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    Reader *reader;
    Drawer *drawer;
    Server *server;
    QThread readerThread,serverThread;
    QSqlDatabase db;

    void drawPlot();
    double read();
    void createDatabaseConnection(QString name);

signals:
    void startRead();
    void startServer();
    void destroy();
};

#endif // MAINWINDOW_H
