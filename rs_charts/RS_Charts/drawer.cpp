#include "drawer.h"

Drawer::Drawer(QCustomPlot *plot, QCalendarWidget *calendar, QSlider *sliderAlpha, QSlider *sliderBeta, QObject *parent):
	QObject(parent),
	plot(plot),
	calendar(calendar),
	sliderAlpha(sliderAlpha),
	sliderBeta(sliderBeta)
{
	currentDate = QDate::currentDate();
	activatedDate = QDate::currentDate();
	createPlot();
	changeTo(QDate::currentDate());

	QTimer *timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(refresh()));
	timer->start(60*1000);
}

Drawer::~Drawer()
{
}

void Drawer::refresh(){
	if(currentDate != activatedDate)
		return;
	
	//date changed!
	if(currentDate != QDate::currentDate()){
		currentDate = QDate::currentDate();
		calendar->setSelectedDate(currentDate);
		plot->graph(0)->clearData();
		plot->graph(1)->clearData();
		for(int i = 0 ; i < DEVICES_SIZE ; i++)
			data[i].clear();
	}
	
	
	//if(data.size()==0){
		changeTo(currentDate);
	//}
	//else{ //destroys smoothing 
	//	QDateTime end = QDateTime(currentDate).addDays(1);
	//	readDB(data.last().time+1, end.toMSecsSinceEpoch());
	//	setPlotDataFromList();
	//}
	
	
}

void Drawer::changeTo(QDate activatedDate){
	this->activatedDate = activatedDate;
	QDateTime start = QDateTime(activatedDate);
	QDateTime end = QDateTime(start).addDays(1);
	for(int i = 0 ; i < DEVICES_SIZE ; i++)
		data[i].clear();
	readDB(start.toMSecsSinceEpoch(), end.toMSecsSinceEpoch()); // current day
	holtsSmoothing(sliderAlpha->value()/1000.0,sliderBeta->value()/1000.0);
	setPlotDataFromList();
	drawToday = (QDate::currentDate() == activatedDate);
}

void Drawer::sliderChanged()
{
	changeTo(activatedDate);
}

void Drawer::readDB(qint64 start, qint64 end){
	QSqlQuery query;
	// NO LONGER CLEARING DATA!!
	//data.clear();

	query.exec("SELECT device,time,value FROM temperature WHERE time BETWEEN "+QString::number(start)+" AND "+QString::number(end));
	qDebug() << query.lastQuery();
	while(query.next()){
		DailyData d;
		d.device = (DrawType)query.value(0).toInt();
		d.time = query.value(1).toLongLong();
		d.value = query.value(2).toDouble();
		data[d.device - 1].append(d);
		//qDebug() << d.device << d.time << d.value;
	}
	for(int i = 0 ; i < DEVICES_SIZE ; i++)
		qSort(data[i].begin(),data[i].end(),before);
}

void Drawer::holtsSmoothing(qreal alpha, qreal beta){
	//empirical <0,1>
	//qreal beta = 0.05; // odporność na zmiany // mniej = bardziej
	//qreal alpha = 0.05; //stopień wygładzenia // mniej = bardziej
	
	qint64 start = QDateTime::currentMSecsSinceEpoch();
	
	for(int i = 0 ; i < DEVICES_SIZE ; i++){
		if(data[i].size() < 2)
			continue;
		
		qreal b = data[i][1].value - data[i][0].value;
		
		for(int j = 1 ; j < data[i].size() ; j++){
			//qDebug()<<"old "<< data[i].value;
			data[i][j].value = alpha*data[i][j].value + (1-alpha)*(data[i][j-1].value+b);
			//qDebug()<<"new "<< data[i].value;
			b = beta*(data[i][j].value - data[i][j-1].value) + (1- beta)*b;
		}
	}
	qDebug() << "smoothingTime = " << QDateTime::currentMSecsSinceEpoch() - start << "ms elements = " << data[0].size() + data[1].size();
}

void Drawer::setPlotDataFromList(){
	QDateTime date;
	QTime time;
	qreal hour, minutes, seconds;

	plot->graph(0)->clearData();
	plot->graph(1)->clearData();
	
	for(int i = 0 ; i < DEVICES_SIZE ; i++){
		foreach (DailyData d, data[i]) {
			date = QDateTime::fromMSecsSinceEpoch(d.time);
			time = date.time();
			hour = time.hour();
			minutes = time.minute();
			seconds = time.second();
			
			// magic -1
			plot->graph(d.device-1)->addData(hour + minutes/60 + seconds/3600,d.value);
		}
	}
	
	plot->replot();
}

/*void Drawer::getData(qreal data, int type){
	if(!drawToday)
		return;

	//day change!
	if(currentDate != QDate::currentDate()){
		currentDate = QDate::currentDate();
		calendar->setSelectedDate(currentDate);
		plot->graph(0)->clearData();
		plot->graph(1)->clearData();
	}

	QTime currentTime = QTime::currentTime();
	qreal hour = currentTime.hour();
	qreal minutes = currentTime.minute();
	qreal seconds = currentTime.second();

	plot->graph(type)->addData(hour + minutes/60 + seconds/3600,data);

	plot->replot();
}*/

void Drawer::createPlot(){
	plot->addGraph(); // graph(0) - woda
	plot->addGraph(); // graph(1) - piec
	plot->addGraph(); // graph(2) - woda prognoza
	plot->addGraph(); // graph(3) - piec prognoza

	plot->graph(WATER)->setPen(QPen(Qt::blue));
	plot->graph(HEATER)->setPen(QPen(Qt::red));

	plot->xAxis->setLabel("Hour");
	plot->xAxis->setRange(0, 24);
	plot->xAxis->setAutoTickCount(22);
	QPen pen;
	pen.setStyle(Qt::SolidLine);
	pen.setColor(QColor::fromRgb(qRgb(170,170,170)));
	plot->xAxis->grid()->setPen(pen);
	plot->xAxis->setAutoSubTicks(false);
	plot->xAxis->setSubTickCount(3);
	plot->xAxis->grid()->setSubGridVisible(true);

	plot->yAxis->setLabel("Temp");
	plot->yAxis->setRange(0, 130);
	plot->yAxis->setAutoTickCount(12);
	plot->yAxis->grid()->setPen(pen);
	plot->yAxis->grid()->setSubGridVisible(true);

	plot->replot();
}
