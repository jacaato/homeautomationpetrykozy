#ifndef DRAWER_H
#define DRAWER_H

#include <QObject>
#include <QSqlQuery>
#include <QtAlgorithms>

#include "qcustomplot.h"

#define DEVICES_SIZE 2

class Drawer : public QObject
{
	Q_OBJECT

public:
	enum DrawType{
		WATER = 0,
		HEATER = 1
	};

	struct DailyData{
		enum Drawer::DrawType device;
		qint64 time;
		qreal value;
	};
	
	static bool before(const DailyData &s1, const DailyData &s2)
	{
		return s1.time < s2.time;
	}
	
	explicit Drawer(QCustomPlot *plot, QCalendarWidget* calendar,QSlider* sliderAlpha, QSlider* sliderBeta, QObject *parent = 0);
	~Drawer();

private:
	QCustomPlot *plot;
	QCalendarWidget *calendar;
	QSlider* sliderAlpha, *sliderBeta;
	QVector<DailyData> data[DEVICES_SIZE];
	QDate currentDate;
	QDate activatedDate;
	bool drawToday;

	void createPlot();
	void readDB(qint64 start, qint64 end);
	void setPlotDataFromList();
	void holtsSmoothing(qreal alpha, qreal beta);

public slots:
	void refresh();
	//void getData(qreal data, int type);
	void changeTo(QDate activatedDate);
	void sliderChanged();
};
#endif // DRAWER_H
