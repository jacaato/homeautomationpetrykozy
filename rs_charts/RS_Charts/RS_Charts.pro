#-------------------------------------------------
#
# Project created by QtCreator 2014-04-16T15:58:28
#
#-------------------------------------------------

QT += core gui printsupport serialport sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RS_Charts
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    reader.cpp \
    drawer.cpp \
    server.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    reader.h \
    drawer.h \
    server.h

FORMS    += mainwindow.ui

target.path = /home/pi/Projekty/RS_Charts
INSTALLS += target

QMAKE_CXXFLAGS += -Wno-psabi

