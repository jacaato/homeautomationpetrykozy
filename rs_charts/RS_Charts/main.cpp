#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	//QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf-8"));
	//QTextCodec::setCodecForTr(QTextCodec::codecForName("utf-8"));
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("utf-8"));
	
	QApplication a(argc, argv);
	MainWindow w;
	w.show();
	
	
	return a.exec();
}
