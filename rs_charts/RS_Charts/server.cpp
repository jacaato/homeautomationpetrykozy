#include "server.h"

Server::Server(QCustomPlot *plot, QObject *parent) :
	QObject(parent),
	plot(plot)
{
    currentTemperatureHeater = 0;
    currentTemperatureWater = 0;
}

void Server::getData(qreal temp,int type, int id){
    currentID = id;
    if(type == Drawer::WATER){
        currentTemperatureWater = temp;
    }else if(type == Drawer::HEATER){
        currentTemperatureHeater = temp;
    }
}

void Server::acceptConnection(){
	if(clients.size() < MAX_CONNECTIONS){
		QTcpSocket* client = server.nextPendingConnection();
		
		qDebug() << "client accepted!" << client;
		
		QObject::connect(client, SIGNAL(readyRead()), this, SLOT(clientReadyRead()));
		QObject::connect(client, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
		
		clients.append(client);
	}
	else{
		qDebug() << "new client rejected!";
	}
}

void Server::clientDisconnected()
{
	QTcpSocket* client;
	client = static_cast<QTcpSocket*>(sender());
	
	qDebug() << "client disconnected!" << client;
	
	client->disconnect(this);
	
	clients.removeOne(client);
	
	client->deleteLater();
}

void Server::clientReadyRead()
{
	QString fname = "curr_plot.jpg"; // name of plot image
	QString err = "could not create widget image";
	
	QTcpSocket* client;
	client = static_cast<QTcpSocket*>(sender());
	
	QByteArray clientData = client->readAll();
	QDataStream in(&clientData,QIODevice::ReadOnly);
	unsigned char request;
	in >> request;
	
	QString allRequest = clientData.data();
	
	//qDebug() << clientData.data();
	
	if(request == GET_TEMP){
		qDebug() << "android client data " << client;
		
		QByteArray packet;
		QDataStream out(&packet,QIODevice::WriteOnly);
		out << currentTemperatureWater << currentID;
		
		qDebug() << "write packet for client " << currentTemperatureWater;
		client->write(packet);
	}
	else if (allRequest.indexOf("GET / HTTP/1.1") != -1) {
		qDebug() << "HTTP open " << client;
		QTextStream os(client);
		os.setAutoDetectUnicode(true);
		os << "HTTP/1.0 200 OK\r\n"
			  "Content-Type: text/html; charset=\"utf-8\"\r\n"
			  "\r\n"
			  
			  "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">"
			  //"<script>setTimeout(function(){location.reload();}, 1000);</script>
			  "</head>"
			  "<body><h1>Temperatura = "
		   << currentTemperatureWater << "°C</h1></br><h2>"<<currentID<<"</h2>"
			  "<img src=\""<< fname << "\" alt=\"" << err <<"\"></body></html>\n";
		
		//client->waitForBytesWritten(1000); // bugged!
		while(client->bytesToWrite() != 0);
		client->close();
		qDebug() << "HTTP close " << client;
	}
	else if(allRequest.indexOf(fname) != -1){
		qDebug() << "JPG open " << client;
		if(plot->saveJpg(fname,800,600)){
		}
		QFile img(fname);
		if(!img.open(QIODevice::ReadOnly)){
			qDebug() << "cant open img file!";
		}
		
		QTextStream os(client);
		
		os.setAutoDetectUnicode(true);
		os << "HTTP/1.0 200 OK\r\n "
			  "Content-Type: image/jpeg \r\n"
			  "\r\n";
		
		client->flush();
		while(client->bytesToWrite() != 0);

		client->write(img.readAll(),img.size());
		client->flush();
		//while(client->bytesToWrite() != 0);
		client->close();
		img.close();
		qDebug() << "JPG close " << client;
	}
}

void Server::startServer(){
     QObject::connect(&server,SIGNAL(newConnection()),this,SLOT(acceptConnection()));
     server.listen(QHostAddress::Any,PORT);
}

Server::~Server()
{
    foreach (QTcpSocket* client, clients) {
        client->deleteLater();
    }
    clients.clear();
}
