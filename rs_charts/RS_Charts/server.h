#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QSqlQuery>
#include <QTcpServer>
#include <QTcpSocket>
#include <QPixmap>
#include <QDataStream>

#include "qcustomplot.h"
#include "drawer.h"

#define PORT 65222

#define GET_TEMP 1
#define MAX_CONNECTIONS 10

class Server : public QObject
{
	Q_OBJECT
	
public:
	explicit Server(QCustomPlot* plot,QObject *parent = 0);
	~Server();
public slots:
	void getData(qreal temp,int type, int id);
	void acceptConnection();
	void clientDisconnected();
	void clientReadyRead();
	void startServer();
private:
	QTcpServer server;
	QList<QTcpSocket*> clients;
	
	QCustomPlot* plot;
	qreal currentTemperatureHeater;
	qreal currentTemperatureWater;
	int currentID;
};

#endif // SERVER_H
