#ifndef OWIRE_H
#define OWIRE_H

//not prepared for parasite mode

#ifndef F_CPU
	#define F_CPU 8000000
#endif

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

// uc configuration
#define OWIRE_DDR	DDRB
#define OWIRE_PORT	PORTB
#define OWIRE_PINX	PINB
#define OWIRE_PIN	6

// rom commands
#define OWIRE_SEARCH_ROM	0xF0
#define OWIRE_READ_ROM		0x33
#define OWIRE_MATCH_ROM		0x55
#define OWIRE_SKIP_ROM		0xCC
#define OWIRE_ALARM_SEARCH	0xEC 

#define OWIRE_SET_ZERO_OUT	{	OWIRE_DDR |= _BV(OWIRE_PIN); \
								OWIRE_PORT &= ~_BV(OWIRE_PIN); }
#define OWIRE_SET_ONE_OUT	{	OWIRE_DDR |= _BV(OWIRE_PIN); \
								OWIRE_PORT |= _BV(OWIRE_PIN); }
#define OWIRE_SET_IN		{	OWIRE_DDR &= ~_BV(OWIRE_PIN); \
								OWIRE_PORT |= _BV(OWIRE_PIN); }

uint8_t OWIRE_readBit();
uint8_t OWIRE_readByte();
void OWIRE_writeBit(uint8_t bit);
void OWIRE_writeByte(uint8_t byte);
//0 ok
uint8_t OWIRE_init();
void OWIRE_writeCommand(uint8_t command);
void OWIRE_writeRom(uint64_t rom);
uint8_t OWIRE_readRom(uint64_t* rom);
uint8_t OWIRE_checkRomCRC(uint64_t rom);
void OWIRE_findRoms(uint64_t* roms,uint8_t length);
//0 ok
uint8_t OWIRE_checkCRC(uint8_t crc, uint8_t* data, uint8_t dataLength);

#endif