#ifndef DS18B20_H
#define DS18B20_H

#ifndef F_CPU
	#define F_CPU 8000000
#endif

#include "1-wire.h"

// ds18b20 commands
// function commands
#define DS18B20_CONVERT_T			0x44
#define DS18B20_WRITE_SCRATCHPAD	0x4E
#define DS18B20_READ_SCRATCHPAD		0xBE
#define DS18B20_COPY_SCRATCHPAD		0x48
#define DS18B20_RECALL_E2			0xB8
#define DS18B20_READ_POWER_SUPPLY	0xB4

//resolutions
#define DS18B20_RES_9BIT	0x1F
#define DS18B20_RES_10BIT	0x3F
#define DS18B20_RES_11BIT	0x5F
#define DS18B20_RES_12BIT	0x7F

#define DS18B20_REAL_MUL	625

//struct to read scratchpad
struct DS18B20_Scratchpad{
	uint8_t tempLSB;
	uint8_t tempMSB;
	uint8_t tH;
	uint8_t tL;
	uint8_t config;
	uint8_t reserved1; // 0xFF
	uint8_t reserved2;
	uint8_t reserved3; // 0x10
	uint8_t crc;
};

struct DS18B20_Temperature{
	int8_t tempInt;
	uint16_t tempReal;
};

//0 ok
uint8_t DS18B20_readScratchpad(struct DS18B20_Scratchpad* scratchpad, uint64_t rom);
void DS18B20_writeScratchpad(uint8_t th, uint8_t tl, uint8_t resolution, uint64_t rom);
// celsius degrees. 0 ok
uint8_t DS18B20_readTemperature(struct DS18B20_Temperature* temperature, uint64_t rom);

#endif