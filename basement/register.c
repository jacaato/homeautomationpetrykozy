#include <avr/wdt.h>
#include "register.h"
#include "project_config.h"
#include "rfm69hw.h"


uint8_t currentDataIndex; // current data index in wholePacketLength
uint8_t packetsLeft; // no of packets to send
uint8_t packetNumber;
uint8_t wholePacketLength; // without preamble (len,address,thisAddress,packetType,packetsLeft)
uint8_t packet[RFM69_FIFO_SIZE];


void sendHeader()
{
	//1. indicate length
	// without length field itself
	uint8_t packetLength;
	if((RFM69_FIFO_SIZE - PREAMBLE_SIZE) > wholePacketLength - currentDataIndex)
	{
		packetLength = wholePacketLength - currentDataIndex + PREAMBLE_SIZE + 1;
	}
	else
	{
		packetLength = RFM69_FIFO_SIZE;
	}
	packet[0] = packetLength;
	//2. send this on address
	packet[1] = RECEIVER_ADDRESS;
	//3. send address
	packet[2] = DEVICE_ADDRESS;
	//4. send packet type
	packet[3] = RFM69_PACKET_TYPE_REGISTER;
	//5. send packets index
	packet[4] = packetNumber;
	//6. send left
	packet[5] = packetsLeft;
}


void sendPart()
{
	uint8_t currentArray;
	uint8_t currentIter;
	uint8_t sentLen = 0;
	uint8_t i = 0;
	
	// last is crc
	if(currentDataIndex >= DEVICE_NAME_LEN + 1 + DATA_CONFIG_LENGTH + 1 + 1)
	{
		currentArray = 2;
		currentIter = currentDataIndex - DEVICE_NAME_LEN - 1 - DATA_CONFIG_LENGTH - 1 - 1;
	}
	else if(currentDataIndex >= DEVICE_NAME_LEN + 1 + 1)
	{
		currentArray = 1;
		currentIter = currentDataIndex - DEVICE_NAME_LEN - 1 - 1;
	}
	else
	{
		currentArray = 0;
		currentIter = currentDataIndex;
	}
	
	sendHeader();
	
	switch(currentArray)
	{
	case 0:
		if(currentIter == 0)
		{
			packet[sentLen + PREAMBLE_SIZE] = DEVICE_NAME_LEN;
			sentLen++;
		}
		for(; currentIter < DEVICE_NAME_LEN ; currentIter++)
		{
			packet[sentLen + PREAMBLE_SIZE] = DEVICE_NAME[currentIter];
			sentLen++;
			if(sentLen == (RFM69_FIFO_SIZE - (PREAMBLE_SIZE + 1)))
			{
				goto endPacket;
			}
		}
		currentArray++;
		currentIter = 0;
	case 1:
		if(currentIter == 0)
		{
			packet[sentLen + PREAMBLE_SIZE] = DATA_CONFIG_LENGTH;
			sentLen++;
		}
		for(; currentIter < DATA_CONFIG_LENGTH ; currentIter++)
		{
			packet[sentLen + PREAMBLE_SIZE] = DATA_CONFIG[currentIter];
			sentLen++;
			if(sentLen == (RFM69_FIFO_SIZE - (PREAMBLE_SIZE + 1)))
			{
				goto endPacket;
			}
		}
		currentArray++;
		currentIter = 0;
	case 2:
		if(currentIter == 0)
		{
			packet[sentLen + PREAMBLE_SIZE] = PARAMS_CONFIG_LENGTH;
			sentLen++;
		}
		for(; currentIter < PARAMS_CONFIG_LENGTH ; currentIter++)
		{
			packet[sentLen + PREAMBLE_SIZE] = PARAMS_CONFIG[currentIter];
			sentLen++;
			if(sentLen == (RFM69_FIFO_SIZE - PREAMBLE_SIZE))
			{
				goto endPacket;
			}
		}
	}
endPacket:
	packet[packet[0] - 1] = 0;
	for(i = 0 ; i < packet[0] - 1; i++)
	{
		packet[packet[0] - 1] += packet[i];
	}
	rfm69_sendPacket(packet,packet[0]);
	while(!rfm69_checkPacketSent());
	wdt_reset();
}


uint8_t registerDevice(uint8_t lastReceived)
{
	static uint8_t firstRun = 1;
	start:
	if(firstRun == 1)
	{
		wholePacketLength = 1 + DEVICE_NAME_LEN + 1 + DATA_CONFIG_LENGTH + 1 + PARAMS_CONFIG_LENGTH;
		packetsLeft = wholePacketLength / (RFM69_FIFO_SIZE - PREAMBLE_SIZE);
		packetNumber = 0;
		currentDataIndex = 0;
		lastReceived = 0;
		firstRun = 0;
	}
	
	USART_writeStdStr("0\r\n");
	{
		char *str = "              \r\n";
		itoa(lastReceived, str, 10);
		itoa(packetsLeft, str + 4, 10);
		itoa(lastReceived == RFM69_PACKET_TYPE_REGISTER_ACK, str + 10,10);
		uint8_t i;
		for(i = 0 ; i < 16 ; i++)
		{
			if(str[i] == '\0')
				str[i] = ' ';
		}
		USART_writeStr(str, 16);
	}
	if(lastReceived == RFM69_PACKET_TYPE_REGISTER_ACK && packetsLeft == 0)
	{
		USART_writeStdStr("a\r\n");
		return 1;
	}
	else if(lastReceived == RFM69_PACKET_TYPE_REGISTER_ACK)
	{
		USART_writeStdStr("b\r\n");
		// we are sure that one packet (except of last) is that size and CRC
		currentDataIndex += RFM69_FIFO_SIZE - (PREAMBLE_SIZE + 1);
		packetNumber++;
		packetsLeft--;
	}
	else if(lastReceived == RFM69_PACKET_TYPE_REGISTER_FAIL)
	{
		USART_writeStdStr("c\r\n");
		firstRun = 1;
		goto start;
	}
	
	sendPart();
	return 0;
}