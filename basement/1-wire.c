#include "1-wire.h"

const uint8_t crcTable[256] PROGMEM = {
	0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
	157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
	35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
	190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
	70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
	219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
	101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
	248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
	140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
	17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
	175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
	50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
	202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
	87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
	233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
	116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
};

uint8_t OWIRE_readBit(){
	OWIRE_SET_ZERO_OUT;
	_delay_us(1);
	OWIRE_SET_IN;
	_delay_us(15);
	uint8_t val = (OWIRE_PINX >> OWIRE_PIN) & 0x01;
	_delay_us(35);
	return val;
}

uint8_t OWIRE_readByte(){
	uint8_t tmp = 0x00; 
	uint8_t i = 0;
	for(; i < 8 ; i++){
		if( OWIRE_readBit() == 0 ){
			tmp &= ~_BV(i);
		}else{
			tmp |= _BV(i);
		}
	}
	return tmp;
}

void OWIRE_writeBit(uint8_t bit){
	OWIRE_SET_ZERO_OUT;
	if(bit == 0x00){
		_delay_us(60);
	}else{
		_delay_us(1);
		OWIRE_SET_ONE_OUT;
		_delay_us(60);
	}
	OWIRE_SET_IN;
}

void OWIRE_writeByte(uint8_t byte){
	uint8_t i = 0;
	for(; i < 8 ; i++){
		OWIRE_writeBit((byte >> i) & 0x01);
	}
}

uint8_t OWIRE_init(){
	uint8_t presence;
	
	OWIRE_SET_ZERO_OUT;
	_delay_us(500); // wait
	OWIRE_SET_IN;
	_delay_us(100); // wait for signal
	//check presence
	presence = (OWIRE_PINX >> OWIRE_PIN) & 0x01;
	_delay_us(405); // wait for signal to end and pullup sensor resistor
	// 0 is ok
	return !presence;
}

void OWIRE_writeCommand(uint8_t command){
	OWIRE_writeByte(command);
	_delay_us(5);
}

void OWIRE_writeRom(uint64_t rom){
	uint8_t i = 0;
	for(; i < 64 ; i++){
		OWIRE_writeBit(rom >> i & 0x01);
	}
}

uint8_t OWIRE_readRom(uint64_t* rom){
	uint8_t tmp[8];
	tmp[0] = OWIRE_readByte();
	tmp[1] = OWIRE_readByte();
	tmp[2] = OWIRE_readByte();
	tmp[3] = OWIRE_readByte();
	tmp[4] = OWIRE_readByte();
	tmp[5] = OWIRE_readByte();
	tmp[6] = OWIRE_readByte();
	tmp[7] = OWIRE_readByte();
	
	if(OWIRE_checkCRC(tmp[7],tmp,7) != 0)
		return 1;
	
	(*rom) = 0;
	(*rom) |= (uint64_t)tmp[7] << 56;
	(*rom) |= (uint64_t)tmp[6] << 48;
	(*rom) |= (uint64_t)tmp[5] << 40;
	(*rom) |= (uint64_t)tmp[4] << 32;
	(*rom) |= (uint64_t)tmp[3] << 24;
	(*rom) |= (uint64_t)tmp[2] << 16;
	(*rom) |= (uint64_t)tmp[1] << 8;
	(*rom) |= (uint64_t)tmp[0];
	
	return 0;
}

uint8_t OWIRE_checkCRC(uint8_t crc, uint8_t* data, uint8_t dataLength){
	uint8_t tmpCRC = 0x00;
	uint8_t i;
	
	for(i = 0 ; i < dataLength ; i++){
		tmpCRC = pgm_read_byte(&(crcTable[tmpCRC ^ data[i]]));
	}
	
	if(tmpCRC == crc)
		return 0;
	return 1;
}

uint8_t OWIRE_checkRomCRC(uint64_t rom){
	uint8_t * tmp = (uint8_t *)(&rom);
	uint8_t tmp2[7] = {
		tmp[0],
		tmp[1],
		tmp[2],
		tmp[3],
		tmp[4],
		tmp[5],
		tmp[6]
	};
	
	return OWIRE_checkCRC(tmp[7],tmp2,7);
}

void OWIRE_findRoms(uint64_t* roms, uint8_t length){
	//x = 00, 0 = 01, 1 = 10
	uint64_t presence = 0xFFFFFFFFFFFFFFFF; // set 0 when passed by without difference 
	uint8_t readedRoms = 0;
	uint8_t i = 0;
	uint8_t b1 = 0, b2 = 0;
	uint8_t lastChange = 0, beforeLastChange = 0, changed = 0;
	
	while(presence != 0){
		if(readedRoms == length)
			break;
		
		if(changed == 0){
			lastChange = beforeLastChange;
			changed = 0;
		}
		// if changed then from last to end set 1 in presence , else from before to end 1
		for(i = lastChange + 1 ; i < 64 ; i++ ){
			presence |= ((uint64_t)1 << i);
		}
		OWIRE_init();
		OWIRE_writeCommand(OWIRE_SEARCH_ROM);
		for(i = 0 ; i < 64 ; i++){
			b1 = OWIRE_readBit();
			b2 = OWIRE_readBit();
			
			if((b1 == 0) && (b2 != 0)){
				roms[readedRoms] &= ~((uint64_t)1 << i);
				presence &= ~_BV(i);
				OWIRE_writeBit(0);
			}else if((b1 != 0) & (b2 == 0)){
				roms[readedRoms] |= ((uint64_t)1 << i);
				presence &= ~_BV(i);
				OWIRE_writeBit(1);
			}else{
				if(((presence & ((uint64_t)1 << i)) == 0) && lastChange > i){
					roms[readedRoms] &= ~((uint64_t)1 << i);
					OWIRE_writeBit(0);
				}
				else if(((presence & ((uint64_t)1 << i)) != 0) && (lastChange > i)){
					roms[readedRoms] |= ((uint64_t)1 << i);
					OWIRE_writeBit(1);
				}
				else if(((presence & ((uint64_t)1 << i)) == 0) && (lastChange == i)){
					presence |= ((uint64_t)1 << i);
					roms[readedRoms] |= ((uint64_t)1 << i);
					OWIRE_writeBit(1);
				}
				else if(((presence & ((uint64_t)1 << i)) != 0) && (lastChange == i)){
					presence &= ~((uint64_t)1 << i);
					roms[readedRoms] &= ~((uint64_t)1 << i);
					OWIRE_writeBit(0);
				}
				else if(lastChange < i){
					beforeLastChange = lastChange;
					lastChange = i;
					changed = 1;
					roms[readedRoms] |= ((uint64_t)1 << i);
					OWIRE_writeBit(1);
				}
			}
		}
		readedRoms++;
	}
	
	for(i = 0 ; i < length ; i++){
		if(OWIRE_checkRomCRC(roms[i]) != 0)
			roms[i] = 0xFFFFFFFFFFFFFFFF;
	}
}