#include "DS18B20.h"

#include "usart.h"
#include "1-wire.h"

uint8_t DS18B20_readScratchpad(struct DS18B20_Scratchpad* scratchpad, uint64_t rom){
	OWIRE_init();
	
	if(rom == 0){
		OWIRE_writeCommand(OWIRE_SKIP_ROM);
	}else{
		//OWIRE_writeCommand(OWIRE_SKIP_ROM);
		OWIRE_writeCommand(OWIRE_MATCH_ROM);
		OWIRE_writeRom(rom);
	}
	
	OWIRE_writeCommand(DS18B20_READ_SCRATCHPAD);
	scratchpad->tempLSB = OWIRE_readByte();
	scratchpad->tempMSB = OWIRE_readByte();
	scratchpad->tH = OWIRE_readByte();
	scratchpad->tL = OWIRE_readByte();
	scratchpad->config = OWIRE_readByte();
	scratchpad->reserved1 = OWIRE_readByte(); // should be 0xFF
	scratchpad->reserved2 = OWIRE_readByte();
	scratchpad->reserved3 = OWIRE_readByte(); // should be 0x10
	scratchpad->crc = OWIRE_readByte();
	
	//char buff[64];
	//if(scratchpad->config != 0xFF){
	//	sprintf(buff, "%d,%d,0x%X,0x%X,0x%X,0x%X,0x%X,0x%X,%d\r\n",scratchpad->tempLSB,
	//													scratchpad->tempMSB,
	//													scratchpad->tH,
	//													scratchpad->tL,
	//													scratchpad->config,
	//													scratchpad->reserved1,
	//													scratchpad->reserved2,
	//													scratchpad->reserved3,
	//													scratchpad->crc);
	//	USART_writeStdStr(buff);
	//}
	
	if(OWIRE_checkCRC(scratchpad->crc, (uint8_t*)scratchpad , 8) == 0)
		return 0;
	return 1;
}

uint8_t DS18B20_readTemperature(struct DS18B20_Temperature* temperature, uint64_t rom){
	struct DS18B20_Scratchpad scratchpad;
	
	if(rom == 0xFFFFFFFFFFFFFFFF){
		return 1;
	}
	
	if (OWIRE_init() == 0){
		return 1;
	}
	
	if(rom == 0x0000000000000000){
		OWIRE_writeCommand(OWIRE_SKIP_ROM);
	}else{
		//OWIRE_writeCommand(OWIRE_SKIP_ROM);
		OWIRE_writeCommand(OWIRE_MATCH_ROM);
		OWIRE_writeRom(rom);
	}
	
	OWIRE_writeCommand(DS18B20_CONVERT_T);
	_delay_ms(94);	// DS18B20_RES_9BIT
	//_delay_ms(188);	// DS18B20_RES_10BIT
	//_delay_ms(375);	// DS18B20_RES_11BIT
	//_delay_ms(750);	//DS18B20_RES_12BIT
	
	if(DS18B20_readScratchpad(&scratchpad,rom) != 0 ){
		return 1;
	}
	
	temperature->tempInt = ((scratchpad.tempMSB << 4) | (scratchpad.tempLSB >> 4));
	temperature->tempReal = ((uint16_t)scratchpad.tempLSB & 0b00001111) * DS18B20_REAL_MUL;
	
	return 0;
}

void DS18B20_writeScratchpad(uint8_t th, uint8_t tl, uint8_t resolution, 
							uint64_t rom){
	OWIRE_init();
	
	if(rom == 0){
		OWIRE_writeCommand(OWIRE_SKIP_ROM);
	}else{
		OWIRE_writeCommand(OWIRE_MATCH_ROM);
		OWIRE_writeRom(rom);
	}
	
	OWIRE_writeCommand(DS18B20_WRITE_SCRATCHPAD);
	OWIRE_writeByte(th);
	OWIRE_writeByte(tl);
	OWIRE_writeByte(resolution);
	_delay_us(5);
}