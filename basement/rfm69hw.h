#ifndef RFM69HW_H
#define RFM69HW_H

#define DEBUG

#ifdef DEBUG
#include "usart.h"
#endif

#ifndef F_CPU
#define F_CPU 8000000
#endif

#include <avr/pgmspace.h>
#include <util/delay.h>
#include "spi.h"

#define RFM69_PACKET_TYPE_NO_PACKET		0x00 //indicate that no packet was received
#define RFM69_PACKET_TYPE_DATA			0x01
#define RFM69_PACKET_TYPE_CONFIG		0x02
#define RFM69_PACKET_TYPE_REGISTER		0x03
#define RFM69_PACKET_TYPE_REGISTER_ACK	0x04
#define RFM69_PACKET_TYPE_REGISTER_FAIL	0x05

#define RFM69_MODE_SLEEP	0
#define RFM69_MODE_STDBY	1
#define RFM69_MODE_FS		2
#define RFM69_MODE_TX		3
#define RFM69_MODE_RX		4

#define RFM69_PACKET_MAX_SIZE	15
#define RFM69_FIFO_SIZE			65

#define RFM69_NODE_ADDRESS		0x03
#define RFM69_BROADCAST_ADDRESS	0xB0

#define RFM69_DI0_DDR		DDRB
#define RFM69_DI0_PORT		PORTB
#define RFM69_DI0_PIN		PINB
#define RFM69_DI0_PIN_NO	1

//define it if used
//#define RFM69_RESET
#ifdef RFM69_RESET
#define RFM69_RESET_DDR		DDRD
#define RFM69_RESET_PORT	PORTD
#define RFM69_RESET_PIN		PIND
#define RFM69_RESET_PIN_NO	6
#endif

void rfm69_init();
void rfm69_setMode(uint8_t mode);
uint8_t rfm69_getMode();
uint8_t rfm69_checkModeSet();
void rfm69_waitForModeSet();

void rfm69_writeByteToPayload(uint8_t byte);
void rfm69_sendPacket(uint8_t *packet, uint8_t size);
void rfm69_receivePacket(uint8_t *packet);

//0 false, anything else ok
uint8_t rfm69_checkPacketReceived();
uint8_t rfm69_checkPacketSent();

#ifdef RFM69_RESET
void rfm69_reset();
#endif

uint8_t rfm69_checkFifoFull();
uint8_t rfm69_checkFifoThreshold();

void rfm69_waitForFreeBand();
uint8_t rfm69_isBandFree();

#endif