#ifndef RFM73_H
#define RFM73_H

//#define RFM73_DEBUG

#ifndef F_CPU
	#define F_CPU 8000000
#endif

#define CE		5
#define CE_ONE	{DDRC |= _BV(CE); PORTC |= _BV(CE);}
#define CE_ZERO	{DDRC |= _BV(CE); PORTC &= ~_BV(CE);}

//commands. some to use with (comm & x)
#define R_REGISTER			0b00000000
#define W_REGISTER			0b00100000
#define R_RX_PAYLOAD		0b01100001
#define W_TX_PAYLOAD		0b10100000
#define FLUSH_TX			0b11100001
#define FLUSH_RX			0b11100010
#define REUSE_TX_PL			0b11100011
#define ACTIVATE			0b01010000
#define R_RX_PL_WID			0b01100000
#define W_ACK_PAYLOAD		0b10101000
#define W_TX_PAYLOAD_NO_ACK	0b10110000
#define NOP					0b11111111

#include "spi.h"

#include <util/delay.h>
#include <avr/pgmspace.h>


// 0 no, >0 yes
uint8_t RFM73_isRXDataReady();

// returns length of read data
uint8_t RFM73_readPayload(uint8_t* pipeNumber, uint8_t* data, uint8_t dataLength);

uint8_t RFM73_readStatus();
void RFM73_writeRegister(uint8_t reg, uint8_t* data, uint8_t dataLength);
void RFM73_readRegister(uint8_t reg, uint8_t* data, uint8_t dataLength);
void RFM73_readCommand(uint8_t command, uint8_t* data, uint8_t dataLength);
void RFM73_writeCommand(uint8_t command, uint8_t* data, uint8_t dataLength);
uint8_t RFM73_checkBank();
void RFM73_setBank(uint8_t bank);
void RFM73_init();

#endif