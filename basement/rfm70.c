#include "rfm70.h"

// magic numbers for initialization
const PROGMEM uint8_t RFM70_bank1Reg0_13[][4] = {
	{ 0x40, 0x4B, 0x01, 0xE2 },
	{ 0xC0, 0x4B, 0x00, 0x00 },
	{ 0xD0, 0xFC, 0x8C, 0x02 },
	{ 0x99, 0x00, 0x39, 0x41 },
	{ 0xD9, 0x9E, 0x86, 0x0B },
	{ 0x24, 0x06, 0x7F, 0xA6 },
	{ 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00 }, // reversed till this!
	{ 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x12, 0x73, 0x00 },
	{ 0x36, 0xB4, 0x80, 0x00 }  // 0x0d
};
// and more of them
const PROGMEM uint8_t RFM70_bank1Reg14[] = {
	0x41, 0x20, 0x08, 0x04, 0x81, 0x20, 0xCF, 0xF7, 0xFE, 0xFF, 0xFF };

uint8_t RFM70_buffer[11];

uint8_t RFM70_readStatus(){
	SS_SET_ZERO_OUT;
	_delay_us(1);
	uint8_t data = SPI_writeByte(NOP);
	SS_SET_ONE_OUT;
	_delay_us(1);
	return data;
}

inline void RFM70_writeRegister(uint8_t reg, uint8_t* data, uint8_t dataLength){
	RFM70_writeCommand(W_REGISTER | reg , data, dataLength);
}

inline void RFM70_readRegister(uint8_t reg, uint8_t* data, uint8_t dataLength){
	RFM70_readCommand(R_REGISTER | reg, data, dataLength);
}

void RFM70_readCommand(uint8_t command, uint8_t* data, uint8_t dataLength){
	SS_SET_ZERO_OUT;
	_delay_us(1);
	SPI_writeByte(command);
	SPI_readBytes(data,dataLength);
	SS_SET_ONE_OUT;
	_delay_us(1);
}

void RFM70_writeCommand(uint8_t command, uint8_t* data, uint8_t dataLength){
	SS_SET_ZERO_OUT;
	_delay_us(1);
	SPI_writeByte(command);
	if(dataLength > 0){
		SPI_writeBytes(data,dataLength);
	}
	SS_SET_ONE_OUT;
	_delay_us(1);
}

uint8_t RFM70_checkBank(){
	uint8_t data[1];
	RFM70_readRegister(0x07,data,1);
	//USART_writeByte(((data[0] & 0b10000000) >> 7) + '0');
	return ((data[0] & 0b10000000) >> 7);
}

void RFM70_setBank(uint8_t bank){
	if(RFM70_checkBank() == bank)
	 	return;
	RFM70_buffer[0] = 0x53;
	RFM70_writeCommand(ACTIVATE,RFM70_buffer,1);
}

void RFM70_init(){
	uint8_t i, j;
	
	RFM70_setBank(1);
	
	for (i = 0 ; i <= 0x0d ; i++){
		for (j = 0; j < 4; j++){
			RFM70_buffer[j] = pgm_read_byte(&RFM70_bank1Reg0_13[i][j]);
		}
		RFM70_writeRegister(i, RFM70_buffer, 4);
	}
	for (i = 0; i < 11; i++){
		RFM70_buffer[i] = pgm_read_byte(&RFM70_bank1Reg14[i]);
	}
	RFM70_writeRegister(0x0e, RFM70_buffer, 11);
	
	RFM70_setBank(0);
}

inline uint8_t RFM70_isRXDataReady(){
	return (RFM70_readStatus() >> 6) & 0x01;
}

uint8_t RFM70_readPayload(uint8_t* pipeNumber, uint8_t* data, uint8_t dataLength){
	uint8_t tmp, len;
	uint8_t status = RFM70_readStatus();
	
	if(RFM70_isRXDataReady() == 0)
		return 0;
	
	// read lenght in payload
	tmp = RFM70_readStatus();
	*pipeNumber = (tmp >> 1) & 0b00000111;
	RFM70_readRegister(0x11 + *pipeNumber,&len,1);
	
	if(len > dataLength)
		RFM70_readCommand(R_RX_PAYLOAD,data,dataLength);
	else
		RFM70_readCommand(R_RX_PAYLOAD,data,len);
	
	//clear RX_DR bit
	tmp = status | 0b01000000;
	RFM70_writeRegister(0x07,&tmp,1);
	
	#ifdef RFM70_DEBUG
		USART_writeStdStr("Packet Read! Payload:\r\n");
		for(ri = 0 ; ri < 32 ; ri++){
			sprintf(buff,"%d,",data[ri]);
			USART_writeStdStr(buff);
		}
		USART_writeStdStr("\r\n");
	#endif
	
	return len;
}
