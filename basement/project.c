#define DEBUG
#define F_CPU 8000000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>

#include "LCD.h"
#include "rfm69hw.h"
#include "eeprom.h"
#include "register.h"
#include "1-wire.h"
#include "DS18B20.h"

#ifdef DEBUG
#include "usart.h"
#endif

//CO SENSOR
#define CO_DDR	DDRC
#define CO_PORT	PORTC
#define CO_PIN	PINC
#define CO_NO	1
//TODO CO 3.3V / 5V switch

//BUZZER
#define BUZZER_DDR	DDRD
#define BUZZER_PORT	PORTD
#define BUZZER_PIN	PIND
#define BUZZER_NO	7

#define BUZZER_ON	{BUZZER_PORT |= _BV(BUZZER_NO);}
#define BUZZER_OFF	{BUZZER_PORT &= ~_BV(BUZZER_NO);}

#define ALARM_STATE_OFF			0
#define ALARM_STATE_TEMPERATURE	1
#define ALARM_STATE_CO			2
#define ALARM_STATE_BOTH		3

#define USER_ALARM_STATE_AUTO	0
#define USER_ALARM_STATE_OFF	1

//DEBUG
#ifdef DEBUG
char debug_buffer[64];
#endif

//LOGIC VARIABLES
volatile uint8_t secondPassed = 0;
volatile uint8_t loopTimePassed = 0; // for radio operation

volatile uint8_t timer2iterator = 0;
volatile uint8_t timer2iteratorSec = 0;
#define TIMER2_MAX_VAL 130
#define TIMER2_MAX_ITERATIONS 60
#define TIMER2_SECONDS 1 // seconds between sensors readings and sending data

//ALARM
uint8_t alarmStartTemperature = 90;
uint8_t alarmStopDiff = 2;
uint16_t alarmCOThreshold = 512;
uint8_t userAlarmState = USER_ALARM_STATE_AUTO;
uint8_t alarmState = ALARM_STATE_OFF;

//LIGHT SENSOR
uint16_t CO_ADCCurrentValue = 0xFFFF;

//TEMPERATURE
#define SENSORS_COUNT	2
uint64_t oneWireSensors[SENSORS_COUNT];
int8_t currentTemperature[SENSORS_COUNT];

//RFM
uint8_t packet[RFM69_PACKET_MAX_SIZE];
uint8_t packetNo = 0;
uint8_t packetReceived = RFM69_PACKET_TYPE_NO_PACKET;
uint8_t deviceRegistered = 0;


/*****************LEDS******************/
inline void lcd_led_on()
{
	//PORTD |= _BV(4);
}
inline void lcd_led_off()
{
	//PORTD &= ~_BV(4);
}
inline void tx_led_on()
{
	//PORTB &= ~_BV(6);
}
inline void tx_led_off()
{
	//PORTB |= _BV(6);
}
inline void rx_led_on()
{
	//PORTB &= ~_BV(7);
}
inline void rx_led_off()
{
	//PORTB |= _BV(7);
}


/*****************ALARM******************/
//TODO different alarms for temp and co
void setAlarm(uint8_t newState)
{
	alarmState = newState;
}


uint8_t checkAlarmCondition()
{
	uint8_t i;
	uint8_t coAlarm = 0;
	uint8_t tempAlarm = 0;
	//user state
	if(userAlarmState == USER_ALARM_STATE_OFF)
		return ALARM_STATE_OFF;
	//co
	if(CO_ADCCurrentValue > alarmCOThreshold)
	{
		coAlarm = 1;
	}
	//temperature
	if(alarmState == ALARM_STATE_TEMPERATURE || alarmState == ALARM_STATE_BOTH)
	{
		for(i = 0 ; i < SENSORS_COUNT ; i++)
		{
			if(currentTemperature[i] >= (alarmStartTemperature - alarmStopDiff))
			{
				tempAlarm = 1;
				break;
			}
		}
	}
	
	if(coAlarm == 0 && tempAlarm == 0)
		return ALARM_STATE_OFF;
	if(coAlarm == 1 && tempAlarm == 1)
		return ALARM_STATE_BOTH;
	if(coAlarm == 1)
		return ALARM_STATE_CO;
	else //if(tempAlarm == 1)
		return ALARM_STATE_TEMPERATURE;
}


/*****************LOGIC******************/
void read1WireSensors()
{
	uint8_t i;
	struct DS18B20_Temperature currentReading;
	
	for(i = 0 ; i < SENSORS_COUNT; i++)
	{
		if(DS18B20_readTemperature(&currentReading, oneWireSensors[i]) == 0 )
		{
			USART_writeStdStr("temp ok!");
			currentTemperature[i] = currentReading.tempInt;
		}
	}
}


void readCO_Sensor()
{
	ADMUX &= ~_BV(MUX1) | ~_BV(MUX2) | ~_BV(MUX3); // set ADC channel
	ADMUX |= _BV(MUX0);
	
	ADCSRA |= _BV(ADSC);
	while( ADCSRA & _BV(ADSC) );
	
	CO_ADCCurrentValue = ADC;
}


/*****************"GUI"******************/
#ifdef DEBUG
void showDebug()
{
	//sprintf(debug_buffer,"long mean = %d.%d\n\r",(int8_t)longMean,(int8_t)((longMean-(int8_t)longMean))*100);
	//USART_writeStdStr(debug_buffer);
	//USART_writeStdStr("\n\n\r");
	//USART_writeStdStr("i\r\n");
}//*/
#endif


/******************RADIO****************/
void changeConfig()
{
	/**
	 * packet[0]	// packet length
	 * packet[1]	// receiver address
	 * packet[2]	// packet type
	 * packet[3]	// new forced fan state
	 * packet[4]	// new fanStartDiffVal
	 * packet[5]	// new fanStopDiffVal
	 * packet[6]	// new force sensor threshold H
	 * packet[7]	// new force sensor threshold L
	 * packet[8]	// new toiletTimeSecondsOn (0-255)
	 * packet[9]	// new light sensor threshold H
	 * packet[10]	// new light sensor threshold L
	 * packet[11]	// crc (sum of all previous)
	**/
	
	/*USART_writeStdStr("changing config...\r\n");
	
	userFanState = packet[3];
	save_variable_byte(userFanState, USER_FAN_STATE_EEPROM_ADDR);
	
	fanStartDiffVal = packet[4];
	save_variable_byte(fanStartDiffVal, FAN_START_DIFF_EEPROM_ADDR);
	
	fanStopDiffVal = packet[5];
	save_variable_byte(fanStopDiffVal, FAN_STOP_DIFF_EEPROM_ADDR);
	
	toiletADCThreshold = ((uint16_t)packet[6]) << 8;
	toiletADCThreshold |= packet[7];
	save_variable_word(toiletADCThreshold, TOILET_ADC_EEPROM_ADDR);
	
	toiletSecondsOn = packet[8];
	save_variable_byte(toiletSecondsOn, TOILET_SECONDS_ON_EEPROM_ADDR);
	
	lightADCThreshold = ((uint16_t)packet[9]) << 8;
	lightADCThreshold |= packet[10];
	save_variable_word(toiletADCThreshold, TOILET_ADC_EEPROM_ADDR);*/
}


void radioOperationSend()
{
	USART_writeStdStr("radio send (was packet received?)=");
	USART_writeByte('0' + packetReceived);
	USART_writeStdStr("\r\n");
	
	uint8_t packetLength = 12;
	
	if(deviceRegistered == 1)
	{
		//prepare packet for send
		packet[0] = packetLength;
		packet[1] = RFM69_BROADCAST_ADDRESS;	// send broadcast
		packet[2] = RFM69_NODE_ADDRESS;			// send this node address
		packet[3] = RFM69_PACKET_TYPE_DATA;		// packet type
		packet[4] = packetNo++;					// count packets
		packet[5] = packetReceived;				// send if ack was received
		packet[6] = currentTemperature[0];		// send stove
		packet[7] = currentTemperature[1];		// send boiler
		packet[8] = alarmState;					// send alarm state
		packet[9] = CO_ADCCurrentValue >> 8;	// co msb
		packet[10] = CO_ADCCurrentValue;		// co lsb
		packet[11] = 0; 						// for crc
		
		//crc
		uint8_t i = 0;
		for(; i < packetLength - 1 ; i++)
		{
			packet[packetLength - 1] += packet[i];
		}
		
		//send packet
		tx_led_on();
		cli();
		
		rfm69_waitForFreeBand();
		wdt_reset();
		
		rfm69_setMode(RFM69_MODE_TX);
		rfm69_waitForModeSet();
		wdt_reset();
		
		rfm69_sendPacket(packet, RFM69_PACKET_MAX_SIZE);
		
		while(!rfm69_checkPacketSent()); // wait for send
		wdt_reset();
		tx_led_off();
	}
	else
	{
		rfm69_waitForFreeBand();
		wdt_reset();
		
		tx_led_on();
		
		rfm69_setMode(RFM69_MODE_TX);
		rfm69_waitForModeSet();
		wdt_reset();
		
		deviceRegistered = registerDevice(packetReceived);
		
		tx_led_off();
	}
	
	//reset packet received
	packetReceived = RFM69_PACKET_TYPE_NO_PACKET;
	
	//receive ack
	rfm69_setMode(RFM69_MODE_RX);
	rfm69_waitForModeSet(); // wait for mode
	wdt_reset();
	
	TCNT2 = 0;
	timer2iterator = 0;
	timer2iteratorSec = 0; //zerujemy timer, żeby zyskać na czasie
	sei();
}


void radioOperationReceive()
{
	rx_led_on();
	
	cli();
	rfm69_receivePacket(packet);
	sei();
	
	//check crc
	uint8_t crc = 0;
	uint8_t i;
	for(i = 0 ; i < packet[0] - 1; i++)
	{
		crc += packet[i];
	}
	
	if(crc != packet[packet[0] - 1])
	{
		packetReceived = RFM69_PACKET_TYPE_NO_PACKET;
	}
	else
	{
		packetReceived = packet[2];
		if(packetReceived == RFM69_PACKET_TYPE_CONFIG && deviceRegistered != 0)
		{
			changeConfig();
		}
	}
	rx_led_off();
	
	USART_writeStdStr("packetReceived radio receive=");
	USART_writeByte('0' + packetReceived);
	USART_writeStdStr("\r\n");
	
	rfm69_setMode(RFM69_MODE_FS);
}


uint8_t init1WireSensors()
{
	uint8_t ret = 0;
	uint8_t i = 0;
	
	OWIRE_findRoms(oneWireSensors, SENSORS_COUNT);
	
	for(; i < SENSORS_COUNT ; i++)
	{
		if(oneWireSensors[i] != 0x0000000000000000 && oneWireSensors[i] != 0xFFFFFFFFFFFFFFFF)
		{
			/*#ifdef DEBUG
			uint8_t *tmp = (uint8_t *)(&oneWireSensors[i]);
			sprintf(buff, "rom%d ok 0X%X%X%X%X%X%X%X%X\n", i, tmp[7], tmp[6], tmp[5], tmp[4], tmp[3], tmp[2], tmp[1], tmp[0]);
			USART_writeStdStr(buff);
			#endif*/
		}
		else
		{
			oneWireSensors[i] = 0xFFFFFFFFFFFFFFFF;
		}
	}
	
	/*#ifdef DEBUG
	if(ret == 1)
		USART_writeStdStr("WRONG ROMS!\r\n");
	
	sprintf(buff, "oneWireSensors %lX%lX\n\r", (uint32_t)(oneWireSensors[0] >> 32) , (uint32_t)(oneWireSensors[0]));
	USART_writeStdStr(buff);
	sprintf(buff, "oneWireSensors %lX%lX\n\r", (uint32_t)(oneWireSensors[1] >> 32) , (uint32_t)(oneWireSensors[1]));
	USART_writeStdStr(buff);
	#endif*/
	
	for(i = 0 ; i < SENSORS_COUNT ; i++)
	{
		if(oneWireSensors[0] != 0xFFFFFFFFFFFFFFFF)
		{
			DS18B20_writeScratchpad(0, 0, DS18B20_RES_9BIT, oneWireSensors[0]);
		}
	}
	
	return ret;
}


/*****************INIT******************/
void init_adc()
{
	ADCSRA |= _BV(ADEN) // włączenie ADC
			 | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0); // preskaler na 128
}

void init_timers()
{
	//set timer2 for one second interrupt
	OCR2 = TIMER2_MAX_VAL; // ctc compare value
	TIMSK |= _BV(OCIE2); // enable interrupt on compare
	TCCR2 |= _BV(CS22) | _BV(CS21) | _BV(CS20); // set prescaler 1024
	TCCR2 |= _BV(WGM21); // set CTC mode
}

void init_variables()
{
	//init_variable_byte(&fanStartDiffVal, FAN_START_DIFF_EEPROM_ADDR);
	//init_variable_byte(&fanStopDiffVal, FAN_STOP_DIFF_EEPROM_ADDR);
	//init_variable_byte(&userFanState, USER_FAN_STATE_EEPROM_ADDR);
	//init_variable_byte(&toiletSecondsOn, TOILET_SECONDS_ON_EEPROM_ADDR);
	//init_variable_word(&toiletADCThreshold, TOILET_ADC_EEPROM_ADDR);
}


void init_system()
{
	_delay_ms(500);
	wdt_enable(WDTO_1S); // watchdog enable
	
#ifdef DEBUG
	USART_init();
	USART_writeStdStr("\r\ninit\r\n");
#endif
	
	//TODO LEDS
	/*PORTD &= ~_BV(5); //power on led
	lcd_led_on();
	
	//led init
	DDRD |= _BV(4) | _BV(5); // lcd led | power on
	DDRB |= _BV(6) | _BV(7); //tx/rx
	
	PORTB &= ~_BV(6) | ~_BV(7); //test rx/tx
	_delay_ms(500);
	PORTB |= _BV(6) | _BV(7); //test*/
	
	wdt_reset();
	
	init_variables();
	
	sei();
	init_adc();
	init_timers();
	init1WireSensors();
	read1WireSensors(); //initial read
	
	// TODO ALARM TEST
	/*FAN_DDR |= _BV(FAN_NO);		// wyjście tranzystora
	fan_start();				// włączenie testowe
	wdt_reset();
	_delay_ms(800);
	wdt_reset();
	fan_stop();					// wyłączenie wiatraka
	_delay_ms(100); */
	
	//radio init
	rfm69_init();
	wdt_reset();
	rfm69_setMode(RFM69_MODE_RX);
	wdt_reset();
	rfm69_waitForModeSet();
	wdt_reset();
}


int main()
{
	wdt_enable(WDTO_1S); // watchdog enable
	init_system();
	
	for(;;)
	{
		setAlarm(checkAlarmCondition());
		
		///////////////////
		if(secondPassed)
		{
			secondPassed = 0;
			readCO_Sensor();
			read1WireSensors();
#ifdef DEBUG
			showDebug();
#endif
		}
		wdt_reset();
		
		//radio operation may be less often
		if(loopTimePassed)
		{
			loopTimePassed = 0;
			radioOperationSend();
		}
		wdt_reset();
		
		if(rfm69_checkPacketReceived() && (rfm69_getMode() == RFM69_MODE_RX))
		{
			radioOperationReceive();
		}
		wdt_reset();
		
	}//*/
}

/*************INTERRUPTS*****************/
//~~one second interrupt
ISR(TIMER2_COMP_vect)
{
	if(++timer2iterator >= TIMER2_MAX_ITERATIONS)
	{
		timer2iterator = 0;
		secondPassed = 1;
		if(++timer2iteratorSec >= TIMER2_SECONDS)
		{
			timer2iteratorSec = 0;
			loopTimePassed = 1;
		}
	}
}//*/