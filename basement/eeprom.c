#include "eeprom.h"

void init_variable_byte(uint8_t *variable, uint8_t address)
{
	eeprom_busy_wait();
	*variable = eeprom_read_byte((uint8_t *)(uint16_t)address);
}//*/

void save_variable_byte(uint8_t variable, uint8_t address)
{
	eeprom_busy_wait();
	eeprom_update_byte ((uint8_t *)(uint16_t)address, variable);
}

void init_variable_word(uint16_t *variable, uint8_t address)
{
	uint8_t tmp = (uint8_t)(*variable >> 8);
	*variable = 0;
	init_variable_byte(&tmp, address);
	*variable = ((uint16_t)tmp) << 8;
	tmp = (uint8_t)(*variable);
	init_variable_byte(&tmp, address + 1);
	*variable |= tmp;
}//*/

void save_variable_word(uint16_t variable, uint8_t address)
{
	save_variable_byte((uint8_t)(variable >> 8), address);
	save_variable_byte((uint8_t)variable, address + 1);
}