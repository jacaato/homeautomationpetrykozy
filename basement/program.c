/* zadaniem jest tylko wysyłanie na podany adres danych z sensorów */
/* 7 bajt to ilość max_rt poprzednich (nieudanych wysłań)
 * ostatnie 6 bajtów payloadu to temperatury:
 * X X X ~ failures tint1 treal1 t2int t2real */

//sensor1rom 0x8c0055d7ec628
//sensor2rom 0xa20055da0bb28

#define F_CPU 8000000

//#define DEBUG

#ifdef DEBUG
	#include <stdio.h>
	#include "usart.h"
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>

#include "DS18B20.h"
#include "rfm73.h"
#include "spi.h"

#define PAYLOAD_LENGTH 32
#define SENSORS_CNT 2
#define CHANNEL 5

#ifdef DEBUG
	char buff[128];
	uint8_t debug[5];
#endif
uint64_t roms[SENSORS_CNT];

uint8_t initSensors(){
	uint8_t ret = 0;
	uint8_t i = 0;
	
	OWIRE_findRoms(roms,SENSORS_CNT);
	
	for(; i < SENSORS_CNT ; i++){
		if(roms[i] != 0x0000000000000000 && roms[i] != 0xFFFFFFFFFFFFFFFF){
			#ifdef DEBUG
				uint8_t * tmp = (uint8_t *)(&roms[i]);
				sprintf(buff,"rom%d ok 0X%X%X%X%X%X%X%X%X\n", i,tmp[7],tmp[6],tmp[5],tmp[4],tmp[3],tmp[2],tmp[1],tmp[0]);
				USART_writeStdStr(buff);
			#endif
		}
		else{
			roms[i] = 0xFFFFFFFFFFFFFFFF;
			ret = 1;
		}
	}
	
	#ifdef DEBUG
		if(ret == 1)
			USART_writeStdStr("WRONG ROMS!\r\n");
		
		sprintf(buff,"roms0 %lX%lX\n\r",(uint32_t)(roms[0] >> 32) , (uint32_t)(roms[0]));
		USART_writeStdStr(buff);
		sprintf(buff,"roms1 %lX%lX\n\r",(uint32_t)(roms[1] >> 32) , (uint32_t)(roms[1]));
		USART_writeStdStr(buff);
	#endif
	
	for(i = 0 ; i < SENSORS_CNT ; i++){
		DS18B20_writeScratchpad(0,0,DS18B20_RES_9BIT,roms[0]);
	}
	
	return ret;
}

void configureRadio(){
	uint8_t data[5];
	
	// features enabled
	data[0] = 0x73;
	RFM73_writeCommand(ACTIVATE,data,1);
	CE_ONE;
	
	RFM73_setBank(0);
	// configure to tx
	data[0] = 0b00001110;
	RFM73_writeRegister(0x00,data,1);
	
	//chanel 5
	data[0] = CHANNEL;
	RFM73_writeRegister(0x05,data,1);
	
	// TX/RX0 address
	uint8_t addr[] = {0xe7,0xe7,0xe7,0xe7,0xe7}; // 0xe7
	RFM73_writeRegister(0x0A,addr,5);
	RFM73_writeRegister(0x10,addr,5);
	
	
	//data[0] = 0b00101111; //2Mb/s rfm 73
	data[0] = 0b00000111; //1Mb/s rfm 73
	RFM73_writeRegister(0x06,data,1);
	
	//disable auto acknowledge
	//data[0] = 0x00000000;
	//RFM73_writeRegister(0x01,data,1);
	
	/*#ifdef DEBUG
		RFM73_readRegister(0x00,data,1);
		sprintf(buff,"CONFIG = %d\n\r",data[0]);
		USART_writeStdStr(buff);
		
		RFM73_readRegister(0x06,data,1);
		sprintf(buff,"RF_DR (speed) = %d\n\r",(data[0] & 0b00001000) >> 3);
		USART_writeStdStr(buff);
		
		RFM73_readRegister(0x0A,data,5);
		sprintf(buff,"RX_ADDR_0 = %X:%X:%X:%X:%X\n\r",data[0],data[1],data[2],data[3],data[4]);
		USART_writeStdStr(buff);
		
		RFM73_readRegister(0x10,data,5);
		sprintf(buff,"TX_ADDR = %X:%X:%X:%X:%X\n\r",data[0],data[1],data[2],data[3],data[4]);
		USART_writeStdStr(buff);
		
		RFM73_readRegister(0x05,data,1);
		sprintf(buff,"RF_CH = %d\n\r",data[0]);
		USART_writeStdStr(buff);
	#endif//*/
}

void initSystem(){
	_delay_ms(500);
	#ifdef DEBUG
		USART_init();
		USART_writeStdStr("init!\r\n");
	#endif
	
	SPI_master_init();
	RFM73_init();
	configureRadio();
	
	initSensors();
}

int main(){
	struct DS18B20_Temperature temperature;
	uint8_t payload[PAYLOAD_LENGTH] ;
	uint8_t i, rfmCommand,failures = 0;
	
	initSystem();
	
	for(;;){
		
		for( i = 0 ; i < PAYLOAD_LENGTH ; i++ ){
			payload[i] = 0;
		}
		
		payload[PAYLOAD_LENGTH - 7] = failures;
		
		if(DS18B20_readTemperature(&temperature, roms[0]) == 0){
			#ifdef DEBUG
				//if(temperature.tempInt > 76){
					sprintf(buff,"\r\ntemp0 =%d.%d\r\n", temperature.tempInt ,temperature.tempReal );
					USART_writeStdStr(buff);
				//}
			#endif
			payload[PAYLOAD_LENGTH - 6] = temperature.tempInt;
			payload[PAYLOAD_LENGTH - 5] = temperature.tempReal >> 8;
			payload[PAYLOAD_LENGTH - 4] = temperature.tempReal ;
		}
		else{
			payload[PAYLOAD_LENGTH - 6] = -128; // error state
			#ifdef DEBUG
				USART_writeStdStr("Błąd odczytu 0\r\n");
			#endif
		}
		
		if(DS18B20_readTemperature(&temperature, roms[1]) == 0){
			#ifdef DEBUG
				sprintf(buff,"temp1 =%d.%d\r\n", temperature.tempInt ,temperature.tempReal );
				USART_writeStdStr(buff);
			#endif
			payload[PAYLOAD_LENGTH - 3] = temperature.tempInt;
			payload[PAYLOAD_LENGTH - 2] = temperature.tempReal >> 8;
			payload[PAYLOAD_LENGTH - 1] = temperature.tempReal;
		}
		else{
			payload[PAYLOAD_LENGTH - 3] = -128; // error state
			#ifdef DEBUG
				USART_writeStdStr("Błąd odczytu 1\r\n");
			#endif
		}
		
		#ifdef DEBUG
		uint8_t i = 0;
		USART_writeStdStr("Payload: ");
		for(i ; i < PAYLOAD_LENGTH ; i++){
			sprintf(buff,"%d,",payload[i]);
			USART_writeStdStr(buff);
		}
		USART_writeStdStr("\r\n");
		#endif
		RFM73_writeCommand(W_TX_PAYLOAD,payload,PAYLOAD_LENGTH);
		#ifdef DEBUG
			RFM73_readRegister(0x17,debug,1);
			sprintf(buff, "Fifo Status:\r\nTX_REUSE\t%d\r\nTX_FULL \t%d\r\nTX_EMPTY\t%d\r\nRX_FULL \t%d\r\nRX_EMPTY\t%d\r\n\r\n",
						//(debug[0] & 0b10000000) >> 7,
						(debug[0] & 0b01000000) >> 6,
						(debug[0] & 0b00100000) >> 5,
						(debug[0] & 0b00010000) >> 4,
						//(debug[0] & 0b00001000) >> 3,
						//(debug[0] & 0b00000100) >> 2,
						(debug[0] & 0b00000010) >> 1,
						(debug[0] & 0b00000001)
					);
			USART_writeStdStr(buff);
			debug[0] = RFM73_readStatus();
			sprintf(buff,"status 0x%X\r\n",debug[0]);
			USART_writeStdStr(buff);
		#endif
		
		//wait
		failures = 0;
		while(((RFM73_readStatus() & 0b00100000) >> 5) != 0x01){
			#ifdef DEBUG
			sprintf(buff,"status = %d\r\n",RFM73_readStatus());
			USART_writeStdStr(buff);
			#endif
			if(((RFM73_readStatus() & 0b00010000) >> 4) == 0x01){
				#ifdef DEBUG
				USART_writeStdStr("max rt!\r\n");
				#endif
				//reset max_rt, cancel sending
				rfmCommand = 0b00010000;
				RFM73_writeRegister(0x07,&rfmCommand,1);
				RFM73_writeCommand(FLUSH_TX,0,0);
				break;
			}
			#ifdef DEBUG
				USART_writeStdStr("sending...");
			#endif
			if(failures >= 10)
				break;
			failures++;
			_delay_ms(100);
		}
		
		if(failures < 10){
			#ifdef DEBUG
			USART_writeStdStr("Data sent\n\r");
			#endif
		}
		else{
			#ifdef DEBUG
			USART_writeStdStr("Failed to send data!\n\r");
			#endif
			//buzzer
			DDRD |= _BV(3);
			PORTD &= ~_BV(3);
			_delay_ms(10);
			PORTD |= _BV(3);
		}
		
		if(((RFM73_readStatus() & 0b00100000) >> 5) == 0x01){
			#ifdef DEBUG
				RFM73_readRegister(0x08,debug,1);
				sprintf(buff,"OBSERVE_TX = %d\n\r",debug[0]);
				USART_writeStdStr(buff);
			#endif
			//reset observe tx by writing to rf_ch
			rfmCommand = CHANNEL;
			RFM73_writeRegister(0x05,&rfmCommand,1);
			failures = 0;
		}
		
		// reset TX_DR
		rfmCommand = 0b00100000;
		RFM73_writeRegister(0x07,&rfmCommand,1);
		
		_delay_ms(500);
	}
	return 0;
}