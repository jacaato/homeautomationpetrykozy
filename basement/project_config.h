#ifndef PROJECT_H
#define PROJECT_H

// addres to send config data
#define RECEIVER_ADDRESS 0xB0

// this device address
#define DEVICE_ADDRESS 0x03

//define device name
#define DEVICE_NAME_LEN 8
uint8_t DEVICE_NAME[DEVICE_NAME_LEN] =
{
	'B', 'a', 's', 'e', 'm', 'e', 'n', 't'
};

// define data config
#define DATA_CONFIG_AMOUNT 4
#define DATA_CONFIG_LENGTH 60
// type 1,2,3..100 - no of bytes unsigned. 101,102,103...200 - no of bytes signed (%100), 255 - enum
// {name_len,name,bigChartId,bigChartType,type(1,2... bytes),minval,maxval,unitlen,unit}
// {name_len,name,type(0xFF),amount,len1,name1...lenN,nameN}
uint8_t DATA_CONFIG[DATA_CONFIG_LENGTH] =
{
	5, 'S', 't', 'o', 'v', 'e', 1, 0, 101, -5, 125, 2, 176/*°*/, 'C', //14
	6, 'B', 'o', 'i', 'l', 'e', 'r', 1, 0, 101, -5, 125, 2, 176/*°*/, 'C', //15
	5, 'A', 'l', 'a', 'r', 'm', 0, 0, 0xFF, 2, 3, 'o', 'f', 'f', 2, 'o', 'n', //17
	2, 'C', 'O', 2, 0, 2, 0, 0, 3, 255, 3, 'p', 'p', 'm' //14
};

// define parameters config
#define PARAMS_CONFIG_AMONUT 4
#define PARAMS_CONFIG_LENGTH 92
uint8_t PARAMS_CONFIG[PARAMS_CONFIG_LENGTH] =
{
	14, 'A', 'l', 'a', 'r', 'm', ' ', 'w', 'a', 't', 'e', 'r', ' ', 'o', 'n', 101, -5, 125, //18
	15, 'A', 'l', 'a', 'r', 'm', ' ', 'w', 'a', 't', 'e', 'r', ' ', 'o', 'f', 'f', 101, -5, 125, //19
	11, 'A', 'l', 'a', 'r', 'm', ' ', 'C', 'O', ' ', 'o', 'n', 2, 0, 0, 1023 >> 8, (uint8_t)1023, //17
	12, 'A', 'l', 'a', 'r', 'm', ' ', 'C', 'O', ' ', 'o', 'f', 'f', 2, 0, 0, 1023 >> 8, (uint8_t)1023, //18
	5, 'A', 'l', 'a', 'r', 'm', 0xFF, 3, 4, 'a', 'u', 't', 'o', 2, 'o', 'n', 3, 'o', 'f', 'f', //20
};

#endif
