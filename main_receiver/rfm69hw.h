#ifndef RFM69HW_H
#define RFM69HW_H

#ifndef F_CPU
#define F_CPU 16000000
#endif

#include <avr/pgmspace.h>
#include "spi.h"
#include <avr/wdt.h>
#include <util/delay.h>

#define RFM69_MODE_SLEEP	0
#define RFM69_MODE_STDBY	1
#define RFM69_MODE_FS		2
#define RFM69_MODE_TX		3
#define RFM69_MODE_RX		4

#define RFM69_PACKET_SIZE	14 //TODO
#define RFM69_FIFO_SIZE		66 //TODO

#define RFM69_NODE_ADDRESS		0x01
#define RFM69_BROADCAST_ADDRESS	0xB0

#define RFM69_DI0_DDR		DDRB
#define RFM69_DI0_PORT		PORTB
#define RFM69_DI0_PIN		PINB
#define RFM69_DI0_PIN_NO	1

void rfm69_init();
void rfm69_setMode(uint8_t mode);
uint8_t rfm69_checkModeSet();

void rfm69_sendPacket();
uint8_t rfm69_readByteFromPayload();
void rfm69_receivePacket(uint8_t *packet, uint8_t len);

//0 false, anything else ok
uint8_t rfm69_checkPacketReceived();
uint8_t rfm69_checkPacketSent();

uint8_t rfm69_checkFifoFull();
uint8_t rfm69_checkFifoThreshold();
uint8_t rfm69_checkFifoEmpty();

void rfm69_waitForFreeBand();
uint8_t rfm69_isBandFree();

#endif