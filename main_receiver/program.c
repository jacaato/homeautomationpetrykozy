﻿#define F_CPU 16000000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/delay.h>

#include "usbdrv.h"
#include "spi.h"
#include "rfm69hw.h"

//leds
#define ENABLE_GREEN_LED	{DDRD |= _BV(7);}
#define ENABLE_RED_LED		{DDRB |= _BV(0);}
#define TURN_ON_GREEN_LED	{PORTD &= ~_BV(7);}
#define TURN_ON_RED_LED		{PORTB &= ~_BV(0);}
#define TURN_OFF_GREEN_LED	{PORTD |= _BV(7);}
#define TURN_OFF_RED_LED	{PORTB |= _BV(0);}

//rfm modes
#define MODE_RX			0x00
#define MODE_TX			0x01

//control commands
#define SET_MODE		0x00
#define CHECK_MODE_SET	0x01
#define CHECK_PACKET_RT	0x02 // check if packet was send(tx) or received(rx)
#define DOWNLOAD_PACKET 0x03 // download packet if there is any in rfm fifo
#define UPLOAD_PACKET	0x04 // upload packet to send

//interrupt messages
#define PACKET_RECEIVED 0x01
#define MODE_SET		0x02
#define PACKET_SENT		0x03

volatile uint8_t packetToSend[RFM69_FIFO_SIZE];
volatile uint8_t packetToReceive[RFM69_FIFO_SIZE];
static uchar currentPosition, bytesRemaining;

static uchar interruptMessage[2];

volatile uint8_t mode;
volatile uint8_t settingNewMode = 0;
volatile uint8_t response = 0;
volatile uint8_t packetSending = 0;


uchar usbFunctionWrite(uchar *data, uchar len)
{
	uchar i;
	if(len > bytesRemaining)	// if this is the last incomplete chunk
		len = bytesRemaining;	// limit to the amount we can store
	bytesRemaining -= len;
	for(i = 0; i < len; i++)
	{
		packetToSend[currentPosition++] = data[i];
	}
	if(bytesRemaining == 0)
	{
		rfm69_waitForFreeBand();
		rfm69_sendPacket(packetToSend);
		packetSending = 1;
		return 1; // return 1 if we have all data
	}
	return 0;
}


USB_PUBLIC uchar usbFunctionSetup(uchar data[8])
{
	// communication computer→device
	usbRequest_t *rq = (void *)data; // cast data to correct type
	
	switch(rq->bRequest)   // custom command is in the bRequest field
	{
	case SET_MODE:
		switch(rq->wValue.word)
		{
		case MODE_RX:
			rfm69_setMode(RFM69_MODE_RX);
			mode = MODE_RX;
			settingNewMode = 1;
			break;
		case MODE_TX:
			rfm69_setMode(RFM69_MODE_TX);
			mode = MODE_TX;
			settingNewMode = 1;
			break;
		default:
			return 0;
		}
		TURN_OFF_RED_LED;
		TURN_OFF_GREEN_LED;
		return 0;
	//case CHECK_MODE_SET:
	//	response = rfm69_checkModeSet(); // same code for tx
	//	usbMsgPtr = (uint16_t)&response;
	//	return sizeof(response);
	case CHECK_PACKET_RT:
		response = rfm69_checkPacketReceived(); // same code for tx
		if(response != 0 && mode == MODE_RX)  // read length to be read
		{
			response = rfm69_readByteFromPayload() - 1; // will not read this one byte anymore
		}
		usbMsgPtr = (uint16_t)&response;
		return sizeof(response);
	case DOWNLOAD_PACKET:
		usbMsgPtr = (uint16_t)packetToReceive;
		TURN_OFF_GREEN_LED;
		return rq->wValue.word;
	case UPLOAD_PACKET:
		TURN_ON_RED_LED;
		currentPosition = 0;						// initialize position index
		bytesRemaining = rq->wLength.word;			// store the amount of data requested
		if(bytesRemaining > sizeof(packetToSend))	// limit to buffer size
			bytesRemaining = sizeof(packetToSend);
		return USB_NO_MSG;							// tell driver to use usbFunctionWrite()
	default:
		return 0;
	}
}


void initSystem()
{
	uint8_t i;
	
	//leds
	ENABLE_GREEN_LED;
	ENABLE_RED_LED;
	TURN_ON_GREEN_LED;
	TURN_ON_RED_LED;
	
	//starting mode
	mode = MODE_RX;
	
	wdt_enable(WDTO_1S); // enable 1s watchdTog timer
	
	SPI_master_init();
	rfm69_init();
	rfm69_setMode(RFM69_MODE_RX);
	
	wdt_reset();
	
	usbInit();
	
	usbDeviceDisconnect(); // enforce re-enumeration
	for(i = 0; i < 250; i++) // wait 500 ms
	{
		wdt_reset(); // keep the watchdog happy
		_delay_ms(2);
	}
	usbDeviceConnect();
	sei(); // Enable interrupts after re-enumeration
	
	TURN_OFF_GREEN_LED;
	TURN_OFF_RED_LED;
}


int main(void)
{
	initSystem();
	
	for(;;)
	{
		wdt_reset(); // keep the watchdog happy
		usbPoll();
		
		// check new packet
		if(mode == MODE_RX && rfm69_checkPacketReceived())
		{
			TURN_ON_GREEN_LED;
			if(usbInterruptIsReady())
			{
				interruptMessage[0] = PACKET_RECEIVED;
				interruptMessage[1] = rfm69_readByteFromPayload(); // packet length
				usbSetInterrupt(interruptMessage, sizeof(interruptMessage));
				rfm69_receivePacket((uint8_t *)packetToReceive, interruptMessage[1]); // clears check packet received
			}
		}
		
		//checking if packet was sent
		if(packetSending == 1 && usbInterruptIsReady() && rfm69_checkPacketReceived() != 0)
		{
			packetSending = 0;
			interruptMessage[0] = PACKET_SENT;
			usbSetInterrupt(interruptMessage, sizeof(interruptMessage));
		}
		
		//setting mode interrupt message
		if(settingNewMode == 1 && usbInterruptIsReady() && rfm69_checkModeSet() != 0)
		{
			packetSending = 0;
			//newPacketArrived = 0;
			settingNewMode = 0;
			interruptMessage[0] = MODE_SET;
			interruptMessage[1] = mode;
			usbSetInterrupt(interruptMessage, sizeof(interruptMessage));
			TURN_OFF_RED_LED;
		}
	}
	return 0;
}