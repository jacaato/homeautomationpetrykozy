#ifndef F_CPU
	#define F_CPU 16000000
#endif

#include <avr/io.h>

#define SS		2
#define MOSI	3
#define MISO	4
#define SCK		5

#define SS_SET_ONE_OUT		{ DDRB |= _BV(SS); PORTB |= _BV(SS); }
#define SS_SET_ZERO_OUT		{ DDRB |= _BV(SS); PORTB &= ~_BV(SS); }

//polling communication
void SPI_master_init();

uint8_t SPI_readByte();
void SPI_readBytes(uint8_t* data, uint8_t dataLength);

uint8_t SPI_writeByte(uint8_t data);
void SPI_writeBytes(uint8_t* data, uint8_t dataLength);
