#include "rfm69hw.h"

#define RFM69_ARRAY2D_SIZE		2
#define RFM69_ARRAY_FREQ_SIZE	3
#define RFM69_ARRAY_CONFIG_SIZE	23
#define RFM69_ARRAY_TX_SIZE		6
#define RFM69_ARRAY_RX_SIZE		6

const uint8_t RFM69HFreqTbl[RFM69_ARRAY_FREQ_SIZE][RFM69_ARRAY2D_SIZE] PROGMEM =
{
	{0x07, 0x6C},
	{0x08, 0x80},
	{0x09, 0x00}
};	//434MHz

const uint8_t RFM69HConfigTbl[RFM69_ARRAY_CONFIG_SIZE][RFM69_ARRAY2D_SIZE] PROGMEM =
{
	{0x02, 0x00},	//RegDataModul 			FSK Packet
	
	{0x05, 0x02},	//RegFdevMsb			241*61Hz = 35KHz
	{0x06, 0x41},	//RegFdevLsb
	{0x03, 0x34},	//RegBitrateMsb			32MHz/0x3410 = 2.4kpbs
	{0x04, 0x10},	//RegBaddressitrateLsb
	{0x13, 0x0F},	//RegOcp				Disable OCP
	{0x18, 0x88},	//RegLNA				200R
	{0x19, 0x52},	//RegRxBw				RxBW 83KHz
	
	{0x2C, 0x00},	//RegPreambleMsb
	{0x2D, 0x05},	//RegPreambleLsb		5Byte Preamble
	
	{0x2E, 0x90},	//enable Sync.Word		2+1=3bytes
	{0x2F, 0x90},	//0x90					SyncWord = 0x900315
	{0x30, 0x03},	//0x03
	{0x31, 0x15},	//0x15
	
	{0x37, 0b10110100},				//RegPacketConfig1  enable CRC, manchester encode, broadcast check
	{0x38, RFM69_FIFO_SIZE},		//RegPayloadLength  N bytes for length & Fixed length
	{0x39, RFM69_NODE_ADDRESS},		// my address
	{0x3A, RFM69_BROADCAST_ADDRESS},// broadcast
	{0x3C, 0x80 | RFM69_PACKET_SIZE}, //RegFiFoThresh
	
	{0x58, 0x1B},	//RegTestLna	Normal sensitivity
	//{0x58,0x2D},	//RegTestLna	increase sensitivity with LNA (Note: consumption also increase!)
	//{0x6F, 0x30},	//RegTestDAGC	Improved DAGC
	{0x6F,0x00},	//chng //RegTestDAGC	Normal DAGC
	
	{0x01, 0x04}	//Standby
};

const uint8_t RFM69HRxTable[RFM69_ARRAY_TX_SIZE][RFM69_ARRAY2D_SIZE] PROGMEM =
{
	{0x11, 0x9F},	//RegPaLevel Fifo In for Rx
	{0x25, 0x40},	//DI0 → payload ready
	{0x13, 0x1A},	//RegOcp			enable OCP
	{0x5A, 0x55},	//Normal and Rx
	{0x5C, 0x70},	//Normal and Rx
	{0x01, 0x10}		//Entry to Rx
};

const uint8_t RFM69HTxTable[RFM69_ARRAY_RX_SIZE][RFM69_ARRAY2D_SIZE] PROGMEM =
{
	{0x25, 0x00},	// DI0 → packet sent
	{0x11, 0x7F}, //chgd //RegPaLevel 20dBm
	//{0x11,0x9F},
	{0x13, 0x0F},	//RegOcp	Disable OCP
	//{0x5A, 0x5D},	//High power mode
	//{0x5C, 0x7C},	//High power mode
	{0x5A,0x55},	//Normal and Rx
	{0x5C,0x70},	//Normal and Rx
	{0x01, 0x0C}		//Entry to Tx
};

void rfm69_initReg(const uint8_t reg[][RFM69_ARRAY2D_SIZE], uint8_t len)
{
	uint8_t i;
	for(i = 0 ; i < len ; i++)
	{
		SS_SET_ZERO_OUT;
		SPI_writeByte(pgm_read_byte(&(reg[i][0])) | 0b10000000); //write
		SPI_writeByte(pgm_read_byte(&(reg[i][1])));
		SS_SET_ONE_OUT;
	}
}

void rfm69_waitForModeSet()
{
	uint8_t state = 0x00;
	while(!state)
	{
		state = rfm69_checkModeSet();
	}
}

void rfm69_init()
{
	SPI_master_init();
	RFM69_DI0_DDR &= ~_BV(RFM69_DI0_PIN_NO); // set as input
	RFM69_DI0_PORT |= _BV(RFM69_DI0_PIN_NO); //pullup
	//RFM69_DI0_PORT &= ~_BV(RFM69_DI0_PIN_NO); //do not pullup
	rfm69_initReg(RFM69HFreqTbl, RFM69_ARRAY_FREQ_SIZE);
	rfm69_initReg(RFM69HConfigTbl, RFM69_ARRAY_CONFIG_SIZE);
	
	//rc calibration
	SS_SET_ZERO_OUT;
	SPI_writeByte(0x0A | 0b10000000); //write
	SPI_writeByte(0x01);
	SS_SET_ONE_OUT;
	
	uint8_t state = 0;
	while(state == 0)
	{
		SS_SET_ZERO_OUT;
		SPI_writeByte(0x0A);
		state = SPI_readByte();
		SS_SET_ONE_OUT;
	}
	//rc calibration over
	
	rfm69_setMode(RFM69_MODE_RX);
	rfm69_waitForModeSet();
}

void rfm69_setMode(uint8_t mode)
{
	switch(mode)
	{
	case RFM69_MODE_SLEEP:
		SS_SET_ZERO_OUT;
		SPI_writeByte(0x01 | 0b10000000); //write
		SPI_writeByte(0b00000000);
		SS_SET_ONE_OUT;
		break;
	case RFM69_MODE_STDBY:
		SS_SET_ZERO_OUT;
		SPI_writeByte(0x01 | 0b10000000); //write
		SPI_writeByte(0b00000010);
		SS_SET_ONE_OUT;
		break;
	case RFM69_MODE_FS:
		SS_SET_ZERO_OUT;
		SPI_writeByte(0x01 | 0b10000000); //write
		SPI_writeByte(0b00000100);
		SS_SET_ONE_OUT;
		break;
	case RFM69_MODE_TX:
		rfm69_initReg(RFM69HTxTable, 6);
		break;
	case RFM69_MODE_RX:
		rfm69_initReg(RFM69HRxTable, 6);
		break;
	}
	//rfm69_waitForModeSet();
}

void rfm69_sendPacket(uint8_t *packet)
{
	uint8_t i;
	for(i = 0 ; i < RFM69_PACKET_SIZE ; i++)
	{
		SS_SET_ZERO_OUT;
		SPI_writeByte(0x00 | 0b10000000); //write
		SPI_writeByte(packet[i]);
		SS_SET_ONE_OUT;
	}
}

inline uint8_t rfm69_checkPacketSent()
{
	//DI0 VERSION
	return (RFM69_DI0_PIN & _BV(RFM69_DI0_PIN_NO));
	//SPI VERSION
	/*uint8_t i;
	SS_SET_ZERO_OUT;
	SPI_writeByte(0x28);
	SPI_readBytes(&i,1);
	SS_SET_ONE_OUT;
	if((i & 0b00001000) != 0b00001000){
		return 0;
	}
	return 1;*/
}


uint8_t rfm69_readByteFromPayload()
{
	uint8_t ret;
	SS_SET_ZERO_OUT;
	SPI_writeByte(0x00);
	ret = SPI_readByte();
	SS_SET_ONE_OUT;
	return ret;
}

void rfm69_receivePacket(uint8_t *packet, uint8_t len)
{
	if(len != 0)
	{
		SS_SET_ZERO_OUT;
		SPI_writeByte(0x00);
		SPI_readBytes(packet, len);
		SS_SET_ONE_OUT;
	}
	else
	{
		SS_SET_ZERO_OUT;
		SPI_writeByte(0x00);
		packet[0] = SPI_readByte() - 1;
		SS_SET_ONE_OUT;
		
		SS_SET_ZERO_OUT;
		SPI_writeByte(0x00);
		SPI_readBytes(packet + 1 , packet[0]);
		SS_SET_ONE_OUT;
	}
}

inline uint8_t rfm69_checkPacketReceived()
{
	//DI0 VERSION
	return (RFM69_DI0_PIN & _BV(RFM69_DI0_PIN_NO));
	//SPI VERSION
	/*uint8_t i = 0;
	SS_SET_ZERO_OUT;
	SPI_writeByte(0x28);
	SPI_readBytes(&i,1);
	SS_SET_ONE_OUT;
	if((i & 0b00000100) != 0b00000100){
		return 0;
	}
	return 1;*/
}

uint8_t rfm69_checkModeSet()
{
	uint8_t state;
	SS_SET_ZERO_OUT;
	SPI_writeByte(0x27);
	state = SPI_readByte();
	SS_SET_ONE_OUT;
	return ((state & 0b10000000) != 0);
}

void rfm69_waitForFreeBand()
{
	uint8_t i;
	uint8_t currentTCNT2;
	while(!rfm69_isBandFree())
	{
		currentTCNT2 = TCNT2;
		for(i = 0; i < currentTCNT2; i++)
		{
			_delay_ms(2);
			wdt_reset();
		}
	}
}

uint8_t rfm69_isBandFree()
{
	uint8_t state;
	
	//set rx
	rfm69_setMode(RFM69_MODE_RX);
	wdt_reset();
	rfm69_waitForModeSet();
	wdt_reset();
	
	//wait some time
	_delay_ms(5);
	
	SS_SET_ZERO_OUT;
	SPI_writeByte(0x27);
	state = SPI_readByte();
	SS_SET_ONE_OUT;
	
	rfm69_setMode(RFM69_MODE_TX);
	rfm69_waitForModeSet();
	
	return !(state & 0b00000001);
}